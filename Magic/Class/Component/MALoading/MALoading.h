//
//  MMLoading.h
//  MumUnion
//
//  Created by Mr Gao on 15/7/8.
//  Copyright (c) 2015年 octech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MALoading : UIView

/** 初始化单例对象 **/
+ (instancetype)shareLoading;

/**
 *  将加载视图加到view中
 *
 *  @param text 提示文字
 *  @param view 视图
 */
+ (void)showLoading:(NSString *)text InView:(UIView *)view;

/**
 *  将加载视图加到window上让用户不能点击
 *
 *  @param text   提示文字
 *  @param window 视图
 */
+ (void)showLoadingInWindow:(NSString *)text;

/**
 *  自定义frame值的菊花
 *
 *  @param view   菊花显示的视图
 *  @param frame   菊花的位置和尺寸
 */

+ (void)showLoadingInView:(UIView *)view withFrame:(CGRect)frame;

/**
 *  从view中移除加载视图
 *
 *  @param view 加载视图的父视图
 */
+ (void)hideLoading;

@end
