//
//  MMLoading.m
//  MumUnion
//
//  Created by Mr Gao on 15/7/8.
//  Copyright (c) 2015年 octech. All rights reserved.
//

#import "MALoading.h"


@interface MALoading ()

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, assign) BOOL target;

@end

@implementation MALoading

+ (instancetype)shareLoading
{
    static MALoading *_loading = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _loading = [[MALoading alloc] init];
    });
    return _loading;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self configUIWithFrame:(CGRect)frame];
    }
    return self;
}



/**
 *  实例化对象
 *
 *  @param view 在哪个view上创建
 *
 *  @return
 */
+ (void)showLoading:(NSString *)text InView:(UIView *)view
{
    CGRect frame = view.frame;
    CGFloat width = frame.size.width;
    CGFloat height = frame.size.height;
    CGFloat offsetY = 0;
    int scroll = 0;
    
    if ([view isKindOfClass:[UIScrollView class]]) {
        UIScrollView *scrollView = (UIScrollView *)view;
        
        height = scrollView.contentSize.height;
        offsetY = scrollView.contentOffset.y;
        scroll = scrollView.scrollEnabled ? 1 : 0; // 缓存scrollView的滚动属性
        
        scrollView.scrollEnabled = NO;
    }
    
    
    MALoading *loading = [MALoading shareLoading];
    loading.frame = CGRectMake(0, 0, width, height);
    loading.tag = scroll;
    
    
    CGFloat offset = 64;
    CGFloat imageHeight = 30;
    
    // 文本框
    CGFloat textWidth = 0;
    if ([text isKindOfClass:[NSString class]] && text.length > 0) {
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:12];
        label.text = text;
        textWidth = [label widthForLabelWithHeight:12] + 30;
        textWidth = MIN(textWidth, view.frame.size.width - 20);
    }
    
    UIView *bgView = [[UIView alloc] init];
    CGFloat bgWidth = (textWidth > 0) ? textWidth : 50;
    CGFloat bgHeight = (textWidth > 0) ? 75 : bgWidth;
    bgView.center = CGPointMake(view.frame.size.width / 2, view.frame.size.height / 2 - offset / 2 + offsetY);
    bgView.bounds = CGRectMake(0, 0, bgWidth, bgHeight);
    bgView.layer.cornerRadius = 6;
//    bgView.layer.borderWidth = 0.5;
//    bgView.layer.borderColor = [UIColor colorWithHex:0xDEDEDE alpha:0.1].CGColor;
    
    // 阴影
//    bgView.layer.shadowColor = [UIColor blackColor].CGColor;
//    bgView.layer.shadowOffset = CGSizeMake(0, 0);
//    bgView.layer.shadowOpacity = 0.2;
//    bgView.layer.shadowRadius = 100; // 值越大,控制器切换时越卡
    
    
    // 路径阴影
//    UIBezierPath *path = [UIBezierPath bezierPath];
//    float width = bgView.bounds.size.width;
//    float height = bgView.bounds.size.height;
//    float x = bgView.bounds.origin.x;
//    float y = bgView.bounds.origin.y;
//    float addWH = 10;
//    
//    CGPoint topLeft      = bgView.bounds.origin;
//    CGPoint topMiddle = CGPointMake(x+(width/2),y-addWH);
//    CGPoint topRight     = CGPointMake(x+width,y);
//    CGPoint rightMiddle = CGPointMake(x+width+addWH,y+(height/2));
//    CGPoint bottomRight  = CGPointMake(x+width,y+height);
//    CGPoint bottomMiddle = CGPointMake(x+(width/2),y+height+addWH);
//    CGPoint bottomLeft   = CGPointMake(x,y+height);
//    CGPoint leftMiddle = CGPointMake(x-addWH,y+(height/2));
//    [path moveToPoint:topLeft];
//    
//    // 添加四个二元曲线
//    [path addQuadCurveToPoint:topRight
//                 controlPoint:topMiddle];
//    [path addQuadCurveToPoint:bottomRight
//                 controlPoint:rightMiddle];
//    [path addQuadCurveToPoint:bottomLeft
//                 controlPoint:bottomMiddle];
//    [path addQuadCurveToPoint:topLeft
//                 controlPoint:leftMiddle];  
//    // 设置阴影路径
//    bgView.layer.shadowPath = path.CGPath;
    
    bgView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    [loading addSubview:bgView];
    
    // 帧动画
    CGFloat marginTop = (textWidth > 0) ? 8 : 0;
    loading.imageView.center = CGPointMake(bgView.center.x, bgView.center.y - marginTop);
    loading.imageView.bounds = CGRectMake(0, 0, imageHeight, imageHeight);
    loading.imageView.image = [UIImage imageNamed:@"animation_loading_flower"];
    [loading addSubview:loading.imageView];

    // viewDidLoad中调用MMLoading时,由于控制器的视图还没显示,所以动画会无效
    // http://stackoverflow.com/questions/13065593/cabasicanimation-is-not-working-when-the-method-is-called-from-the-viewdidload
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        CABasicAnimation *rotationAnimation;
        rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotationAnimation.toValue = [NSNumber numberWithFloat:M_PI * 2.0];
        rotationAnimation.duration = 1.5;
        rotationAnimation.cumulative = YES;
        rotationAnimation.repeatCount = 100;
        [loading.imageView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    });
    
    
    if (textWidth > 0) {
        UILabel *textLabel = [[UILabel alloc] init];
        textLabel.textAlignment = NSTextAlignmentCenter;
        textLabel.textColor = [UIColor blueColor];
        textLabel.numberOfLines = 1;
        textLabel.font = [UIFont systemFontOfSize:12];
        textLabel.text = text;
        textLabel.center = CGPointMake(bgView.center.x, bgView.center.y + 20);
        textLabel.bounds = CGRectMake(0, 0, textWidth, 20);
        [loading addSubview:textLabel];
    }
    
    [view addSubview:loading];
}

/**
 *  这个方法不能点击导航栏上的按钮
 *
 *  @param text   提示文字
 *  @param window 加载的视图的window
 */
+ (void)showLoadingInWindow:(NSString *)text
{
    UIView *window = [[UIApplication sharedApplication].windows lastObject];
    [MALoading showLoading:text InView:window];
}

/** 自定义位置的菊花 **/
+ (void)showLoadingInView:(UIView *)view withFrame:(CGRect)frame
{
    // 帧动画
    UIImage *image = [UIImage imageNamed:@"animation_loading_flower"];
    MALoading *loading = [MALoading shareLoading];
    loading.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
    CGFloat imageViewW = frame.size.width > 30 ? 30 : frame.size.width;
    loading.imageView.frame = CGRectMake(frame.origin.x, frame.origin.y, imageViewW, imageViewW);
    loading.imageView.image = image;
    [loading addSubview:loading.imageView];
    [view addSubview:loading];
    
    CABasicAnimation *rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat:M_PI * 2.0];
    rotationAnimation.duration = 1.5;
    rotationAnimation.cumulative = YES;
    [loading.imageView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

/**
 *  从view中移除
 *
 *  @param view
 */
+ (void)hideLoading
{
    MALoading *loading = [MALoading shareLoading];
    
    // 恢复scrollView的滚动属性
    if ([loading.superview isKindOfClass:[UIScrollView class]]) {
        UIScrollView *scrollView = (UIScrollView *)loading.superview;
        scrollView.scrollEnabled = (loading.tag == 1);
    }
    
    // 移除动画,防止重复添加
    [loading.imageView.layer removeAllAnimations];
    
    for (UIView *view in loading.subviews) {
        [view removeFromSuperview];
    }
    
    [loading removeFromSuperview];
}

/** 创建子控件 **/
- (void)configUIWithFrame:(CGRect)frame
{
    [self addSubview:self.imageView];
    self.imageView.frame = CGRectMake(frame.size.width / 2, frame.size.height / 2, 30, 30);
}

#pragma mark - getter
- (UIImageView *)imageView
{
    if (_imageView == nil) {
        _imageView = [[UIImageView alloc] init];
        _imageView.center = self.center;
        _imageView.bounds = CGRectMake(0, 0, 30, 30);
        _imageView.image = [UIImage imageNamed:@"animation_loading_flower"];
        
        CABasicAnimation *rotationAnimation;
        rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotationAnimation.toValue = [NSNumber numberWithFloat:M_PI * 2.0];
        rotationAnimation.duration = 1.5;
        rotationAnimation.cumulative = YES;
        rotationAnimation.repeatCount = 100;
        [_imageView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    }
    return _imageView;
}

@end
