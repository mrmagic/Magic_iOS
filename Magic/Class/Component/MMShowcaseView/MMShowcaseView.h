//
//  橱窗展示视图
//
//  Created by zawaliang on 16/6/2.
//  Copyright © 2016年 octech. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MMShowcaseView;


@protocol MMShowcaseViewDelegate <NSObject>
@optional
/**
 *  点击图片
 *
 *  @param showcaseView
 *  @param index
 */
- (void)MMShowcaseView:(MMShowcaseView *)showcaseView cellDidClickAtIndex:(NSUInteger)index;

//滑动到区域
- (void)MMShowcaseViewWithIndex:(NSInteger)index;


@end


@protocol MMShowcaseViewDataSource <NSObject>
@required
/**
 *  返回cell个数
 *
 *  @param showcaseView
 *
 *  @return
 */
- (NSInteger)numberOfViewsInMMShowcaseView:(MMShowcaseView *)showcaseView;

/**
 *  获取指定索引的视图 (不存在重用问题,不建议对相同的index缓存视图,应该都返回新的视图)
 *
 *  @param showcaseView
 *  @param cellIndex  cell内部结构索引,cellIndex>index
 *  @param index 实际索引,对应numberOfViewsInMMShowcaseView的索引
 *
 *  @return
 */
- (UIView *)MMShowcaseView:(MMShowcaseView *)showcaseView viewAtCellIndex:(NSUInteger)cellIndex index:(NSUInteger)index;





@end



@interface MMShowcaseView : UIView
@property (nonatomic, weak) id<MMShowcaseViewDelegate>delegate;
@property (nonatomic, weak) id<MMShowcaseViewDataSource>dataSource; // 自定义视图时需要设置, 设置了数据源则忽略placeholderImage/images属性

@property (nonatomic, assign) BOOL userInteractionEnabledForCell; // 是否允许cell交互,默认YES
@property (nonatomic, assign) BOOL uniformScale; // 等比缩放, 根据frame调整, 默认YES
@property (nonatomic, assign) CGSize size; // cell尺寸, scale=1.0时的尺寸
@property (nonatomic, assign) CGFloat offset; // cell间距, 默认15
@property (nonatomic, assign) CGFloat scale; // 滚动时图片缩放比, 0.0~1.0, 默认1.0; 当<1.0时,居中的图片为第一张;否则左边为第一张
@property (nonatomic, assign) NSTimeInterval interval; // 自动播放间隔(秒), 0时不自动播放, 默认为3.0
@property (nonatomic, assign) CGFloat borderWidth; // 边框宽度,默认无边框
@property (nonatomic, strong) UIColor *borderColor; // 边框颜色
@property (nonatomic, assign) BOOL showCurrentShadow; // 是否显示当前图片阴影,当scale<1.0时有效,默认不显示

@property (nonatomic, strong) UIImage *placeholderImage; // 占位图,默认为image_placeholder_texture

/**
 *  图片列表,请先设置以上属性再调用
 *  支持UIImage或UUID
 */
@property (nonatomic, strong) NSArray *images;

/**
 *  刷新数据(自定义视图时有效,非自定义视图时重新设置images即可)
 */
- (void)reloadData;

/**
 *  开始播放
 */
- (void)start;

/**
 *  停止播放
 */
- (void)stop;

@end
