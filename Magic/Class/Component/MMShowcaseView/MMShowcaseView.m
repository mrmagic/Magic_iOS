//
//  橱窗展示视图
//
//  Created by zawaliang on 16/6/2.
//  Copyright © 2016年 octech. All rights reserved.
//

#import "MMShowcaseView.h"
#import "SDWebImage/UIImageView+WebCache.h"

@interface MMShowcaseView () <UIScrollViewDelegate>
@property (nonatomic, assign) NSInteger fixOffset; // 取整,避免浮点数误差; 可为负数
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) NSUInteger cellCount; // 传入的cell数量
@property (nonatomic, assign) NSUInteger actualCellCount; // 实际的cell数量
@property (nonatomic, assign) CGFloat cellWidth; // 实际计算出来的cell宽度
@end


@implementation MMShowcaseView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.userInteractionEnabledForCell = YES;
    self.placeholderImage = [UIImage imageNamed:@"image_placeholder_texture"];
    self.uniformScale = YES;
    self.scale = 1.0;
    self.offset = 15;
    self.fixOffset = self.offset;
    self.interval = 3;
    self.borderWidth = 0;
    
    self.clipsToBounds = YES;
    [self addSubview:self.scrollView];
}

- (void)dealloc
{
    [self stopTimer];
}

/**
 *  更改frame时重新设置视图
 *
 *  @param frame
 */
- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    if (self.dataSource || self.images.count > 0) {
        [self initCellViews];
    }
}

- (void)reloadData
{
    if (self.dataSource) {
        self.cellCount = [self.dataSource numberOfViewsInMMShowcaseView:self];
        
        if (self.cellCount > 1) {
            self.actualCellCount = (self.scale < 1.0) ? self.cellCount * 4 : self.cellCount * 3;
        } else {
            self.actualCellCount = self.cellCount;
        }
        
        [self initCellViews];
    }
}

- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.pagingEnabled = YES;
        _scrollView.clipsToBounds = NO;
        _scrollView.scrollsToTop = NO;
        _scrollView.delegate = self;
    }
    
    return _scrollView;
}

- (void)setScale:(CGFloat)scale
{
    _scale = MIN(scale, 1.0);
}

- (void)setImages:(NSArray *)images
{
    self.cellCount = images.count;
    
    NSMutableArray *array = [NSMutableArray array];
    [array addObjectsFromArray:[images copy]];
    
    // 图片多于一张时
    if (self.cellCount > 1) {
        [array addObjectsFromArray:[images copy]];
        [array addObjectsFromArray:[images copy]];
        
        // 以左侧为滚动起点,复制到3屏数据 (scale == 1.0)
        // 已中点为滚动起点,复制到4屏数据 (scale < 1.0)
        if (self.scale < 1.0) {
            [array addObjectsFromArray:[images copy]];
        }
    }
    
    _images = [[NSArray alloc] initWithArray:array];
    self.actualCellCount = _images.count;
    
    [self initCellViews];
}

/**
 *  布局时初始化dataSource
 */
- (void)layoutSubviews
{
    if (self.cellCount < 1) {
        [self reloadData];
    }
}

/**
 *  获取指定索引的图片视图
 *
 *  @param index
 *
 *  @return
 */
- (UIImageView *)imageViewAtIndex:(NSUInteger)index frame:(CGRect)frame
{
    CGFloat width = frame.size.width;
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.contentMode = UIViewContentModeScaleToFill;
    imageView.backgroundColor = [UIColor whiteColor];
    imageView.clipsToBounds = YES; // 加了就没阴影了,切换时控制
    
    id image = [self.images objectAtIndex:index];
    if ([image isKindOfClass:[UIImage class]]) {
        imageView.image = (UIImage *)image;
    } else {
        [imageView sd_setImageWithURL:[NSURL URLWithString:image]
                     placeholderImage:[UIImage imageNamed:@""]
                            completed:nil];
    }
    return imageView;
}

/**
 *  获取指定位置的自定义视图
 *
 *  @param index
 *  @param frame
 *
 *  @return
 */
- (UIView *)customViewAtIndex:(NSUInteger)index frame:(CGRect)frame
{
    // 将index转换为实际的索引
    NSUInteger i = index % self.cellCount;
    
    UIView *view = [self.dataSource MMShowcaseView:self viewAtCellIndex:index index:i];
    view.frame = frame;
    
    return view;
}

/**
 *  移除图片列表视图
 */
- (void)removeCells
{
    [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
}

/**
 *  初始化cell列表视图
 */
- (void)initCellViews
{
    // 先移除,防止重复调用图片重叠
    [self removeCells];
    [self stopTimer];
    
    
    CGSize frameSize = self.frame.size;
    
    // 取整,防止浮点数
    NSUInteger width = 0;
    NSUInteger height = 0;
    
    if (self.uniformScale) {
        height = self.frame.size.height;
        width = self.size.width * height / self.size.height;
    } else {
        width = self.size.width;
        height = width * self.size.height / self.size.width;
    }
    
    // 初始化显示的位置
    NSUInteger offsetIndex = 0;
    if (self.cellCount > 1) {
        if (self.scale == 1.0) {
            offsetIndex = self.cellCount;
        } else {
            offsetIndex = self.cellCount * 2;
        }
    }
    
    // scrollView是按正常尺寸来进行翻页的,当需要scale时,造成图片之间的间距加大,这里对offset进行下优化
    // 但最终的offset跟预期还是会有出入
    if (self.scale < 1.0) {
        CGFloat offset = self.offset - (width - width * self.scale) / 2;
//        offset = MAX(offset, 0);
        self.fixOffset = offset;
    } else {
        self.fixOffset = self.offset;
    }
    
    
    CGFloat y = (self.frame.size.height - height) / 2;
    UIView *lastView;
    
    for (NSUInteger i = 0; i < self.actualCellCount; i++) {
        BOOL shouldScale = (self.scale < 1.0 && i != offsetIndex); // 是否需要缩放
        CGFloat x = (self.fixOffset + width) * i;
        CGRect frame = CGRectMake(x, y, width, height);
        
        UIView *view = self.dataSource
            ? [self customViewAtIndex:i frame:frame]
            : [self imageViewAtIndex:i frame:frame];
        
        view.tag = 100 + i;
        
        // 边框
        if (self.borderWidth > 0) {
            view.layer.borderWidth = self.borderWidth;
            view.layer.borderColor = self.borderColor.CGColor;
        }
        
        // 阴影
        if (self.showCurrentShadow) {
            view.layer.shadowColor = [UIColor colorWithHex:0x999999].CGColor;
            view.layer.shadowOffset = CGSizeMake(0, 3);
            view.layer.shadowOpacity = 0;
            view.layer.shadowRadius = 3;
        }
        
        // 缩放
        if (shouldScale) {
            view.transform = CGAffineTransformMakeScale(self.scale, self.scale);
        }
        
        if (self.userInteractionEnabledForCell) {
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickCell:)];
            view.userInteractionEnabled = YES;
            [view addGestureRecognizer:tap];
        }
        
        [self.scrollView addSubview:view];
        
        lastView = view;
    }
    
    self.cellWidth = width;
    self.scrollView.frame = CGRectMake((frameSize.width - width) / 2, 0, width + self.fixOffset, frameSize.height);
    self.scrollView.contentSize = CGSizeMake(CGRectGetMaxX(lastView.frame) + self.fixOffset, frameSize.height);
    
    if (self.cellCount > 1) {
        CGFloat startX = (width + self.fixOffset) * offsetIndex;
        self.scrollView.contentOffset = CGPointMake(startX, 0);
        
        // 自动播放
        [self startTimer];
    } else {
        self.scrollView.contentOffset = CGPointZero;
    }
}

/**
 *  自动播放
 */
- (void)autoplay
{
    CGFloat width = (self.cellWidth + self.fixOffset);
    NSInteger index = self.scrollView.contentOffset.x / width;
    CGFloat x = (index + 1) * width;
    [self.scrollView setContentOffset:CGPointMake(x, 0) animated:YES];
}

- (void)start
{
    if (!self.timer) {
        [self startTimer];
    }
}

- (void)stop
{
    [self stopTimer];
}

/**
 *  启动定时器
 */
- (void)startTimer
{
    // 一个图片时不启动定时器
    if (self.interval > 0 && self.cellCount > 1) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:self.interval
                                                      target:self
                                                    selector:@selector(autoplay)
                                                    userInfo:nil
                                                     repeats:YES];
        
        [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    }
}

/**
 *  停止定时器
 */
- (void)stopTimer
{
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

/**
 *  点击cell
 *
 *  @param tap
 */
- (void)clickCell:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(MMShowcaseView:cellDidClickAtIndex:)]) {
        UIView *view = tap.view;
        NSUInteger tag = view.tag - 100;
        NSUInteger index = tag % self.cellCount;
        
        [self.delegate MMShowcaseView:self cellDidClickAtIndex:index];
    }
}


#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([self.delegate respondsToSelector:@selector(MMShowcaseViewWithIndex:)]) {
        NSInteger index = (self.scrollView.contentOffset.x + kscreenWidth / 2) / kscreenWidth;
        [self.delegate MMShowcaseViewWithIndex:index % self.cellCount];
    }
    if (scrollView == self.scrollView && self.cellCount > 1) {
        CGFloat offsetX = CGRectGetMinX(self.scrollView.frame); // scrollView左侧距离父视图的位置
        CGFloat x = scrollView.contentOffset.x;
        CGFloat itemWidth = self.cellWidth + self.fixOffset;
        CGFloat groupWidth = self.cellCount * itemWidth; // 每一组数据的长度
        
        
        // 缩放效果
        UIView *view;
        UIView *prevView;
        UIView *nextView;
        
        if (self.scale < 1.0) {
            CGFloat preScale = (1 - self.scale) / itemWidth;
            NSUInteger index = x / itemWidth;
            
            // 细分到每个缩放点的透明度
            // 上限为0.2
            CGFloat opacity = self.showCurrentShadow ? (0.2 / (1 - self.scale)) : 0;
            
            // 当前张
            NSUInteger tag = 100 + index;
            CGFloat scale = 1 - preScale * (x - index * itemWidth);
            view = [self.scrollView viewWithTag:tag];
            view.transform = CGAffineTransformMakeScale(scale, scale);
            view.clipsToBounds = NO;
            view.layer.shadowOpacity = opacity * fabs(scale - self.scale);
            
            // 上一张
            prevView = [self.scrollView viewWithTag:tag - 1];
            CGFloat prevScale = self.scale; // self.scale + preScale * (x - itemWidth - (index - 1) * itemWidth);
            prevView.transform = CGAffineTransformMakeScale(prevScale, prevScale);
            prevView.clipsToBounds = NO;
            
            // 下一张
            nextView = [self.scrollView viewWithTag:tag + 1];
            CGFloat nextScale = self.scale + preScale * (x + itemWidth - (index + 1) * itemWidth);
            nextView.transform = CGAffineTransformMakeScale(nextScale, nextScale);
            nextView.clipsToBounds = NO;
            nextView.layer.shadowOpacity = opacity * fabs(nextScale - self.scale);
        }
        
        if (x < offsetX + itemWidth) { // 右滑快到底
            // 防止下次过渡时开始值为1
            if (view) {
                view.transform = CGAffineTransformMakeScale(self.scale, self.scale);
                prevView.transform = CGAffineTransformMakeScale(self.scale, self.scale);
                nextView.transform = CGAffineTransformMakeScale(self.scale, self.scale);
            }
            
            x += groupWidth;
            scrollView.contentOffset = CGPointMake(x, 0);
        } else if (scrollView.contentSize.width - x - itemWidth < offsetX + itemWidth) { // 左滑快到底
            if (view) {
                view.transform = CGAffineTransformMakeScale(self.scale, self.scale);
                prevView.transform = CGAffineTransformMakeScale(self.scale, self.scale);
                nextView.transform = CGAffineTransformMakeScale(self.scale, self.scale);
            }
            
            x -= groupWidth;
            scrollView.contentOffset = CGPointMake(x, 0);
        }
    }
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self stopTimer];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self startTimer];
}


/**
 *  让超出scrollView的位置也可以滑动
 *
 *  @param point
 *  @param event
 *
 *  @return
 */
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
//    hitTest:withEvent:方法的处理流程如下:
//    
//    首先调用当前视图的pointInside:withEvent:方法判断触摸点是否在当前视图内；
//    若返回NO,则hitTest:withEvent:返回nil;
//    若返回YES,则向当前视图的所有子视图(subviews)发送hitTest:withEvent:消息，所有子视图的遍历顺序是从top到bottom，即从subviews数组的末尾向前遍历,直到有子视图返回非空对象或者全部子视图遍历完毕；
//    若第一次有子视图返回非空对象,则hitTest:withEvent:方法返回此对象，处理结束；
//    如所有子视图都返回非，则hitTest:withEvent:方法返回自身(self)。
//    hitTest:withEvent:方法忽略隐藏(hidden=YES)的视图,禁止用户操作(userInteractionEnabled=YES)的视图，以及alpha级别小于0.01(alpha<0.01)的视图。如果一个子视图的区域超过父视图的bound区域(父视图的clipsToBounds 属性为NO,这样超过父视图bound区域的子视图内容也会显示)，那么正常情况下对子视图在父视图之外区域的触摸操作不会被识别,因为父视图的pointInside:withEvent:方法会返回NO,这样就不会继续向下遍历子视图了。当然，也可以重写pointInside:withEvent:方法来处理这种情况
    
    
    UIView *view = [super hitTest:point withEvent:event];
    
    if ([view isEqual:self]) {
        for (UIView *subview in self.scrollView.subviews) {
            CGFloat x = point.x - self.scrollView.frame.origin.x + self.scrollView.contentOffset.x - subview.frame.origin.x;
            CGFloat y = point.y - self.scrollView.frame.origin.y + self.scrollView.contentOffset.y - subview.frame.origin.y;
            CGPoint offset = CGPointMake(x, y);
            
            if ((view = [subview hitTest:offset withEvent:event])) {
                return view;
            }
        }
        
        return self.scrollView;
    }
    
    return view;
}

@end
