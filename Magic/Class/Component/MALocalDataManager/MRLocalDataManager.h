//
//  本地缓存
//

#import <Foundation/Foundation.h>

@interface MRLocalDataManager : NSObject

/**
 *  保存可归档的装有自定义Model数组
 *
 *  @param array
 *  @param fileName
 */
+ (void)saveArray:(NSArray *)array fileName:(NSString *)fileName;

/**
 *  获取归档的数组
 *
 *  @param fileName 文件名
 *
 *  @return array
 */
+ (NSMutableArray *)arrayWithFileName:(NSString *)fileName;


/**
 *  JSON缓存
 *
 *  @param json
 *  @param name
 */
+ (void)saveJsonDictionary:(NSDictionary *)json fileName:(NSString *)fileName;

/**
 *  获取JSON缓存
 *
 *  @param fileName
 *
 *  @return
 */
+ (NSDictionary *)jsonDictionaryWithFileName:(NSString *)fileName;

@end
