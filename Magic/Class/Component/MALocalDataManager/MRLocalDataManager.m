//
//  本地缓存
//

#import "MRLocalDataManager.h"


@implementation MRLocalDataManager

/**
 *  数组归档
 *
 *  @param modelArray
 *  @param fileName
 */
+ (void)saveArray:(NSArray *)array fileName:(NSString *)fileName
{
    NSString *MMfilePath = [self filePathWithStr:fileName];
    BOOL isExist = [[NSFileManager defaultManager] fileExistsAtPath:fileName];
    
    if (isExist) {
        [[NSFileManager defaultManager] removeItemAtPath:MMfilePath error:nil];
    }
    
    [NSKeyedArchiver archiveRootObject:array toFile:MMfilePath];
}

/**
 *  数组解归档
 *
 *  @param fileName
 *
 *  @return
 */
+ (NSMutableArray *)arrayWithFileName:(NSString *)fileName
{
    NSString *filePath = [self filePathWithStr:fileName];
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:0];
    BOOL isExist = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    
    if (isExist) {
        array = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    }
    return array;
}

/**
 *  存的文件路径
 *
 *  @param fileStr 文件名
 *
 *  @return 文件路径
 */
+ (NSString *)filePathWithStr:(NSString *)fileStr
{
    NSString *tmpDir = NSTemporaryDirectory();
    NSString *filePath = [tmpDir stringByAppendingString:fileStr];
    
    return filePath;
}

+ (void)saveJsonDictionary:(NSDictionary *)json fileName:(NSString *)fileName
{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:nil];
    NSString *filePath = [self filePathWithStr:fileName];
    BOOL fileExist = [[NSFileManager defaultManager] fileExistsAtPath:fileName];
    
    if (fileExist) {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }

    [jsonData writeToFile:filePath atomically:YES];
}

+ (NSDictionary *)jsonDictionaryWithFileName:(NSString *)fileName
{
    NSString *filePath = [self filePathWithStr:fileName];
    NSData *data = [[NSData alloc]initWithContentsOfFile:filePath];
    NSDictionary *dictionary = nil;
    
    if (data != nil) {
        dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    }
    
    return dictionary;
}

@end
