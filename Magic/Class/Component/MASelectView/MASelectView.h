//
//  MASelectView.h
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MASelectViewDelegate <NSObject>

@optional

- (void)MASelectViewIndexChangeWithIndex:(NSInteger)index;

@end

@interface MASelectView : UIView

@property (nonatomic, assign) id<MASelectViewDelegate> delegate;

@property (nonatomic, copy) NSArray *titleArray;
@property (nonatomic, assign) CGFloat space;

- (void)update;

- (void)updateToSelectIndex:(NSInteger)index;

@end
