//
//  MASelectView.m
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MASelectView.h"

@interface MASelectView ()

@property (nonatomic, strong) UIView *lineView;

@end

@implementation MASelectView

- (void)buttonClick:(UIButton *)sender
{
    for (int i = 0 ; i < self.titleArray.count; i++) {
        UIButton *button = [self viewWithTag:1000 + i];
        if (button.tag == sender.tag) {
            [button setTitleColor:MRMainColor forState:UIControlStateNormal];
            [UIView animateWithDuration:0.2 animations:^{
                self.lineView.frame = CGRectMake(button.frame.origin.x - 10, self.frame.size.height - 1, button.frame.size.width + 20, 1);
            }];
        } else {
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
    }
    if ([self.delegate respondsToSelector:@selector(MASelectViewIndexChangeWithIndex:)]) {
        [self.delegate MASelectViewIndexChangeWithIndex:sender.tag - 1000];
    }
}

- (void)updateToSelectIndex:(NSInteger)index
{
    if (index <= self.titleArray.count - 1) {
        for (int i = 0 ; i < self.titleArray.count; i++) {
            UIButton *button = [self viewWithTag:1000 + i];
            if (button.tag == 1000 + index) {
                [button setTitleColor:MRMainColor forState:UIControlStateNormal];
                [UIView animateWithDuration:0.2 animations:^{
                    self.lineView.frame = CGRectMake(button.frame.origin.x - 10, self.frame.size.height - 1, button.frame.size.width + 20, 1);
                }];
            } else {
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            }
        }
    }
}

- (void)update
{
    if ([self subviews].count > 0) {
        for (UIView *view in [self subviews]) {
            [view removeFromSuperview];
        }
    }
    
    [self addSubview:self.lineView];
    
    CGFloat viewWidth = 0;
    for (int i = 0 ; i < self.titleArray.count; i++) {
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:18];
        label.text = [self.titleArray objectAtIndex:i];
        CGFloat width =[label widthForLabelWithHeight:18];
        viewWidth = width + viewWidth;
    }
    CGFloat step = (self.frame.size.width - 2 * self.space - viewWidth) / (self.titleArray.count - 1);
    
    for (int i = 0 ; i < self.titleArray.count; i++) {
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:18];
        label.text = [self.titleArray objectAtIndex:i];
        CGFloat width =[label widthForLabelWithHeight:18];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = 1000 + i;
        [button setTitle:[self.titleArray objectAtIndex:i] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        if (i == 0) {
            button.frame = CGRectMake(self.space, 0, width, self.frame.size.height - 0.5);
            self.lineView.frame = CGRectMake(self.space - 10, self.frame.size.height - 1, width + 20, 1);
            [button setTitleColor:MRMainColor forState:UIControlStateNormal];

        } else {
            UIButton *newbutton = [self viewWithTag:1000 + i - 1];
            button.frame = CGRectMake(CGRectGetMaxX(newbutton.frame) + step, 0, width, self.frame.size.height - 1);
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

        }
        [self addSubview:button];
    }
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = MRMainColor;
    }
    return _lineView;
}



@end
