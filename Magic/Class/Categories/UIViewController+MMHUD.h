//
//  HUD（透明指示层）的分类
//
//  Created by YangFaXian on 15/7/9.
//  Copyright (c) 2015年 octech. All rights reserved.
//  

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"


@interface UIViewController (MMHUD)

#pragma mark - Toast
/**
 *  显示纯文本HUD, 默认2秒后消失
 *
 *  @param text 需要显示的文本 (不能为nil, 否则不显示)
 */
- (void)showToast:(NSString *)text;



#pragma mark - Success
/**
 *  显示成功HUD, 默认2秒后消失
 *
 *  @param text 成功文本
 */
- (void)showSuccess:(NSString *)text;


#pragma mark - Error
/**
 *  显示错误HUD, 默认2秒后消失
 *
 *  @param text 失败文本
 */
- (void)showError:(NSString *)text;


#pragma mark - Loading
/**
 *  显示Loading HUD到self.view, 不会锁全屏 (此方法需要手动调用hideHUD来隐藏)
 *
 *  注意：如果出现HUD被遮挡的问题，就在视图层级改变后调用bringHUDToFront方法
 *
 *  @param text 加载时的文本
 */
- (void)showLoading:(NSString *)text;

/**
 *   显示Loading HUD到keyWindow, 会锁全屏 (此方法需要手动调用hideHUD来隐藏)
 *
 *  @param text 加载时的文本
 */
- (void)showLoadingInWindow:(NSString *)text;


#pragma mark - Hide
/**
 *  隐藏HUD，所有的HUD都用此方法隐藏
 */
- (void)hideHUD;


@end
