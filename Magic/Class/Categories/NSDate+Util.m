//
//  NSDate+Util.m
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import "NSDate+Util.h"

@implementation NSDate (Util)

+(NSString *)timeWithUnixTimestamp:(NSTimeInterval)timestamp formatter:(NSDateFormatter *)formatter
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];

    NSString *timeStr = [formatter stringFromDate:date];
    return timeStr;
}
+ (NSString *)timeWeekWithUnixTimestamp:(NSTimeInterval)timestamp
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    
    //计算星期
    NSArray *weekArray = [NSArray arrayWithObjects:@"星期日", @"星期一", @"星期二", @"星期三", @"星期四", @"星期五", @"星期六", nil];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *comps = [calendar components:unitFlags fromDate:date];
    NSInteger week = [comps weekday]; // 1基,1表示星期日
    NSString *weekDay = [NSString stringWithFormat:@"%@", [weekArray objectAtIndex:week - 1]];
    return weekDay;
}

@end
