//
//  NSURL+Util.h
//  iphoneLive
//
//  Created by Ty on 2016/8/18.
//  Copyright © 2016年 cat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Util)

/**
 *  获取接口URL
 *
 *  @param sever 服务器地址
 *  @param parameters 参数
 *
 *  @return
 */
+ (NSString *)stringURLWithSever:(NSString *)sever
                      parameters:(id)parameters;



/**
 *  获取接口URL
 *
 *  @param sever 服务器地址
 *  @param authentication 鉴权
 *  @param parameters 参数
 *
 *  @return
 */
+ (NSString *)stringURLWithSever:(NSString *)sever
                  authentication:(BOOL)authentication
                      parameters:(id)parameters;


@end
