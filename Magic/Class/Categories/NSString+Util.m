//
//  NSString+Util.m
//  Magic
//
//  Created by 王 on 16/9/27.
//  Copyright © 2016年 王. All rights reserved.
//

#import "NSString+Util.h"

@implementation NSString (Util)

/**
 *  是否正确的手机号
 *
 *  @param mobile
 *
 *  @return
 */
+ (BOOL)isMobile:(NSString *)mobile
{
    NSString *re = @"^1[0-9]{10}$";
    
    NSPredicate *reg = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", re];
    BOOL result = [reg evaluateWithObject:mobile];
    return result;
}

/**
 *  是否正确的密码格式(6~12位)
 *
 *  @param password
 *
 *  @return
 */
+ (BOOL)isPassword:(NSString *)password
{
    if (password.length >= 6 && password.length <= 12) {
        return YES;
    }
    return NO;
}

/**
 *  是否正确的验证码格式(6~12位)
 *
 *  @param password
 *
 *  @return
 */
+ (BOOL)isCode:(NSString *)code
{
    if (code.length == 6) {
        return YES;
    }
    return NO;
}

@end
