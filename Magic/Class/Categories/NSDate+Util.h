//
//  NSDate+Util.h
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Util)


+(NSString *)timeWithUnixTimestamp:(NSTimeInterval)timestamp formatter:(NSDateFormatter *)formatter;


+ (NSString *)timeWeekWithUnixTimestamp:(NSTimeInterval)timestamp;

@end
