//
//  HUD（透明指示层）的分类
//
//  Created by YangFaXian on 15/7/9.
//  Copyright (c) 2015年 octech. All rights reserved.
//

#import "UIViewController+MMHUD.h"
#import "MALoading.h"


@implementation UIViewController (MMHUD)

#pragma mark - Toast
- (void)showToast:(NSString *)text
{
    [self showToast:text duration:2.0f];
}

- (void)showToast:(NSString *)text duration:(NSTimeInterval)duration
{
    if (text == nil || text.length == 0) {
        return;
    }
    
    CGFloat padding = 4; // 此值同MBProgressHUD里的kPadding
    CGFloat margin = 10;
    
    
    // 1.创建包装HUD的容器View (不可见)
    UILabel *label = [[UILabel alloc] init];
    label.font = [UIFont systemFontOfSize:14];
    label.text = text;
    CGFloat textHeight = [label heightForLabelWithWidth:CGRectGetWidth(self.view.frame) - 4 * margin];
    
    CGFloat hudheight = textHeight + 20 + 10;
    
    // HACK: 由于HUB内部限制了文本不换行,这里计算实际的文本高度与HUB内部计算的高度差,弥补此差值即可换行
    CGFloat HUBTextHeight = hudheight - padding - 4 * margin; // HUB内部计算文本高度的算法,其算法取父容器的高度进行计算
    if (HUBTextHeight < textHeight) {
        hudheight += (textHeight - HUBTextHeight);
    }
    
    
    CGFloat contentY = CGRectGetHeight(self.view.frame) * 0.5 - hudheight * 0.5;
    if (self.navigationController) {
        contentY -= (64) * 0.5;
    }
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                   contentY,
                                                                   CGRectGetWidth(self.view.frame),
                                                                   hudheight)];
    [self.view addSubview:contentView];
    
    
    // 2.创建HUD
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:contentView];
    hud.mode = MBProgressHUDModeText;
    hud.detailsLabelText = text;
    hud.detailsLabelFont = [UIFont systemFontOfSize:14];
    hud.cornerRadius = 4.0f;
    hud.margin = margin;
    hud.removeFromSuperViewOnHide = YES;
    [contentView addSubview:hud];
    
    // 3.显示HUD
    [hud showAnimated:YES whileExecutingBlock:^{
        sleep(duration);
    } completionBlock:^{
        [contentView removeFromSuperview];
    }];
}


#pragma mark - Success
- (void)showSuccess:(NSString *)text
{
    [self showSuccess:text duration:2.0f];
}

- (void)showSuccess:(NSString *)text duration:(NSTimeInterval)duration
{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_success_white"]];
    [self showCustomView:imageView text:text duration:duration];
}

#pragma mark - Error
- (void)showError:(NSString *)text
{
    [self showError:text duration:2.0];
}

- (void)showError:(NSString *)text duration:(NSTimeInterval)duration
{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_error_white"]];
    [self showCustomView:imageView text:text duration:duration];
}


#pragma mark - Loading
- (void)showLoading:(NSString *)text
{
    [self showLoading:text lockScreen:NO];
}

- (void)showLoadingInWindow:(NSString *)text
{
    [self showLoading:text lockScreen:YES];
}


- (void)showLoading:(NSString *)text lockScreen:(BOOL)flag
{
    if (flag == YES) {
        [MALoading showLoadingInWindow:nil];
    }  else {
        [MALoading showLoading:nil InView:self.view];
    }
    
}

#pragma mark - Hide
- (void)hideHUD
{ 
    // 默认MBProgressHUD
    // 1.遍历隐藏View上的所有HUD
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    // 2.遍历隐藏Window上的所有HUD
    [MBProgressHUD hideAllHUDsForView:[[UIApplication sharedApplication].windows lastObject] animated:YES];
    
    // MM自定义HUD
    [MALoading hideLoading];
}

#pragma mark - Other
- (void)bringHUDToFront
{
    if (self.view.subviews.count <= 1) { return; }
    
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[MBProgressHUD class]]) {
            [self.view bringSubviewToFront:view];
        }
    }
}

#pragma mark - Privite Methods
- (void)showCustomView:(UIView *)customView
                  text:(NSString *)text
              duration:(NSTimeInterval)duration
{
    // 1.创建包装HUD的容器View (不可见)
    UILabel *label = [[UILabel alloc] init];
    label.font = [UIFont systemFontOfSize:14];
    label.text = text;
    CGFloat textHeight = [label heightForLabelWithWidth:CGRectGetWidth(self.view.frame)];
    
    CGFloat viewHeight = customView.frame.size.height;
    CGFloat hudheight = textHeight + viewHeight + 20 + 40;
    CGFloat contentY = CGRectGetHeight(self.view.frame) * 0.5 - hudheight * 0.5;
    if (self.navigationController) {
        contentY -= (64) * 0.5;
    }
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                   contentY,
                                                                   CGRectGetWidth(self.view.frame),
                                                                   hudheight)];
    [self.view addSubview:contentView];
    
    
    // 2.创建HUD
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:contentView]; 
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = customView;
    hud.detailsLabelText = text;
    hud.detailsLabelFont = [UIFont systemFontOfSize:14];
    hud.cornerRadius = 4.0f;
    hud.margin = 10;
    hud.removeFromSuperViewOnHide = YES;
    [contentView addSubview:hud];
    
    // 3.显示HUD
    [hud showAnimated:YES whileExecutingBlock:^{
        sleep(duration);
    } completionBlock:^{
        [contentView removeFromSuperview];
    }];
}

@end
