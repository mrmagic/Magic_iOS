//
//  UILabel+Util.h
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Util)

/**
 *  改变Label内部部分字体和颜色
 *
 *  @param font  字体
 *  @param color 颜色
 *  @param range 指定的区域
 */
- (void)changeLabelFont:(id)font color:(UIColor *)color inRange:(NSRange)range;

/**
 *  自适应宽度
 *
 *  @param width 宽度
 *
 *  @return 返回高度值 自动换行
 */
- (CGFloat)heightForLabelWithWidth:(CGFloat)width;

/**
 *  自适应宽度
 *
 *  @param height 高度
 *
 *  @return 返回宽度值 不换行
 */
- (CGFloat)widthForLabelWithHeight:(CGFloat)height;

/**
 *  设置行间距 根据已有的text
 *
 *  @param lineSpacing 行距
 *  @param width       宽度
 *
 *  @return 高度
 */
- (CGFloat)textHeightForLineSpacing:(CGFloat)lineSpacing width:(CGFloat)width;

/**
 *  设置行间距 根据已有的attributedText
 *
 *  @param lineSpacing 行距
 *  @param width       宽度
 *
 *  @return 高度
 */
- (CGFloat)attributedTextHeightForLineSpacing:(CGFloat)lineSpacing width:(CGFloat)width;



@end
