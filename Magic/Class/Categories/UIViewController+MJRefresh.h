//
//  MJRefresh扩展
//
//  Created by zawaliang on 15/6/15.
//  Copyright (c) 2015年 octech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MJRefresh/MJRefresh.h>

@interface UIViewController (MJRefresh)

/**
 *  简化MJRefresh(下拉刷新)
 *
 *  @param tableView
 *  @param target
 *  @param refreshingAction 下拉刷新句柄
 */
- (void)initMJRefreshWithScrollView:(UIScrollView *)scrollview
                             target:(id)target
                   refreshingAction:(SEL)refreshingAction;

/**
 *  简化MJRefresh(上拉加载更多)
 *
 *  @param tableView
 *  @param target
 *  @param pageAction 上滑翻页句柄
 */
- (void)initMJRefreshWithScrollView:(UIScrollView *)scrollview
                             target:(id)target
                         pageAction:(SEL)pageAction;

/**
 *  简化MJRefresh(上下拉刷新)
 *
 *  @param tableView
 *  @param target
 *  @param refreshingAction 下拉刷新句柄
 *  @param pageAction 上滑翻页句柄, nil时没有翻页
 */
- (void)initMJRefreshWithScrollView:(UIScrollView *)scrollview
                            target:(id)target
                  refreshingAction:(SEL)refreshingAction
                        pageAction:(SEL)pageAction;

/**
 *  简化MJRefresh(上下拉刷新)
 *
 *  @param tableView
 *  @param target
 *  @param options
     {
         @"headerIdle":@"点击加载更多",
         @"headerPulling":@"松开加载...",
         @"headerRefreshing":@"加载中...",
 
         @"footerIdle":@"点击加载更多",
         @"footerRefreshing":@"加载中...",
         @"footerNoMoreData":@"没有数据了...",
     }
 *  @param refreshingAction 下拉刷新句柄
 *  @param pageAction 上滑翻页句柄, nil时没有翻页
 */
- (void)initMJRefreshWithScrollView:(UIScrollView *)scrollview
                             target:(id)target
                            options:(NSDictionary *)options
                   refreshingAction:(SEL)refreshingAction
                         pageAction:(SEL)pageAction;

@end