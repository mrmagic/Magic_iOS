//
//  MJRefresh扩展
//
//  Created by zawaliang on 15/6/15.
//  Copyright (c) 2015年 octech. All rights reserved.
//

#import "UIViewController+MJRefresh.h"


@interface MMRefreshHeader : MJRefreshHeader
@property (weak, nonatomic) UILabel *label;
@property (weak, nonatomic) UIActivityIndicatorView *loading;
@property (weak, nonatomic) NSString *idleString;
@property (weak, nonatomic) NSString *pullingString;
@property (weak, nonatomic) NSString *refreshingString;
@end


@implementation MMRefreshHeader
// 自定义动画
//- (void)prepare
//{
//    [super prepare];
//    
//    // 设置普通状态的动画图片
//    NSMutableArray *idleImages = [NSMutableArray array];
//    for (NSUInteger i = 1; i <= 8; i++) {
//        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"animation_dropdown_refresh%zd", i]];
//        [idleImages addObject:image];
//    }
//    [self setImages:idleImages forState:MJRefreshStateIdle];
//    
//    // 设置即将刷新状态的动画图片（一松开就会刷新的状态）
//    NSMutableArray *refreshingImages = [NSMutableArray array];
//    for (NSUInteger i = 1; i <= 8; i++) {
//        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"animation_dropdown_refresh%zd", i]];
//        [refreshingImages addObject:image];
//    }
//    [self setImages:refreshingImages forState:MJRefreshStatePulling];
//    
//    // 设置正在刷新状态的动画图片
//    [self setImages:refreshingImages forState:MJRefreshStateRefreshing];
//    
//    // 隐藏时间
//    self.lastUpdatedTimeLabel.hidden = YES;
//    
//    // 设置文字
//    [self setTitle:@"下拉刷新..." forState:MJRefreshStateIdle];
//    [self setTitle:@"松开加载..." forState:MJRefreshStatePulling];
//    [self setTitle:@"正在载入..." forState:MJRefreshStateRefreshing];
//    self.stateLabel.font      = [UIFont systemFontOfSize:14];
//    self.stateLabel.textColor = MMTextColor;
//    
//}
- (void)prepare
{
    [super prepare];
    
    // 控件的高度
    self.mj_h = 44;
    
    // label
    UILabel *label = [[UILabel alloc] init];
    label.textColor = MRTextColor;
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:label];
    self.label = label;
    
    // loading
    UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self addSubview:loading];
    self.loading = loading;
}

- (void)placeSubviews
{
    [super placeSubviews];
    
    self.label.frame = self.bounds;
    
    self.loading.center = CGPointMake(self.mj_w * 0.5 - 55, self.mj_h * 0.5);
}

- (void)setState:(MJRefreshState)state
{
    MJRefreshCheckState;
    
    switch (state) {
        case MJRefreshStateIdle:
            [self.loading stopAnimating];
            self.label.text = self.idleString ? self.idleString : @"加载更多精彩";
            break;
        case MJRefreshStatePulling:
            [self.loading stopAnimating];
            self.label.text = self.pullingString ? self.pullingString : @"加载更多精彩";
            break;
        case MJRefreshStateRefreshing:
            self.label.text = self.refreshingString ? self.refreshingString : @"正在载入...";
            [self.loading startAnimating];
            break;
        default:
            break;
    }
}
@end



@interface MMRefreshFooter : MJRefreshAutoFooter
@property (weak, nonatomic) UILabel *label;
@property (weak, nonatomic) UIActivityIndicatorView *loading;
@property (weak, nonatomic) NSString *idleString;
@property (weak, nonatomic) NSString *refreshingString;
@property (weak, nonatomic) NSString *noMoreDataString;
@end


@implementation MMRefreshFooter
- (void)prepare
{
    [super prepare];
    
    // 控件高度
    self.mj_h = 50;
    
    // label
    UILabel *label = [[UILabel alloc] init];
    label.textColor = MRTextColor;
    label.font = [UIFont systemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:label];
    self.label = label;
    
    // loading
    UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self addSubview:loading];
    self.loading = loading;
}

- (void)placeSubviews
{
    [super placeSubviews];
    
    self.label.frame = self.bounds;
    
    self.loading.center = CGPointMake(self.mj_w * 0.5 - 60, self.mj_h * 0.5);
}

- (void)setState:(MJRefreshState)state
{
    MJRefreshCheckState;
    
    switch (state) {
        case MJRefreshStateIdle:
            self.label.text = self.idleString ? self.idleString : @"到底啦,一起期待更多内容吧";
            [self.loading stopAnimating];
            break;
        case MJRefreshStateRefreshing:
            self.label.text = self.refreshingString ? self.refreshingString : @"正在载入...";
            [self.loading startAnimating];
            break;
        case MJRefreshStateNoMoreData:
            self.label.text = self.noMoreDataString ? self.noMoreDataString : @"没有数据了";
            [self.loading stopAnimating];
            break;
        default:
            break;
    }
}
@end



@implementation UIViewController (MJRefresh)

- (void)initMJRefreshWithScrollView:(UIScrollView *)scrollview
                             target:(id)target
                   refreshingAction:(SEL)refreshingAction
{
    return [self initMJRefreshWithScrollView:scrollview target:target refreshingAction:refreshingAction pageAction:nil];
}


- (void)initMJRefreshWithScrollView:(UIScrollView *)scrollview
                             target:(id)target
                         pageAction:(SEL)pageAction
{
    return [self initMJRefreshWithScrollView:scrollview target:target refreshingAction:nil pageAction:pageAction];
}

- (void)initMJRefreshWithScrollView:(UIScrollView *)scrollview
                             target:(id)target
                   refreshingAction:(SEL)refreshingAction
                         pageAction:(SEL)pageAction
{
    [self initMJRefreshWithScrollView:scrollview
                               target:target
                              options:nil
                     refreshingAction:refreshingAction
                           pageAction:pageAction];
}

- (void)initMJRefreshWithScrollView:(UIScrollView *)scrollview
                             target:(id)target
                            options:(NSDictionary *)options
                   refreshingAction:(SEL)refreshingAction
                         pageAction:(SEL)pageAction
{
    
    // 头部
    if (refreshingAction != nil) {
        MMRefreshHeader *header = [MMRefreshHeader headerWithRefreshingTarget:target refreshingAction:refreshingAction];
        
        if ([options isKindOfClass:[NSDictionary class]]) {
            header.idleString = [options objectForKey:@"headerIdle"];
            header.pullingString = [options objectForKey:@"headerPulling"];
            header.refreshingString = [options objectForKey:@"headerRefreshing"];
        }
        
        scrollview.mj_header = header;
    } else {
        [scrollview.mj_header setHidden:YES];
    }
    
    
    if (pageAction != nil) {
        MMRefreshFooter *footer = [MMRefreshFooter footerWithRefreshingTarget:target refreshingAction:pageAction];
        
        if ([options isKindOfClass:[NSDictionary class]]) {
            footer.idleString = [options objectForKey:@"footerIdle"];
            footer.refreshingString = [options objectForKey:@"footerRefreshing"];
            footer.noMoreDataString = [options objectForKey:@"footerNoMoreData"];
        }
        
        scrollview.mj_footer = footer;
        
        // footer默认隐藏,业务根据是否可以分页做处理
        [scrollview.mj_footer setHidden:YES];
    } else {
        [scrollview.mj_footer setHidden:YES];
    }
}


@end





