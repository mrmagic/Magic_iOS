//
//  NSURL+Util.m
//  iphoneLive
//
//  Created by Ty on 2016/8/18.
//  Copyright © 2016年 cat. All rights reserved.
//

#import "NSURL+Util.h"

@implementation NSURL (Util)


/**
 *  获取接口URL
 *
 *  @param sever 服务器地址
 *  @param parameters 参数
 *
 *  @return
 */
+ (NSString *)stringURLWithSever:(NSString *)sever
                      parameters:(id)parameters
{
    if ([MRUser sharedClient].isLogin) {
        return  [self stringURLWithSever:sever authentication:YES parameters:parameters];
    } else {
        return  [self stringURLWithSever:sever authentication:NO parameters:parameters];
    }
}


/**
 *  获取接口URL
 *
 *  @param sever 服务器地址
 *  @param sever 服务器地址
 *  @param parameters 参数
 *
 *  @return
 */
+ (NSString *)stringURLWithSever:(NSString *)sever
                  authentication:(BOOL)authentication
                      parameters:(id)parameters
{
    NSString *url =  [NSString stringWithFormat:@"%@?service=%@", purl,sever];
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:1];
    if (authentication) {
        [dic setValue:[MRUser sharedClient].ID forKey:@"uid"];
        [dic setValue:[MRUser sharedClient].token forKey:@"token"];
    } else {
        [dic setValue:@"0" forKey:@"uid"];
        [dic setValue:@"0" forKey:@"token"];
    }
    for (NSString *key in [dic allKeys]) {
        NSString *values = [dic objectForKey:key];
        url = [NSString stringWithFormat:@"%@&%@=%@", url, key, values];
    }
    for (NSString *key in [parameters allKeys]) {
        NSString *values = [parameters objectForKey:key];
        url = [NSString stringWithFormat:@"%@&%@=%@", url, key, values];
    }
    return url;
}



@end
