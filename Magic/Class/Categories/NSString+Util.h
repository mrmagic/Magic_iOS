//
//  NSString+Util.h
//  Magic
//
//  Created by 王 on 16/9/27.
//  Copyright © 2016年 王. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Util)

/**
 *  是否正确的手机号
 *
 *  @param mobile
 *
 *  @return
 */
+ (BOOL)isMobile:(NSString *)mobile;

/**
 *  是否正确的密码格式(6~12位)
 *
 *  @param password
 *
 *  @return
 */
+ (BOOL)isPassword:(NSString *)password;

/**
 *  是否正确的验证码格式(6位)
 *
 *  @param password
 *
 *  @return
 */
+ (BOOL)isCode:(NSString *)code;

@end
