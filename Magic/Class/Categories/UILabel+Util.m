//
//  UILabel+Util.m
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import "UILabel+Util.h"

@implementation UILabel (Util)

- (void)changeLabelFont:(id)font color:(UIColor *)color inRange:(NSRange)range
{
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:self.text];
    [str addAttribute:NSFontAttributeName value:font range:range];
    [str addAttribute:NSForegroundColorAttributeName value:color range:range];
    
    self.attributedText = str;
}

/**
 *  自适应宽度
 *
 *  @param width 宽度
 *
 *  @return 返回高度值 自动换行
 */
- (CGFloat)heightForLabelWithWidth:(CGFloat)width
{
    self.numberOfLines = 0;
    
    NSDictionary *attribute = @{NSFontAttributeName:self.font};
    
    CGRect singRect = [self.text boundingRectWithSize:CGSizeMake(width, 0) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil];
    
    return singRect.size.height;
}

/**
 *  自适应宽度
 *
 *  @param height 高度
 *
 *  @return 返回宽度值 不换行
 */
- (CGFloat)widthForLabelWithHeight:(CGFloat)height
{
    NSDictionary *attribute = @{NSFontAttributeName:self.font};
    
    CGRect singRect = [self.text boundingRectWithSize:CGSizeMake(0, height) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil];
    
    return singRect.size.width;
}

/**
 *  设置行间距 根据已有的text
 *
 *  @param lineSpacing 行距
 *  @param width       宽度
 *
 *  @return 高度
 */
- (CGFloat)textHeightForLineSpacing:(CGFloat)lineSpacing width:(CGFloat)width
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 0)];
    
    label.numberOfLines = 0;
    
    NSDictionary *attribute = @{NSFontAttributeName:self.font};
    
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:self.text attributes:attribute];
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    paragraphStyle.lineSpacing = lineSpacing;
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [self.text length])];
    
    label.attributedText = attributedString;
    
    [label sizeToFit];
    
    self.attributedText = attributedString;
    
    self.numberOfLines = 0;
    
    return label.frame.size.height;
}

/**
 *  设置行间距 根据已有的attributedText
 *
 *  @param lineSpacing 行距
 *  @param width       宽度
 *
 *  @return 高度
 */
- (CGFloat)attributedTextHeightForLineSpacing:(CGFloat)lineSpacing width:(CGFloat)width
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 0)];
    
    label.numberOfLines = 0;
    
    NSMutableAttributedString * attributedString = [self.attributedText mutableCopy];
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    paragraphStyle.lineSpacing = lineSpacing;
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [self.text length])];
    
    label.attributedText = attributedString;
    
    [label sizeToFit];
    
    self.attributedText = attributedString;
    
    self.numberOfLines = 0;
    
    return label.frame.size.height;
}


@end
