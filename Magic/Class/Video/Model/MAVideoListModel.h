//
//  MAVideoListModel.h
//  Magic
//
//  Created by 王 on 16/9/20.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MRBaseModel.h"

@interface MAVideoListModel : MRBaseModel

@property(nonatomic,copy)NSString *datetime;
@property(nonatomic,copy)NSString *duration;
@property(nonatomic,copy)NSString *avatar;
@property(nonatomic,copy)NSString *des;
@property(nonatomic,copy)NSString *fee;
@property(nonatomic,copy)NSString *hit;
@property(nonatomic,copy)NSString *ID;
@property(nonatomic,strong)NSNumber *isbuy;
@property(nonatomic,copy)NSString *recommend;
@property(nonatomic,copy)NSString *thumb;
@property(nonatomic,copy)NSString *tips;
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *uid;
@property(nonatomic,copy)NSString *user_nicename;
@property(nonatomic,copy)NSString *video_url;
@property(nonatomic,copy)NSString *time;
@property (nonatomic, assign) BOOL collected;

@end
