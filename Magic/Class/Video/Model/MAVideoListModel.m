//
//  MAVideoListModel.m
//  Magic
//
//  Created by 王 on 16/9/20.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAVideoListModel.h"

@implementation MAVideoListModel

- (instancetype)initWithJson:(NSDictionary *)json
{
    self = [super init];
    if (self) {
        _duration = [self checkForNull:[json valueForKey:@"duration"]];
        _datetime = [self checkForNull:[json valueForKey:@"datetime"]];
        _isbuy = [self checkForNull:[json valueForKey:@"isbuy"]];
        _ID = [self checkForNull:[json valueForKey:@"id"]];
        _uid = [self checkForNull:[json valueForKey:@"uid"]];
        _recommend = [self checkForNull:[json valueForKey:@"recommend"]];
        _thumb = [self checkForNull:[json valueForKey:@"thumb"]];
        _title = [self checkForNull:[json valueForKey:@"title"]];
        _video_url = [self checkForNull:[json valueForKey:@"video_url"]];
        _fee = [self checkForNull:[json valueForKey:@"fee"]];
        _user_nicename = [self checkForNull:[json valueForKey:@"user_nicename"]];
        _avatar = [self checkForNull:[json valueForKey:@"avatar"]];
        _des = [self checkForNull:[json valueForKey:@"des"]];
        _tips = [self checkForNull:[json valueForKey:@"tips"]];
        _hit = [self checkForNull:[json valueForKey:@"hit"]];
        _time = [self checkForNull:[json valueForKey:@"addtime"]];
    }
    return self;
}

@end
