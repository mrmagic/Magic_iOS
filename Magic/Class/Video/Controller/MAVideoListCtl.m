//
//  MAVideoListCtl.m
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAVideoListCtl.h"
#import "MAVideoListModel.h"
#import "MAVideoCell.h"
#import "MATeachIntroduceCtl.h"
#import "MRSelectCarouselModel.h"
#import "MAVideoHeaderView.h"
#import "MAMeMasterCtl.h"
#import "MATeachPurchasedCtl.h"

#define tableViewHeight kscreenHeight - 64 - 49

@interface MAVideoListCtl ()< UITableViewDelegate, UITableViewDataSource, MAVideoHeaderViewDelegate, MAVideoCellDelegate>

@property (nonatomic, strong) UITableView *recommendedTableView;
@property (nonatomic, strong) MAVideoHeaderView *headerView;

@property (nonatomic, strong) NSMutableArray *recommendedArray;
@property (nonatomic, strong) NSMutableArray *carouselArray;
@property (nonatomic, assign) NSInteger recommendedPage;




@end

@implementation MAVideoListCtl

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.recommendedPage = 0;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    [self setupUI];
    
    [self.recommendedTableView.mj_header beginRefreshing];
}

- (void)setupUI
{
    [self.view addSubview:self.recommendedTableView];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_me_magic"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(leftBarButtonItemClick)];
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
//                                                                                        target:self
//                                                                                        action:@selector(rightBarButtonItemClick)];
}

- (void)reloadate
{
    [self dataSource];
    [self getMyViewMessage];
}

- (void)loadMore
{
    
}

-(void)dataSource
{
    NSDictionary *parameters = @{@"uid":@"1157"};
    NSString *url = [NSURL stringURLWithSever:@"Video.getVideoList" parameters:nil];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 [self.recommendedTableView.mj_header endRefreshing];
                                 if(code == 0)
                                 {
                                     if (self.recommendedPage == 0) {
                                         self.recommendedArray = nil;
                                     }
                                     NSArray *info = [JSON valueForKey:@"info"];
                                     for (NSDictionary *dic in info) {
                                         MAVideoListModel *model = [[MAVideoListModel alloc] initWithJson:dic];
                                         [self.recommendedArray addObject:model];
                                     }
                                     [self.recommendedTableView reloadData];
                                     self.recommendedPage ++;
                                 }
                             } failure:^(NSError *error) {
                                 [self.recommendedTableView.mj_header endRefreshing];
                                 [self showError:MRNetWorkError];

                             }];
}

/**
 *  轮播图数据
 */
- (void)getMyViewMessage
{
    NSString *url = [NSURL stringURLWithSever:@"User.GetSlide" parameters:nil];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                     if (code  == 0){
                                         NSArray *info = [JSON valueForKey:@"info"];
                                         if ([info isKindOfClass:[NSArray class]]) {
                                             if (self.carouselArray.count > 0) {
                                                 self.carouselArray = nil;
                                             }
                                             for (NSDictionary *dic in info) {
                                                 MRSelectCarouselModel *model = [[MRSelectCarouselModel alloc] initWithDic:dic];
                                                 [self.carouselArray addObject:model];
                                             }
                                         }
                                         self.recommendedTableView.tableHeaderView = self.headerView;
                                         [self.headerView updateWithCarouselArray:self.carouselArray];
                                         [self.recommendedTableView reloadData];
                                     } 
                             } failure:^(NSError *error) {
                                 //提示一次 不进行第二次提示
                             }];
}


- (void)leftBarButtonItemClick
{
    MAMeMasterCtl *vc = [[MAMeMasterCtl alloc] init];
    [self pushToViewControllerWithController:vc];
}

- (void)rightBarButtonItemClick
{
   
}

/**
 轮播图点击
 */
- (void)MAVideoHeaderViewViewClickWithIndex:(NSInteger)index
{
    MRSelectCarouselModel *model = [self.carouselArray objectAtIndex:index];
    [self pushToViewControllerWithUrl:model.link title:model.name];
}

- (void)MAVideoCellFollowButtonButtonClickWithModel:(MAVideoListModel *)model
                                            success:(void (^)(NSInteger code))success
                                            failure:(void (^)(NSError *error))failure
{
    if ([MRUser sharedClient].ID == 0) {
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:[[MALoginCtl alloc] init]];
        [self presentViewController:nav
                           animated:YES
                         completion:nil];
    } else {
        //忽略鉴权
        NSDictionary *parameters = @{@"cid": model.ID,
                                     @"ctype": @"2",
                                     @"action": [NSNumber numberWithBool:!model.collected],
                                     };
        NSString *url = [NSURL stringURLWithSever:@"Liverecord.GetPlaybackList"
                                       parameters:parameters];
        [[MRHttpClient sharedClient] get:url
                              parameters:nil
                                 success:^(NSDictionary *JSON, NSInteger code) {
                                     success(!model.collected);
                                 } failure:^(NSError *error) {
                                     failure(nil);
                                 }];
    }
}


#pragma mark - UITableViewDelagate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.recommendedArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MAVideoCell *cell = [MAVideoCell cellWithTableView:tableView];
    cell.delegate = self;
    MAVideoListModel *model = [self.recommendedArray objectAtIndex:indexPath.row];
    [cell cellWithModel:model];
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60 + kscreenWidth * 9 / 16;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MATeachIntroduceCtl *vc = [[MATeachIntroduceCtl alloc] init];
    MAVideoListModel *model = [self.recommendedArray objectAtIndex:indexPath.row];
    vc.VideoId = model.ID;
    [self pushToViewControllerWithController:vc];
}


#pragma mark -懒加载

- (MAVideoHeaderView *)headerView
{
    if (!_headerView) {
        _headerView = [[MAVideoHeaderView alloc] init];
        _headerView.frame = CGRectMake(0, 0, kscreenWidth, 175);
        _headerView.delegate = self;
    }
    return _headerView;
}

- (UITableView *)recommendedTableView
{
    if (!_recommendedTableView) {
        _recommendedTableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, kscreenWidth, tableViewHeight) style:UITableViewStylePlain];
        _recommendedTableView.delegate = self;
        _recommendedTableView.dataSource = self;
        _recommendedTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _recommendedTableView.backgroundColor = MRBackGroundCloor;
        [_recommendedTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
        
        
        //下拉刷新
        _recommendedTableView.mj_header = ({
            MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(reloadate)];
            header.lastUpdatedTimeLabel.hidden = YES;
            [header setTitle:@"加载更多精彩内容" forState:MJRefreshStateIdle];
            [header setTitle:@"加载中" forState:MJRefreshStateRefreshing];
            [header setTitle:@"加载更多精彩内容" forState:MJRefreshStateWillRefresh];
            [header setTitle:@"加载更多精彩内容" forState:MJRefreshStatePulling];
            header;
        });
        

//        MMRefreshFooter *footer = [MMRefreshFooter footerWithRefreshingTarget:target refreshingAction:pageAction];
//
//        
//        //上拉加载
//        _recommendedTableView.mj_footer = ({
//            MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
//            footer. = YES;
//
//            [footer setTitle:@"加载更多" forState:MJRefreshStateRefreshing];
//            footer;
//        });
    }
    return _recommendedTableView;
}

- (NSMutableArray *)recommendedArray
{
    if (!_recommendedArray) {
        _recommendedArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _recommendedArray;
}

- (NSMutableArray *)carouselArray
{
    if (!_carouselArray) {
        _carouselArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _carouselArray;
}



@end
