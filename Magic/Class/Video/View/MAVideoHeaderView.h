//
//  MAVideoHeaderView.h
//  Magic
//
//  Created by 王 on 16/9/26.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MAVideoHeaderViewDelegate <NSObject>

@optional

- (void)MAVideoHeaderViewViewClickWithIndex:(NSInteger)index;

@end

@interface MAVideoHeaderView : UIView

@property (nonatomic, assign) id<MAVideoHeaderViewDelegate> delegate;

/**
 更新试图
 
 @param carouselArray 轮播数组
 */
- (void)updateWithCarouselArray:(NSArray *)carouselArray;

@end
