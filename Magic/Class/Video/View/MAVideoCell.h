//
//  MAVideoCell.h
//  Magic
//
//  Created by 王 on 16/9/20.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAVideoListModel.h"

@protocol MAVideoCellDelegate <NSObject>

@optional
- (void)MAVideoCellFollowButtonButtonClickWithModel:(MAVideoListModel *)model
                                            success:(void (^)(NSInteger code))success
                                            failure:(void (^)(NSError *error))failure;


@end


@interface MAVideoCell : UITableViewCell

@property (nonatomic, assign) id<MAVideoCellDelegate> delegate;
+(MAVideoCell *)cellWithTableView:(UITableView *)tableView;


- (void)cellWithModel:(MAVideoListModel *)model;

@end
