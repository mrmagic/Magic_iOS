//
//  MAVideoHeaderView.m
//  Magic
//
//  Created by 王 on 16/9/26.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAVideoHeaderView.h"
#import "MMShowcaseView.h"
#import "MRSelectCarouselModel.h"

@interface MAVideoHeaderView ()<MMShowcaseViewDelegate, MMShowcaseViewDataSource>

@property (nonatomic, strong) MMShowcaseView *showcaseView;
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) NSMutableArray *carouselArray;
@property (nonatomic, assign) BOOL isSetUp;

@end

@implementation MAVideoHeaderView

/**
 更新试图
 
 @param carouselArray 轮播数组
 */
- (void)updateWithCarouselArray:(NSArray *)carouselArray
{
    self.carouselArray = [NSMutableArray arrayWithArray:carouselArray];
    if (!self.isSetUp) {
        [self setupUI];
        self.isSetUp = YES;
    }
    self.pageControl.numberOfPages = self.carouselArray.count;
    self.pageControl.frame = CGRectMake(kscreenWidth - self.carouselArray.count * 25, 0, self.carouselArray.count * 25, 25);
    _titleLabel.frame = CGRectMake(10, 0, kscreenWidth - self.pageControl.frame.size.width, 25);


    [self.showcaseView reloadData];
}

- (void)setupUI
{
    [self addSubview:self.showcaseView];
    [self addSubview:self.backView];
    [self.backView addSubview:self.titleLabel];
    [self.backView addSubview:self.pageControl];
}
#pragma mark - MMEvaluateListRecruitViewDelegate

/**
 轮播图点击
 
 @param showcaseView 试图
 @param index        点击的个数
 */
- (void)MMShowcaseView:(MMShowcaseView *)showcaseView cellDidClickAtIndex:(NSUInteger)index
{
    if ([self.delegate respondsToSelector:@selector(MAVideoHeaderViewViewClickWithIndex:)]) {
        [self.delegate MAVideoHeaderViewViewClickWithIndex:index];
    }
}

- (NSInteger)numberOfViewsInMMShowcaseView:(MMShowcaseView *)showcaseView
{
    return self.carouselArray.count;
}


- (UIView *)MMShowcaseView:(MMShowcaseView *)showcaseView viewAtCellIndex:(NSUInteger)cellIndex index:(NSUInteger)index
{
    UIImageView *view = [[UIImageView alloc] init];
    view.frame = CGRectMake(0, 0, kscreenWidth, 175);
    MRSelectCarouselModel *model = [self.carouselArray objectAtIndex:index];
    [view sd_setImageWithURL:[NSURL URLWithString:model.imageUrl]
            placeholderImage:[UIImage imageNamed:@""]
                   completed:nil];
    return view;
}

- (void)MMShowcaseViewWithIndex:(NSInteger)index
{
    MRSelectCarouselModel *model = [self.carouselArray objectAtIndex:index];
    self.pageControl.currentPage = index;
    self.titleLabel.text = model.name;
}

- (MMShowcaseView *)showcaseView
{
    if (!_showcaseView) {
        _showcaseView = [[MMShowcaseView alloc] initWithFrame:CGRectMake(0, 0, kscreenWidth, 175)];
        _showcaseView.backgroundColor = [UIColor whiteColor];
        _showcaseView.size = CGSizeMake(kscreenWidth, 175);
        _showcaseView.scale = 1;
        _showcaseView.uniformScale = NO;
        _showcaseView.offset = 0;
        _showcaseView.interval = 3;
        _showcaseView.dataSource = self;
        _showcaseView.delegate = self;
    }
    return _showcaseView;
}

- (UIView *)backView
{
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.frame = CGRectMake(0, 150, kscreenWidth, 25);
        _backView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.3];
    }
    return _backView;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
    }
    return _titleLabel;
}

- (UIPageControl *)pageControl
{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] init];
        _pageControl.pageIndicatorTintColor = MRTextColor;
        _pageControl.currentPageIndicatorTintColor = MRMainColor;
    }
    return _pageControl;
}

- (NSMutableArray *)carouselArray
{
    if (!_carouselArray) {
        _carouselArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _carouselArray;
}


@end
