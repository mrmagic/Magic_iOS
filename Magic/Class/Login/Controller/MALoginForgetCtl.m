//
//  忘记密码
//  Magic
//
//  Created by 王 on 16/9/23.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALoginForgetCtl.h"

@interface MALoginForgetCtl ()<UITextFieldDelegate>

@property (nonatomic, strong) UIImageView *magicImageView;
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIView *loginTitleView;
@property (nonatomic, strong) UITextField *phoneNumberTextFiled;
@property (nonatomic, strong) UITextField *validateTextFiled;
@property (nonatomic, strong) UIButton *validateButton;
@property (nonatomic, strong) UITextField *passWordTextFiled;


@property (nonatomic, strong) UIButton *nextButton;


@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) NSInteger time;

@end

@implementation MALoginForgetCtl

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isNeedLogin = NO;

    }
    return self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self keyboardDismiss];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self addNotification];
}

#pragma mark - 构架UI
- (void)setupUI
{
    
    [self.view addSubview:self.magicImageView];
    [self.view addSubview:self.titleLabel];
    
    [self.view addSubview:self.loginTitleView];
    [self.view addSubview:self.phoneNumberTextFiled];
    [self.view addSubview:self.validateTextFiled];
    [self.view addSubview:self.validateButton];
    [self.view addSubview:self.passWordTextFiled];
    [self.view addSubview:self.nextButton];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardDismiss)];
    [self.view addGestureRecognizer:tap];
}

#pragma mark - 通知
- (void)addNotification
{
    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
}

/**
 键盘出现
 */
- (void)keyboardWillShow:(NSNotification *)notification
{
    //获取键盘的高度
    NSDictionary *userInfo = [notification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    int height = keyboardRect.size.height;
    if (height > kscreenHeight - CGRectGetMaxY(self.nextButton.frame) - 10) {
        CGFloat offset = height - (kscreenHeight - CGRectGetMaxY(self.nextButton.frame) - 10);
        [UIView animateWithDuration:0.2 animations:^{
            self.view.frame = CGRectMake(0, -offset, kscreenWidth, kscreenHeight);
        }];
    }
}

/**
 键盘消失
 */
- (void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame = CGRectMake(0, 0, kscreenWidth, kscreenHeight);
    }];
}


/**
 键盘主动消失
 */
- (void)keyboardDismiss
{
    [self.phoneNumberTextFiled resignFirstResponder];
    [self.passWordTextFiled resignFirstResponder];
    [self.validateTextFiled resignFirstResponder];
}

#pragma mark - UITextFiledDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.phoneNumberTextFiled) {
        if(range.length + range.location > textField.text.length){
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 11;//最大字数长度
    } else if (textField == self.validateTextFiled){
        if(range.length + range.location > textField.text.length){
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 6;//最大字数长度
    } else {
        if(range.length + range.location > textField.text.length){
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 12;//最大字数长度
    }
}

- (void)textFiledChange:(UITextField *)textFiled
{
    if (self.phoneNumberTextFiled.text.length == 11) {
        self.validateButton.backgroundColor = MRMainColor;
        self.validateButton.userInteractionEnabled = YES;
        if (self.validateTextFiled.text.length && self.passWordTextFiled.text.length > 0) {
            self.nextButton.backgroundColor = MRMainColor;
            self.nextButton.userInteractionEnabled = YES;
        } else {
            self.nextButton.backgroundColor = MRTextColor;
            self.nextButton.userInteractionEnabled = NO;
        }
    } else {
        self.validateButton.backgroundColor = MRTextColor;
        self.validateButton.userInteractionEnabled = NO;
        self.nextButton.backgroundColor = MRTextColor;
        self.nextButton.userInteractionEnabled = NO;
    }
}

#pragma mark - 网络请求
- (void)getInviteCode
{
    BOOL result =  [NSString isMobile:self.phoneNumberTextFiled.text];
    if (result) {
        NSString *url = [purl stringByAppendingFormat:@"?service=User.getCode&mobile=%@",self.phoneNumberTextFiled.text];
        [[MRHttpClient sharedClient] get:url
                              parameters:nil
                                 success:^(NSDictionary *JSON, NSInteger code) {
                                     if(code == 0)
                                     {
                                         self.validateButton.userInteractionEnabled = NO;
                                         self.time = 30;
                                         self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeUpdate) userInfo:nil repeats:YES];
                                     }else if (code == 2){
                                         NSString *info = [JSON valueForKey:@"msg"];
                                         [self showError:info];
                                     }
                                 } failure:^(NSError *error) {
                                     [self showError:MRNetWorkError];
                                 }];
        
    } else {
        [self showError:@"输入格式错误"];
    }
}

- (void)sendPassWord
{
    NSString *url = [purl stringByAppendingFormat:@"?service=User.userLostPass&user_login=%@&code=%@&pass=%@",self.phoneNumberTextFiled.text,self.validateTextFiled.text,self.passWordTextFiled.text];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 if (code  == 0) {
                                     [self.navigationController popToRootViewControllerAnimated:YES];
                                 } else {
                                     NSString *msg = [JSON valueForKey:@"msg"];
                                     [self showError:msg];
                                 }
                             } failure:^(NSError *error) {
                                 [self showError:MRNetWorkError];
                                 
                             }];
}
#pragma mark - 按钮点击

/**
 获取验证码
 */
- (void)validateButtonClick
{
    [self getInviteCode];
}
#pragma mark - 按钮点击
- (void)loginButtonClick
{
    BOOL result = [NSString isPassword:self.passWordTextFiled.text];
    if (result) {
        [self sendPassWord];
    } else {
        [self showToast:@"密码长度至少6位"];
    }
}


- (void)timeUpdate
{
    NSString *str = [NSString stringWithFormat:@"验证码已发送(%ld)", (long)self.time];
    [self.validateButton setTitle:str forState:UIControlStateNormal];
    self.time--;
    if (self.time < 0) {
        self.validateButton.userInteractionEnabled = YES;
        [self.validateButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [self.timer invalidate];
        self.timer = nil;
    }
}
#pragma mark - 懒加载
- (UIImageView *)magicImageView
{
    if (!_magicImageView) {
        _magicImageView = [[UIImageView alloc] init];
        _magicImageView.frame = CGRectMake((kscreenWidth - 75) / 2, 70, 75, 75);
        _magicImageView.image = [UIImage imageNamed:@"icon_login_magic"];
    }
    return _magicImageView;
    
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:20];
        _titleLabel.textColor = MRTextColor;
        _titleLabel.text = @"忘记密码";
        _titleLabel.frame = CGRectMake((kscreenWidth - [_titleLabel widthForLabelWithHeight:20]) / 2, CGRectGetMaxY(self.magicImageView.frame), [_titleLabel widthForLabelWithHeight:20], 20);
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}

- (UIView *)loginTitleView
{
    if (!_loginTitleView) {
        _loginTitleView = [[UIView alloc] init];
        _loginTitleView.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + 60, kscreenWidth, 13);
        
        UILabel *label = [[UILabel alloc] init];
        label.text = @"手机号验证";
        label.textColor = MRTextColor;
        label.font = [UIFont systemFontOfSize:13];
        label.frame = CGRectMake((kscreenWidth - [label widthForLabelWithHeight:13]) / 2, 0, [label widthForLabelWithHeight:13], 13);
        
        UIView *left = [[UIView alloc] init];
        left.frame = CGRectMake(CGRectGetMinX(label.frame) - 60, 6, 56, 0.5);
        left.backgroundColor = [UIColor colorWithHex:0xbdb1b2];
        
        
        UIView *right = [[UIView alloc] init];
        right.backgroundColor = [UIColor colorWithHex:0xbdb1b2];
        right.frame = CGRectMake(CGRectGetMaxX(label.frame) + 4, 6, 56, 0.5);
        
        [_loginTitleView addSubview:left];
        [_loginTitleView addSubview:label];
        [_loginTitleView addSubview:right];
        
    }
    return _loginTitleView;
}

- (UITextField *)phoneNumberTextFiled
{
    if (!_phoneNumberTextFiled) {
        _phoneNumberTextFiled = [[UITextField alloc] init];
        _phoneNumberTextFiled.frame = CGRectMake(35, CGRectGetMaxY(self.loginTitleView.frame) + 35, kscreenWidth - 70, 30);
        _phoneNumberTextFiled.borderStyle = UITextBorderStyleRoundedRect;
        _phoneNumberTextFiled.keyboardType = UIKeyboardTypeNumberPad;
        _phoneNumberTextFiled.placeholder = @"手机号";
        _phoneNumberTextFiled.font = [UIFont systemFontOfSize:13];
        _phoneNumberTextFiled.delegate = self;
        
        //添加方法
        [_phoneNumberTextFiled addTarget:self action:@selector(textFiledChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _phoneNumberTextFiled;
}

- (UITextField *)validateTextFiled
{
    if (!_validateTextFiled) {
        _validateTextFiled = [[UITextField alloc] init];
        _validateTextFiled.frame = CGRectMake(35, CGRectGetMaxY(self.phoneNumberTextFiled.frame) + 5, kscreenWidth - 200, 30);
        _validateTextFiled.borderStyle = UITextBorderStyleRoundedRect;
        _validateTextFiled.keyboardType = UIKeyboardTypeNumberPad;
        _validateTextFiled.placeholder = @"验证码";
        _validateTextFiled.font = [UIFont systemFontOfSize:13];
        _validateTextFiled.delegate = self;

        //添加方法
        [_validateTextFiled addTarget:self action:@selector(textFiledChange:) forControlEvents:UIControlEventEditingChanged];
        
    }
    return _validateTextFiled;
}

- (UIButton *)validateButton
{
    if (!_validateButton) {
        _validateButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _validateButton.frame = CGRectMake(CGRectGetMaxX(self.validateTextFiled.frame) + 5, CGRectGetMinY(self.validateTextFiled.frame), 120, 30);
        [_validateButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_validateButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _validateButton.backgroundColor = MRTextColor;
        _validateButton.userInteractionEnabled = NO;
        _validateButton.layer.cornerRadius = 2;
        _validateButton.clipsToBounds = YES;
        _validateButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_validateButton addTarget:self action:@selector(validateButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _validateButton;
}


- (UITextField *)passWordTextFiled
{
    if (!_passWordTextFiled) {
        _passWordTextFiled = [[UITextField alloc] init];
        _passWordTextFiled.frame = CGRectMake(35, CGRectGetMaxY(self.validateTextFiled.frame) + 5, kscreenWidth - 70, 30);
        _passWordTextFiled.borderStyle = UITextBorderStyleRoundedRect;
        _passWordTextFiled.keyboardType = UIKeyboardTypeASCIICapable;
        _passWordTextFiled.placeholder = @"密码";
        _passWordTextFiled.font = [UIFont systemFontOfSize:13];
        _passWordTextFiled.secureTextEntry = YES;
        _passWordTextFiled.delegate = self;
        //添加方法
        [_passWordTextFiled addTarget:self action:@selector(textFiledChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _passWordTextFiled;
}

- (UIButton *)nextButton
{
    if (!_nextButton) {
        _nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _nextButton.frame = CGRectMake(35, CGRectGetMaxY(self.passWordTextFiled.frame) + 17.5, kscreenWidth - 70, 30);
        _nextButton.backgroundColor = MRTextColor;
        _nextButton.userInteractionEnabled = NO;
        _nextButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_nextButton setTitle:@"完成修改" forState:UIControlStateNormal];
        [_nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_nextButton addTarget:self action:@selector(loginButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _nextButton;
}



@end
