//
//  登录控制器
//  Magic
//
//  Created by 王 on 16/9/23.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALoginCtl.h"
#import "MALoginRegisterCtl.h"
#import "MALoginForgetCtl.h"

@interface MALoginCtl ()<UITextFieldDelegate>

@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIImageView *magicImageView;
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIView *loginTitleView;
@property (nonatomic, strong) UITextField *phoneNumberTextFiled;
@property (nonatomic, strong) UITextField *passWordTextFiled;
@property (nonatomic, strong) UIButton *loginButton;
@property (nonatomic, strong) UIButton *registerButton;
@property (nonatomic, strong) UIButton *ForgetButton;



@end

@implementation MALoginCtl

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.view.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self addNotification];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //试图消失 主动把键盘消失
    [self keyboardDismiss];
}

#pragma mark - 构建ui
- (void)setupUI
{
    [self.view addSubview:self.cancelButton];
    
    [self.view addSubview:self.magicImageView];
    [self.view addSubview:self.titleLabel];
    
    [self.view addSubview:self.loginTitleView];
    [self.view addSubview:self.phoneNumberTextFiled];
    [self.view addSubview:self.passWordTextFiled];
    [self.view addSubview:self.loginButton];
    [self.view addSubview:self.registerButton];
    [self.view addSubview:self.ForgetButton];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardDismiss)];
    [self.view addGestureRecognizer:tap];
}


#pragma mark - 通知
- (void)addNotification
{
    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(registerSuccess:)
                                                 name:kRegisterSuccessNotification
                                               object:nil];
    
 
}

/**
 键盘出现
 */
- (void)keyboardWillShow:(NSNotification *)notification
{
    //获取键盘的高度
    NSDictionary *userInfo = [notification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    int height = keyboardRect.size.height;
    if (height > kscreenHeight - CGRectGetMaxY(self.ForgetButton.frame) - 10) {
        CGFloat offset = height - (kscreenHeight - CGRectGetMaxY(self.ForgetButton.frame) - 10);
        [UIView animateWithDuration:0.2 animations:^{
            self.view.frame = CGRectMake(0, -offset, kscreenWidth, kscreenHeight);
        }];
    }
}

/**
 键盘消失
 */
- (void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame = CGRectMake(0, 0, kscreenWidth, kscreenHeight);
    }];
}


- (void)registerSuccess:(NSNotification *)notification
{
    [self loginSuccess];
}
/**
 键盘主动消失
 */
- (void)keyboardDismiss
{
    [self.phoneNumberTextFiled resignFirstResponder];
    [self.passWordTextFiled resignFirstResponder];
}

#pragma mark - UITextFiledDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.phoneNumberTextFiled) {
        if(range.length + range.location > textField.text.length){
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 11;//最大字数长度
    } else {
        if(range.length + range.location > textField.text.length){
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 12;//最大字数长度
    }
}

- (void)textFiledChange:(UITextField *)textFiled
{
    if (self.passWordTextFiled.text.length > 0 && self.phoneNumberTextFiled.text.length > 0) {
        self.loginButton.backgroundColor = MRMainColor;
        self.loginButton.userInteractionEnabled = YES;
    } else {
        self.loginButton.backgroundColor = MRTextColor;
        self.loginButton.userInteractionEnabled = NO;
    }
}


#pragma mark - 点击方法

/**
 关闭控制器
 */
- (void)cancelButtonClick:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)loginSuccess
{
    //登录通知
    [self dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoginStatusChangeNotification object:nil userInfo:nil];
    }];
}


/**
忘记密码
 */
- (void)forgrtButtonClick
{
    MALoginForgetCtl *forgrt = [[MALoginForgetCtl alloc] init];
    [self pushToViewControllerWithController:forgrt];
}


/**
 注册
 */
- (void)registerButtonClick
{
    MALoginRegisterCtl *vc = [[MALoginRegisterCtl alloc] init];
    [self pushToViewControllerWithController:vc];
}


/**
 登录
 */
- (void)loginButtonClick
{
    [self loginwithuserName:self.phoneNumberTextFiled.text password:self.passWordTextFiled.text];
}



-(void)loginwithuserName:(NSString *)userName password:(NSString *)password
{
    BOOL isPhoneNumber = [NSString isMobile:self.phoneNumberTextFiled.text];
    BOOL isPassword = [NSString isPassword:self.passWordTextFiled.text];
    if (isPhoneNumber) {
        if (isPassword) {
            [self login];
        } else {
            [self showError:@"密码格式有误"];
        }
    } else {
        [self showError:@"手机号码格式有误"];
    }
}

- (void)login
{
    NSString *url = [purl stringByAppendingFormat:@"?service=User.userLogin&user_login=%@&&pass=%@",self.phoneNumberTextFiled.text,self.passWordTextFiled.text];
    __weak typeof(self) weakSelf = self;
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 if(code == 0){
                                     NSDictionary *info = [JSON valueForKey:@"info"];
                                     [[MRUser sharedClient] saveUserWithDic:info];
                                     [weakSelf loginSuccess];
                                 }else {
                                     [self showToast:@"登录失败"];
                                 }
                             } failure:^(NSError *error) {
                                 [self showError:MRNetWorkError];
                             }];
}

#pragma mark - 懒加载
- (UIButton *)cancelButton
{
    if (!_cancelButton) {
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancelButton.frame = CGRectMake(0, 14, 50, 50);
        [_cancelButton addTarget:self action:@selector(cancelButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.image = [UIImage imageNamed:@"icon_login_close"];
        imageView.frame = CGRectMake(15, 15, 20, 20);
        [_cancelButton addSubview:imageView];
    }
    return _cancelButton;
}


- (UIImageView *)magicImageView
{
    if (!_magicImageView) {
        _magicImageView = [[UIImageView alloc] init];
        _magicImageView.frame = CGRectMake((kscreenWidth - 75) / 2, 70, 75, 75);
        _magicImageView.image = [UIImage imageNamed:@"icon_login_magic"];
    }
    return _magicImageView;
    
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:20];
        _titleLabel.textColor = MRTextColor;
        _titleLabel.text = @"登录";
        _titleLabel.frame = CGRectMake((kscreenWidth - [_titleLabel widthForLabelWithHeight:20]) / 2, CGRectGetMaxY(self.magicImageView.frame), [_titleLabel widthForLabelWithHeight:20], 20);
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}

- (UIView *)loginTitleView
{
    if (!_loginTitleView) {
        _loginTitleView = [[UIView alloc] init];
        _loginTitleView.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + 60, kscreenWidth, 13);
        
        UILabel *label = [[UILabel alloc] init];
        label.text = @"手机号快速登录";
        label.textColor = MRTextColor;
        label.font = [UIFont systemFontOfSize:13];
        label.frame = CGRectMake((kscreenWidth - [label widthForLabelWithHeight:13]) / 2, 0, [label widthForLabelWithHeight:13], 13);
        
        UIView *left = [[UIView alloc] init];
        left.frame = CGRectMake(CGRectGetMinX(label.frame) - 60, 6, 56, 0.5);
        left.backgroundColor = [UIColor colorWithHex:0xbdb1b2];
        
        
        UIView *right = [[UIView alloc] init];
        right.backgroundColor = [UIColor colorWithHex:0xbdb1b2];
        right.frame = CGRectMake(CGRectGetMaxX(label.frame) + 4, 6, 56, 0.5);
        
        [_loginTitleView addSubview:left];
        [_loginTitleView addSubview:label];
        [_loginTitleView addSubview:right];
        
    }
    return _loginTitleView;
}

- (UITextField *)phoneNumberTextFiled
{
    if (!_phoneNumberTextFiled) {
        _phoneNumberTextFiled = [[UITextField alloc] init];
        _phoneNumberTextFiled.frame = CGRectMake(35, CGRectGetMaxY(self.loginTitleView.frame) + 35, kscreenWidth - 70, 30);
        _phoneNumberTextFiled.borderStyle = UITextBorderStyleRoundedRect;
        _phoneNumberTextFiled.keyboardType = UIKeyboardTypeNumberPad;
        _phoneNumberTextFiled.placeholder = @"手机号";
        _phoneNumberTextFiled.font = [UIFont systemFontOfSize:13];
        _phoneNumberTextFiled.delegate = self;
        //添加方法
        [_phoneNumberTextFiled addTarget:self action:@selector(textFiledChange:) forControlEvents:UIControlEventEditingChanged];
        
        UIImageView *leftView = [[UIImageView alloc] initWithFrame:CGRectMake(5, (40 - 15) / 2, 20, 15)];
        leftView.image = [UIImage imageNamed:@"icon_login_mobile"];
        leftView.contentMode = UIViewContentModeRight; // leftView设置x值无效
        _phoneNumberTextFiled.leftView = leftView;
        _phoneNumberTextFiled.leftViewMode = UITextFieldViewModeAlways;
    }
    return _phoneNumberTextFiled;
}

- (UITextField *)passWordTextFiled
{
    if (!_passWordTextFiled) {
        _passWordTextFiled = [[UITextField alloc] init];
        _passWordTextFiled.frame = CGRectMake(35, CGRectGetMaxY(self.phoneNumberTextFiled.frame) + 5, kscreenWidth - 70, 30);
        _passWordTextFiled.borderStyle = UITextBorderStyleRoundedRect;
        _passWordTextFiled.keyboardType = UIKeyboardTypeASCIICapable;
        _passWordTextFiled.placeholder = @"密码";
        _passWordTextFiled.font = [UIFont systemFontOfSize:13];
        _passWordTextFiled.secureTextEntry = YES;
        _passWordTextFiled.delegate = self;
        //添加方法
        [_passWordTextFiled addTarget:self action:@selector(textFiledChange:) forControlEvents:UIControlEventEditingChanged];
        
        UIImageView *leftView = [[UIImageView alloc] initWithFrame:CGRectMake(5, (40 - 15) / 2, 20, 15)];
        leftView.image = [UIImage imageNamed:@"icon_login_lock"];
        leftView.contentMode = UIViewContentModeRight; // leftView设置x值无效
        _passWordTextFiled.leftView = leftView;
        _passWordTextFiled.leftViewMode = UITextFieldViewModeAlways;
    }
    return _passWordTextFiled;
}

- (UIButton *)loginButton
{
    if (!_loginButton) {
        _loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _loginButton.frame = CGRectMake(35, CGRectGetMaxY(self.passWordTextFiled.frame) + 17.5, kscreenWidth - 70, 30);
        _loginButton.backgroundColor = MRTextColor;
        _loginButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_loginButton setTitle:@"登录" forState:UIControlStateNormal];
        [_loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _loginButton.userInteractionEnabled = NO;

        [_loginButton addTarget:self action:@selector(loginButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _loginButton;
}

- (UIButton *)registerButton
{
    if (!_registerButton) {
        _registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _registerButton.backgroundColor = [UIColor clearColor];
        _registerButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_registerButton setTitle:@"快速注册" forState:UIControlStateNormal];
        [_registerButton setTitleColor:MRMainColor forState:UIControlStateNormal];
        
        UILabel *label = [[UILabel alloc] init];
        label.text = @"快速注册";
        label.font = [UIFont systemFontOfSize:13];
        CGFloat width = [label widthForLabelWithHeight:13];
        
        _registerButton.frame = CGRectMake(35, CGRectGetMaxY(self.loginButton.frame) + 15, width, 13);
        
        [_registerButton addTarget:self action:@selector(registerButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _registerButton;
}

- (UIButton *)ForgetButton
{
    if (!_ForgetButton) {
        _ForgetButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _ForgetButton.backgroundColor = [UIColor clearColor];
        _ForgetButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_ForgetButton setTitle:@"忘记密码" forState:UIControlStateNormal];
        [_ForgetButton setTitleColor:MRMainColor forState:UIControlStateNormal];
        
        UILabel *label = [[UILabel alloc] init];
        label.text = @"忘记密码";
        label.font = [UIFont systemFontOfSize:13];
        CGFloat width = [label widthForLabelWithHeight:13];
        
        _ForgetButton.frame = CGRectMake(kscreenWidth - 35 - width, CGRectGetMaxY(self.loginButton.frame) + 15, width, 13);
        
        [_ForgetButton addTarget:self action:@selector(forgrtButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _ForgetButton;
}





@end
