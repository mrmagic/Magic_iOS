//
//  注册
//  Magic
//
//  Created by 王 on 16/9/23.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALoginRegisterCtl.h"

@interface MALoginRegisterCtl ()<UITextFieldDelegate>

@property (nonatomic, strong) UIImageView *magicImageView;
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIView *loginTitleView;
@property (nonatomic, strong) UITextField *phoneNumberTextFiled;
@property (nonatomic, strong) UITextField *validateTextFiled;
@property (nonatomic, strong) UIButton *validateButton;

@property (nonatomic, strong) UITextField *passWordTextFiled;
@property (nonatomic, strong) UITextField *inviteTextFiled;

@property (nonatomic, strong) UIButton *registerButton;
@property (nonatomic, strong) UIButton *agreementButton;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) NSInteger time;

@end

@implementation MALoginRegisterCtl

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isNeedLogin = NO;
        self.view.backgroundColor = MRBackGroundCloor;
    }
    return self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self keyboardDismiss];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"注册";
    
    [self setupUI];
    [self addNotification];
}

- (void)setupUI
{
    [self.view addSubview:self.magicImageView];
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.loginTitleView];
    [self.view addSubview:self.phoneNumberTextFiled];
    [self.view addSubview:self.validateTextFiled];
    [self.view addSubview:self.validateButton];
    [self.view addSubview:self.passWordTextFiled];
    [self.view addSubview:self.inviteTextFiled];
    [self.view addSubview:self.registerButton];
    [self.view addSubview:self.agreementButton];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardDismiss)];
    [self.view addGestureRecognizer:tap];
}

#pragma mark - 通知
- (void)addNotification
{
    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
}

/**
 键盘出现
 */
- (void)keyboardWillShow:(NSNotification *)notification
{
    //获取键盘的高度
    NSDictionary *userInfo = [notification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    int height = keyboardRect.size.height;
    if (height > kscreenHeight - CGRectGetMaxY(self.registerButton.frame) - 10) {
        CGFloat offset = height - (kscreenHeight - CGRectGetMaxY(self.registerButton.frame) - 10);
        [UIView animateWithDuration:0.2 animations:^{
            self.view.frame = CGRectMake(0, -offset, kscreenWidth, kscreenHeight);
        }];
    }
}

/**
 键盘消失
 */
- (void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame = CGRectMake(0, 0, kscreenWidth, kscreenHeight);
    }];
}
/**
 键盘主动消失
 */
- (void)keyboardDismiss
{
    [self.phoneNumberTextFiled resignFirstResponder];
    [self.validateTextFiled resignFirstResponder];
    [self.inviteTextFiled resignFirstResponder];
}


#pragma mark - 网络请求

/**
 获取验证码
 */
- (void)getInviteCode
{
    BOOL result =  [NSString isMobile:self.phoneNumberTextFiled.text];
    if (result) {
        NSString *url = [purl stringByAppendingFormat:@"?service=User.getCode&mobile=%@",self.phoneNumberTextFiled.text];
        [[MRHttpClient sharedClient] get:url
                              parameters:nil
                                 success:^(NSDictionary *JSON, NSInteger code) {
                                     if(code == 0)
                                     {
                                         self.validateButton.userInteractionEnabled = NO;
                                         self.time = 30;
                                         self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeUpdate) userInfo:nil repeats:YES];
                                     }else if (code == 2){
                                         NSString *info = [JSON valueForKey:@"msg"];
                                         [self showError:info];
                                     }
                                     
                                 } failure:^(NSError *error) {
                                     [self showError:MRNetWorkError];
                                 }];
        
    } else {
        [self showError:@"输入格式错误"];
    }
}

- (void)sendRegister
{
    NSString *url = [purl stringByAppendingFormat:@"?service=User.userReg&user_login=%@&code=%@&pass=%@&invitecode=%@",self.phoneNumberTextFiled.text,self.validateTextFiled.text,self.passWordTextFiled.text,self.inviteTextFiled.text];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 if(code == 0)
                                 {
                                     NSDictionary *info = [JSON valueForKey:@"info"];
                                     [[MRUser sharedClient] saveUserWithDic:info];
                                     [self showSuccess:@"注册成功"];
                                     //注册成功
                                     [[NSNotificationCenter defaultCenter] postNotificationName:kRegisterSuccessNotification object:nil];
                                     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                         [self.navigationController popToRootViewControllerAnimated:YES];
                                     });
                                 } else {
                                     NSString *msg = [JSON valueForKey:@"msg"];
                                     [self showError:msg];
                                 }
                             } failure:^(NSError *error) {
                                 [self showError:MRNetWorkError];
                             }];
    
}

#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.phoneNumberTextFiled) {
        if(range.length + range.location > textField.text.length){
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 11;//最大字数长度
    }else if (textField == self.passWordTextFiled){ //验证码 邀请码 验证
        if(range.length + range.location > textField.text.length){
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 12;//最大字数长度
    }else{ //验证码 邀请码 验证
        if(range.length + range.location > textField.text.length){
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 6;//最大字数长度
    }
}

- (void)textFiledChange:(UITextField *)textFiled
{
    if (self.phoneNumberTextFiled.text.length == 11) {
        self.validateButton.backgroundColor = MRMainColor;
        self.validateButton.userInteractionEnabled = YES;
        if (self.validateTextFiled.text.length > 0 && self.inviteTextFiled.text.length > 0 && self.passWordTextFiled.text.length > 0) {
            self.registerButton.backgroundColor = MRMainColor;
            self.registerButton.userInteractionEnabled = YES;
        } else {
            self.registerButton.backgroundColor = MRTextColor;
            self.registerButton.userInteractionEnabled = NO;
        }
    } else {
        self.validateButton.backgroundColor = MRTextColor;
        self.validateButton.userInteractionEnabled = NO;
        self.registerButton.backgroundColor = MRTextColor;
        self.registerButton.userInteractionEnabled = NO;
    }
}
#pragma mark - 按钮点击
/**
 下一步点击
 */
- (void)registerButtonClick
{
    BOOL result = [NSString isPassword:self.passWordTextFiled.text];
    if (result) {
        [self sendRegister];
    } else {
        [self showToast:@"密码长度至少6位"];
    }
}

- (void)timeUpdate
{
    NSString *str = [NSString stringWithFormat:@"验证码已发送(%ld)", (long)self.time];
    [self.validateButton setTitle:str forState:UIControlStateNormal];
    self.time--;
    if (self.time < 0) {
        self.validateButton.userInteractionEnabled = YES;
        [self.validateButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [self.timer invalidate];
        self.timer = nil;
    }
}

#pragma mark - 懒加载
- (UIImageView *)magicImageView
{
    if (!_magicImageView) {
        _magicImageView = [[UIImageView alloc] init];
        _magicImageView.frame = CGRectMake((kscreenWidth - 75) / 2, 70, 75, 75);
        _magicImageView.image = [UIImage imageNamed:@"icon_login_magic"];
    }
    return _magicImageView;
    
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:20];
        _titleLabel.textColor = MRTextColor;
        _titleLabel.text = @"注册";
        _titleLabel.frame = CGRectMake((kscreenWidth - [_titleLabel widthForLabelWithHeight:20]) / 2, CGRectGetMaxY(self.magicImageView.frame), [_titleLabel widthForLabelWithHeight:20], 20);
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}

- (UIView *)loginTitleView
{
    if (!_loginTitleView) {
        _loginTitleView = [[UIView alloc] init];
        _loginTitleView.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + 60, kscreenWidth, 13);
        
        UILabel *label = [[UILabel alloc] init];
        label.text = @"手机号快速注册";
        label.textColor = MRTextColor;
        label.font = [UIFont systemFontOfSize:13];
        label.frame = CGRectMake((kscreenWidth - [label widthForLabelWithHeight:13]) / 2, 0, [label widthForLabelWithHeight:13], 13);
        
        UIView *left = [[UIView alloc] init];
        left.frame = CGRectMake(CGRectGetMinX(label.frame) - 60, 6, 56, 0.5);
        left.backgroundColor = [UIColor colorWithHex:0xbdb1b2];
        
        
        UIView *right = [[UIView alloc] init];
        right.backgroundColor = [UIColor colorWithHex:0xbdb1b2];
        right.frame = CGRectMake(CGRectGetMaxX(label.frame) + 4, 6, 56, 0.5);
        
        [_loginTitleView addSubview:left];
        [_loginTitleView addSubview:label];
        [_loginTitleView addSubview:right];
        
    }
    return _loginTitleView;
}

- (UITextField *)phoneNumberTextFiled
{
    if (!_phoneNumberTextFiled) {
        _phoneNumberTextFiled = [[UITextField alloc] init];
        _phoneNumberTextFiled.frame = CGRectMake(35, CGRectGetMaxY(self.loginTitleView.frame) + 35, kscreenWidth - 70, 30);
        _phoneNumberTextFiled.borderStyle = UITextBorderStyleRoundedRect;
        _phoneNumberTextFiled.keyboardType = UIKeyboardTypeNumberPad;
        _phoneNumberTextFiled.placeholder = @"手机号";
        _phoneNumberTextFiled.font = [UIFont systemFontOfSize:13];
        _phoneNumberTextFiled.delegate = self;
        
        //添加方法
        [_phoneNumberTextFiled addTarget:self action:@selector(textFiledChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _phoneNumberTextFiled;
}

- (UITextField *)validateTextFiled
{
    if (!_validateTextFiled) {
        _validateTextFiled = [[UITextField alloc] init];
        _validateTextFiled.frame = CGRectMake(35, CGRectGetMaxY(self.phoneNumberTextFiled.frame) + 5, kscreenWidth - 200, 30);
        _validateTextFiled.borderStyle = UITextBorderStyleRoundedRect;
        _validateTextFiled.keyboardType = UIKeyboardTypeNumberPad;
        _validateTextFiled.placeholder = @"验证码";
        _validateTextFiled.font = [UIFont systemFontOfSize:13];
        _validateTextFiled.delegate = self;
        
        //添加方法
        [_validateTextFiled addTarget:self action:@selector(textFiledChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _validateTextFiled;
}

- (UIButton *)validateButton
{
    if (!_validateButton) {
        _validateButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _validateButton.frame = CGRectMake(CGRectGetMaxX(self.validateTextFiled.frame) + 5, CGRectGetMinY(self.validateTextFiled.frame), 120, 30);
        _validateButton.backgroundColor = MRTextColor;
        _validateButton.userInteractionEnabled = NO;
        [_validateButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_validateButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _validateButton.layer.cornerRadius = 2;
        _validateButton.clipsToBounds = YES;
        _validateButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_validateButton addTarget:self action:@selector(getInviteCode) forControlEvents:UIControlEventTouchUpInside];
    }
    return _validateButton;
}

- (UITextField *)passWordTextFiled
{
    if (!_passWordTextFiled) {
        _passWordTextFiled = [[UITextField alloc] init];
        _passWordTextFiled.frame = CGRectMake(35, CGRectGetMaxY(self.validateTextFiled.frame) + 5, kscreenWidth - 70, 30);
        _passWordTextFiled.borderStyle = UITextBorderStyleRoundedRect;
        _passWordTextFiled.keyboardType = UIKeyboardTypeASCIICapable;
        _passWordTextFiled.placeholder = @"密码";
        _passWordTextFiled.font = [UIFont systemFontOfSize:13];
        _passWordTextFiled.secureTextEntry = YES;
        _passWordTextFiled.delegate = self;
        //添加方法
        [_passWordTextFiled addTarget:self action:@selector(textFiledChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _passWordTextFiled;
}


- (UITextField *)inviteTextFiled
{
    if (!_inviteTextFiled) {
        _inviteTextFiled = [[UITextField alloc] init];
        _inviteTextFiled.frame = CGRectMake(35, CGRectGetMaxY(self.passWordTextFiled.frame) + 5, kscreenWidth - 70, 30);
        _inviteTextFiled.borderStyle = UITextBorderStyleRoundedRect;
        _inviteTextFiled.keyboardType = UIKeyboardTypeASCIICapable;
        _inviteTextFiled.placeholder = @"邀请码";
        _inviteTextFiled.font = [UIFont systemFontOfSize:13];
        _inviteTextFiled.delegate = self;
        
        //添加方法
        [_inviteTextFiled addTarget:self action:@selector(textFiledChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _inviteTextFiled;
}

- (UIButton *)registerButton
{
    if (!_registerButton) {
        _registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _registerButton.frame = CGRectMake(35, CGRectGetMaxY(self.inviteTextFiled.frame) + 20, kscreenWidth - 70, 30);
        _registerButton.backgroundColor = MRTextColor;
        _registerButton.userInteractionEnabled = NO;
        _registerButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_registerButton setTitle:@"注册" forState:UIControlStateNormal];
        [_registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_registerButton addTarget:self action:@selector(registerButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _registerButton;
}

- (UIButton *)agreementButton
{
    if (!_agreementButton) {
        _agreementButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _agreementButton.backgroundColor = [UIColor clearColor];
        _agreementButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_agreementButton setTitle:@"已同意本平台协议" forState:UIControlStateNormal];
        [_agreementButton setTitleColor:MRMainColor forState:UIControlStateNormal];
        
        UILabel *label = [[UILabel alloc] init];
        label.text = @"已同意本平台协议";
        label.font = [UIFont systemFontOfSize:13];
        CGFloat width = [label widthForLabelWithHeight:13];
        
        _agreementButton.frame = CGRectMake((kscreenWidth - width) / 2, kscreenHeight - 20 - 15, width, 13);
        
        
        UIView *line = [[UIView alloc] init];
        line.backgroundColor = MRMainColor;
        line.frame = CGRectMake(0, 12.5, width, 0.5);
        [_agreementButton addSubview:line];
        
    }
    return _agreementButton;
}

@end
