//
//  MALoginViewController.m
//  Magic
//
//  Created by 王 on 16/9/26.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALoginMainController.h"

@interface MALoginMainController ()

@property (nonatomic, strong) UIImageView *backView;
@property (nonatomic, strong) UIButton *retuenBackButton;

@end

@implementation MALoginMainController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isNeedLogin = NO;
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.backView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

/**
 优化的push<检测登录>
 
 @param controller 控制器
 */
- (void)pushToViewControllerWithController:(UIViewController *)controller
{
    [self.navigationController pushViewController:controller animated:YES];
    [controller.view addSubview:self.retuenBackButton];
}

#pragma mark - 按钮点击方法
- (void)returnButtonClick:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 懒加载
- (UIImageView *)backView
{
    if (!_backView) {
        _backView = [[UIImageView alloc] init];
        _backView.frame = CGRectMake(0, 0, kscreenWidth, kscreenHeight);
        _backView.clipsToBounds = YES;
        _backView.contentMode = UIViewContentModeTop;
        _backView.image = [UIImage imageNamed:@"image_login_back"];
    }
    return _backView;
}

- (UIButton *)retuenBackButton
{
    if (!_retuenBackButton) {
        _retuenBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _retuenBackButton.frame = CGRectMake(0, 14, 50, 50);
        [_retuenBackButton addTarget:self action:@selector(returnButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.image = [UIImage imageNamed:@"icon_login_magic"];
        imageView.frame = CGRectMake(15, 15, 20, 20);
        [_retuenBackButton addSubview:imageView];
    }
    return _retuenBackButton;
}

@end
