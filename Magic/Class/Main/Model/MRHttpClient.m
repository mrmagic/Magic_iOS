//
//  网络请求
//  iphoneLive
//
//  Created by Ty
//  Copyright © 2016年 cat. All rights reserved.
//

#import "MRHttpClient.h"
#import "AFNetworking.h"

@interface MRHttpClient ()


@end

@implementation MRHttpClient

/**
 *  HTTP 单例
 */
+ (instancetype)sharedClient
{
    static MRHttpClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[MRHttpClient alloc] init];
    });
    return _sharedClient;
}

/**
 *  HTTP GET请求
 *
 *  @param url 接口地址
 *  @param parameters 参数
 *  @param success 成功回调
 *  @param failure 网络异常回调
 */
- (void)get:(NSString *)url
 parameters:(id)parameters
    success:(void (^)(NSDictionary *JSON, NSInteger code))success
    failure:(void (^)(NSError *error))failure
{
    AFHTTPSessionManager *Manager = [[AFHTTPSessionManager alloc] init];
    [Manager GET:url
      parameters:parameters
        progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSInteger ret = [[responseObject valueForKey:@"ret"] integerValue];
            if (ret == 200) {
                NSDictionary *data = [responseObject valueForKey:@"data"];
                NSInteger code = [[data valueForKey:@"code"] integerValue];
                success(data, code);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            failure(error);
        }];
}

/**
 *  HTTP post请求
 *
 *  @param url 接口地址
 *  @param parameters 参数
 *  @param success 成功回调
 *  @param failure 网络异常回调
 */
- (void)post:(NSString *)url
  parameters:(id)parameters
     success:(void (^)(NSDictionary *JSON))success
     failure:(void (^)(NSError *error))failure
{
    AFHTTPSessionManager *Manager = [[AFHTTPSessionManager alloc] init];
    [Manager POST:url
       parameters:parameters
         progress:^(NSProgress * _Nonnull uploadProgress) {
             
         } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             success(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failure(error);
         }];
}


@end
