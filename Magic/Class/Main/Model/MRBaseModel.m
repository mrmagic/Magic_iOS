//
//  MRBaseModel.m
//  iphoneLive
//
//  Created by Ty
//  Copyright © 2016年 cat. All rights reserved.
//

#import "MRBaseModel.h"

@implementation MRBaseModel

/**
 *  初始化方法
 */
- (instancetype)initWithJson:(NSDictionary *)json
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

/**
 *  检查是否是空 保证为空不会出现错误
 */
- (id)checkForNull:(id)value
{
    return (value == [NSNull null]) ? nil : value;
}

/**
 *  检查是否是空 保证为空不会出现错误
 */
+ (id)checkForNull:(id)value
{
    return (value == [NSNull null]) ? nil : value;
}


@end
