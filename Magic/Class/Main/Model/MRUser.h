//
//  MRUser.h
//  iphoneLive
//
//  Created by Ty on 2016/8/26.
//  Copyright © 2016年 cat. All rights reserved.
//

#import "MRBaseModel.h"

@interface MRUser : MRBaseModel

@property (nonatomic, strong) NSString *user_login;    //手机号
@property (nonatomic, strong) NSString *user_pass;     //密码
@property (nonatomic, strong) NSString *ID;            //用户ID
@property (nonatomic, strong) NSString *user_nicename; //用户名
@property (nonatomic, strong) NSString *avatar;        //头像
@property (nonatomic, strong) NSString *birthday;      //生日
@property (nonatomic, strong) NSString *level;         //等级
@property (nonatomic, strong) NSString *coin;          //魔币
@property (nonatomic, strong) NSString *sex;           //性别
@property (nonatomic, strong) NSString *userType;      //用户类型
@property (nonatomic, strong) NSString *signature;     //描述
@property (nonatomic, strong) NSString *city;          //城市

@property (nonatomic, strong) NSString *createTime;    //创建时间
@property (nonatomic, strong) NSString *expiretime;    //过期时间
@property (nonatomic, strong) NSString *ismagic;       //是否魔术师
@property (nonatomic, strong) NSString *token;         //token

@property (nonatomic, assign) BOOL isLogin;            //是否登录


/**
 *  HTTP 单例
 */
+ (instancetype)sharedClient;

/**
 *  存储用户信息
 */
- (void)saveUserWithDic:(NSDictionary *)dic;


/**
 *  修改用户信息
 */
- (void)saveUserWithKey:(NSString *)key values:(NSString *)values;

/**
 *  清除用户信息
 */
- (void)clearUserInforMation;


/**
 验证登录
 */
- (void)verifyLogin;



@end
