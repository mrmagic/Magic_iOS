//
//  网络请求
//  iphoneLive
//
//  Created by Ty
//  Copyright © 2016年 cat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MRHttpClient : NSObject

/**
 *  HTTP 单例
 */
+ (instancetype)sharedClient;

/**
 *  HTTP GET请求
 *
 *  @param url 接口地址
 *  @param parameters 参数
 *  @param success 成功回调
 *  @param failure 网络异常回调
 */
- (void)get:(NSString *)url
 parameters:(id)parameters
    success:(void (^)(NSDictionary *JSON, NSInteger code))success
    failure:(void (^)(NSError *error))failure;

/**
 *  HTTP post请求
 *
 *  @param url 接口地址
 *  @param parameters 参数
 *  @param success 成功回调
 *  @param failure 网络异常回调
 */
- (void)post:(NSString *)url
  parameters:(id)parameters
     success:(void (^)(NSDictionary *JSON))success
     failure:(void (^)(NSError *error))failure;

@end
