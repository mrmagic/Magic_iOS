//
//  MRUser.m
//  iphoneLive
//
//  Created by Ty on 2016/8/26.
//  Copyright © 2016年 cat. All rights reserved.
//

#import "MRUser.h"
#import "MRLocalDataManager.h"

@interface MRUser ()

@end

#define MRUserInformation @"MRUser"

@implementation MRUser

/**
 *  HTTP 单例
 */
+ (instancetype)sharedClient
{
    static MRUser *_user = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _user = [[MRUser alloc] init];
    });
    NSDictionary *JSON = [MRLocalDataManager jsonDictionaryWithFileName:MRUserInformation];
    [_user updatWithJson:JSON];
    return _user;
}

/**
 *  初始化用户信息
 *
 *  @param json 字典
*/
- (void)updatWithJson:(NSDictionary *)json
{
    _ID = [self checkForNull:[json objectForKey:@"id"]];
    _user_nicename = [self checkForNull:[json objectForKey:@"user_nicename"]];
    _user_login =[self checkForNull:[json objectForKey:@"user_login"]];
    _user_pass =[self checkForNull:[json objectForKey:@"user_pass"]];
    _avatar = [self checkForNull:[json objectForKey:@"avatar"]];
    _birthday = [self checkForNull:[json objectForKey:@"birthday"]];
    _level = [self checkForNull:[json objectForKey:@"level"]];
    _coin = [self checkForNull:[json objectForKey:@"coin"]];
    _sex = [self checkForNull:[json objectForKey:@"sex"]];
    _userType = [self checkForNull:[json objectForKey:@"userType"]];
    _signature = [self checkForNull:[json objectForKey:@"signature"]];
    _city = [self checkForNull:[json objectForKey:@"city"]];
    _createTime =[self checkForNull:[json objectForKey:@"creatTime"]];
    _expiretime = [self checkForNull:[json objectForKey:@"expiretime"]];
    _ismagic = [self checkForNull:[json objectForKey:@"ismagic"]];
    _token =[self checkForNull:[json objectForKey:@"token"]];
    if ([_ID integerValue] > 0) {
        _isLogin = YES;
    } else {
        _isLogin = NO;
    }
}

/**
 *  存储用户信息
 *
 */
- (void)saveUserWithDic:(NSDictionary *)dic
{
   [MRLocalDataManager saveJsonDictionary:dic fileName:MRUserInformation];
}

/**
 *  更新用户信息
 */
- (void)saveUserWithKey:(NSString *)key values:(NSString *)values
{
    NSDictionary *JSON = [MRLocalDataManager jsonDictionaryWithFileName:MRUserInformation];
    //确保修改的用户信息是存在的
    NSArray *keyArray = [JSON allKeys];
    for (NSString *newKey in keyArray) {
        if ([newKey isEqualToString:key]) {
            [JSON setValue:values forKey:key];
            [self saveUserWithDic:JSON];
        }
    }
}

/**
 *  清除用户信息
 */
- (void)clearUserInforMation
{
    NSDictionary *dic= [NSDictionary dictionary];
    [self saveUserWithDic:dic];
}

/**
 验证登录
 */
- (void)verifyLogin
{
    if (self.user_login.length > 0) {
        NSString *url = [purl stringByAppendingFormat:@"?service=iftoken&uid=%@&token=%@",self.ID,self.token];
        [[MRHttpClient sharedClient] get:url parameters:nil success:^(NSDictionary *JSON, NSInteger ret) {
            
            
        } failure:^(NSError *error) {
            
        }];
       
    } else {
        //未登录
    }
}

@end
