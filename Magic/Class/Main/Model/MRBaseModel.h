//
//  MRBaseModel.h
//  iphoneLive
//
//  Created by Ty
//  Copyright © 2016年 cat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MRBaseModel : NSObject


/**
 *  初始化方法
 */
- (instancetype)initWithJson:(NSDictionary *)json;

/**
 *  检查是否是空 保证为空不会出现错误
 */
- (id)checkForNull:(id)value;

/**
 *  检查是否是空 保证为空不会出现错误
 */
+ (id)checkForNull:(id)value;


@end
