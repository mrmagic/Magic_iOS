
//
//  自定义试图控制器
//  iphoneLive
//
//  Created
//  Copyright © 2016年 cat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "MRLocalDataManager.h"
#import "UIViewController+MMHUD.h"
#import "UIViewController+MJRefresh.h"
#import "MJRefresh.h"
#import "SBJson.h"

typedef void (^MAViewLoginCallback)(id params);
static MAViewLoginCallback loginCallbackOnLoginStatusChange; // 回调(登录与否都会调用),控制器公用一个回调,防止多次注入


@interface MAViewController : UIViewController

@property (nonatomic, assign) BOOL isNeedLogin;


/**
 优化的push<检测登录>
 
 @param controller 控制器
 */
- (void)pushToViewControllerWithController:(UIViewController *)controller;

/**
 跳转H5

 @param url   链接
 @param title 标题
 */
- (void)pushToViewControllerWithUrl:(NSString *)url title:(NSString *)title;

@end
