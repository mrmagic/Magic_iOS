
//
//  自定义试图控制器
//  iphoneLive
//
//  Created 
//  Copyright © 2016年 cat. All rights reserved.
//

#import "MAViewController.h"
#import "MRWebViewCtl.h"
#import "MALoginCtl.h"

@interface MAViewController ()<UINavigationControllerDelegate>

@property (nonatomic, strong) MAViewLoginCallback loginCallback;
@property (nonatomic, assign) BOOL isLoginNotificationInited;


@end

@implementation MAViewController

/**
 *  内存警告
 */
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil) {
        self.view = nil;
    }
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isNeedLogin = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.view.backgroundColor = MRBackGroundCloor;
    //顶部导航栏颜色
    self.navigationController.navigationBar.tintColor = MRMainColor;
    //开启优化返回
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    if (self.isNeedLogin && [MRUser sharedClient].ID == 0) {
        
    }
}


/**
 跳转H5
 
 @param url   链接
 @param title 标题
 */
- (void)pushToViewControllerWithUrl:(NSString *)url title:(NSString *)title
{
    if (url.length > 0) {
        MRWebViewCtl *web = [[MRWebViewCtl alloc] initWithUrl:url title:title];
        [self pushToViewControllerWithController:web];
    }
}

/**
 *  自定义返回
 */
- (void)returnBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 优化的push<检测登录>
 
 @param controller 控制器
 */
- (void)pushToViewControllerWithController:(UIViewController *)controller
{
    __weak typeof(self) weakSelf = self;
    [self loginWithViewController:controller
                         callback:^(id params)
     {
        controller.hidesBottomBarWhenPushed = YES;
        [weakSelf.navigationController pushViewController:controller animated:YES];
        
        controller.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_nav_back"]
                                                                                           style:UIBarButtonItemStylePlain
                                                                                          target:self
                                                                                          action:@selector(returnBack)];
        
    }];
}

/**
 *  检查是否控制器是否需要登录并回调 (根据isNeedLogin属性判断,没有此出行则不强制登录)
 *
 *  @param viewController
 *  @param callback
 */
- (void)loginWithViewController:(UIViewController *)viewController
                       callback:(MAViewLoginCallback)callback
{
    self.loginCallback = callback;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fireCallbackOnLoginStatusChange) name:kLoginStatusChangeNotification object:nil];
    // 根据isNeedLogin判断是否需要登录,其他则直接回调
    BOOL isNeedLogin = NO;
    
    if ([viewController isKindOfClass:[MAViewController class]]) {
        MAViewController *ctl = (MAViewController *)viewController;
        isNeedLogin = ctl.isNeedLogin;
    }
    
    if (isNeedLogin == YES && [MRUser sharedClient].isLogin == NO) {
        loginCallbackOnLoginStatusChange = callback;
        [self presentLoginController];
    } else {
        [self fireInnerCallback];
    }
    
}


/**
 *  回调
 */
- (void)fireInnerCallback
{
    if (self.loginCallback != nil) {
        self.loginCallback(nil);
        self.loginCallback = nil;
    }
}

/**
 <#Description#>
 */
- (void)presentLoginController
{
    MALoginCtl *login = [[MALoginCtl alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:login];
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)fireCallbackOnLoginStatusChange
{
    if (loginCallbackOnLoginStatusChange != nil) {
        loginCallbackOnLoginStatusChange(nil);
        loginCallbackOnLoginStatusChange = nil;
    }
}


@end
