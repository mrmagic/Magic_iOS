
//
//  MRTabBarViewController
//  iphoneLive
//
//  Created 
//  Copyright © 2016年 cat. All rights reserved.
//

#import "MRTabBarViewController.h"

#import "MASelectListCtl.h"
#import "MAVideoListCtl.h"
#import "MALiveListCtl.h"
#import "MATopicListCtl.h"
#import "MATeachListCtl.h"


#define clickInit 10

@interface MRTabBarViewController ()<UITabBarControllerDelegate>

@property (nonatomic, assign) NSInteger clickNum;

@end

@implementation MRTabBarViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.delegate = self;
        self.clickNum = clickInit;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //初始化控制器
    [self initChilderController];
    
    [self toVerifyLogin];
}

/**
 *  初始化控制器
 */
- (void)initChilderController
{
    /*添加子控制器 */
    /** 精选 */
    UINavigationController *perNav = [self setUpNavigationControllerWithController:[[MASelectListCtl alloc]init]
                                                                          norImage:[UIImage imageNamed:@"icon_bar_home"]
                                                                          selImage:[UIImage imageNamed:@"icon_bar_homeClick"]
                                                                             title:@"首页"];
    
    /** 视频 */
    UINavigationController *movieNav = [self setUpNavigationControllerWithController:[[MAVideoListCtl alloc] init]
                                                                            norImage:[UIImage imageNamed:@"icon_bar_video"]
                                                                            selImage:[UIImage imageNamed:@"icon_bar_videoClick"]
                                                                               title:@"视频"];
    
    /** 直播 */
    UINavigationController *listNav = [self setUpNavigationControllerWithController:[[MALiveListCtl alloc] init]
                                                                           norImage:[UIImage imageNamed:@"icon_bar_live"]
                                                                           selImage:[UIImage imageNamed:@"icon_bar_liveClick"]
                                                                              title:@"直播"];
    
    /** 话题*/
//    MRNavigationController *foundNav = [self setUpNavigationControllerWithController:[[MATopicListCtl alloc] init]
//                                                                            norImage:[UIImage imageNamed:@"icon_bar_chat"]
//                                                                            selImage:[UIImage imageNamed:@"icon_bar_chatClick"]
//                                                                               title:@"话题"];
    
    //教学
    UINavigationController *myNav =[self setUpNavigationControllerWithController:[[MATeachListCtl alloc] init]
                                                                        norImage:[UIImage imageNamed:@"icon_bar_spades"]
                                                                        selImage:[UIImage imageNamed:@"icon_bar_spadesClick"]
                                                                           title:@"教学"];
    
    self.viewControllers = @[perNav, movieNav, listNav,myNav];
    self.tabBar.tintColor = [UIColor whiteColor];
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kscreenWidth, 49)];
    backView.backgroundColor = [UIColor blackColor];
    [self.tabBar insertSubview:backView atIndex:0];
    self.tabBar.opaque = YES;
    [self.tabBar setClipsToBounds:YES];
}

/**
 *  构造控制器
 *
 *  @param controller 控制器
 *  @param norImage   初始图片
 *  @param selImage   选中图片
 *  @param title      标题
 *
 *  @return 导航控制器
 */
- (UINavigationController *)setUpNavigationControllerWithController:(UIViewController *)controller norImage:(UIImage *)norImage selImage:(UIImage *)selImage title:(NSString *)title
{
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:controller];
    controller.title = title;
    controller.tabBarItem.image = norImage;
    controller.tabBarItem.selectedImage = [selImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    return nav;
}

#pragma mark --UITabBarControllerDelegate
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if (self.clickNum == tabBarController.selectedIndex) {
        self.clickNum = clickInit;
        NSString *controller = [self chouseControllerWithSelectedIndex:tabBarController.selectedIndex];
        [[NSNotificationCenter defaultCenter] postNotificationName:kTabBarControllerSelectedIndex object:controller];
    } else{
        self.clickNum = tabBarController.selectedIndex;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.clickNum = clickInit;
    });
}

/**
 *  选择控制器
 *
 *  @param selectedIndex 双击的item
 *
 *  @return 控制器名字
 */
- (NSString *)chouseControllerWithSelectedIndex:(NSInteger)selectedIndex
{
    switch (selectedIndex) {
        case 0:
            return @"MASelectListCtl";
            break;
        case 1:
            return @"MAVideoListCtl";
            break;
        case 2:
            return @"MALiveListCtl";
            break;
        case 3:
            return @"MATopicListCtl";
            break;
        case 4:
            return @"MATopicListCtl";
            break;
            
        default:
            break;
    }
    return nil;
}
/**
 验证登录
 */
- (void)toVerifyLogin
{
    [[MRUser sharedClient] verifyLogin];
}



@end
