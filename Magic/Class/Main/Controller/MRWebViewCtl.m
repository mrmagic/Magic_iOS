//
//  MRWebViewCtl.m
//  iphoneLive
//
//  Created 
//  Copyright © 2016年 cat. All rights reserved.
//

#import "MRWebViewCtl.h"

@interface MRWebViewCtl ()<UIWebViewDelegate>

@property (nonatomic, strong) UIWebView *webview;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *navTitle;

@end



@implementation MRWebViewCtl

- (instancetype)initWithUrl:(NSString *)url title:(NSString *)title
{
    self = [super init];
    if (self) {
        self.url = url;
        self.navTitle = title;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    self.title = self.navTitle;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view addSubview:self.webview];
    
    [self openURL];
}

- (void)openURL
{
    // 上一个请求尚未完成时,停止加载
    if (self.webview.isLoading) {
        [self.webview stopLoading];
    }
    // 开始请求
    [self.webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
}

#pragma mark - webview代理
/**
 *  开始加载
 *
 */
- (void)webViewDidStartLoad:(UIWebView *)webView
{
  
}

/**
 *  加载完成
 *
 */
- (void)webViewDidFinishLoad:(UIWebView *)webView
{

}

/**
 *  加载失败

 */
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

    });
}

/**
 *  是否允许请求
 */
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (UIWebView *)webview
{
    if (!_webview) {
        _webview = [[UIWebView alloc] init];
        _webview.frame = CGRectMake(0, 0, kscreenWidth, kscreenHeight);
        _webview.clipsToBounds = YES;
        _webview.delegate = self;
    }
    return _webview;
}




@end
