//
//  设置
//  Magic
//
//  Created by 王 on 16/9/30.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAMeSetCtl.h"
#import "MAMeCell.h"

@interface MAMeSetCtl ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, copy) NSDictionary *titleDic;


@end

@implementation MAMeSetCtl

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isNeedLogin = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"设定";
    self.titleDic = @{@"0":@{@"0":@{@"账号安全":@"icon_me_male"}},
                      @"1":@{@"0":@{@"清理缓存":@"icon_me_trash"}},
                      @"2":@{@"0":@{@"帮助与反馈":@"icon_me_help"},
                             @"1":@{@"关于魔术吧":@"icon_me_circled"},
                             @"2":@{@"版本号":@"icon_me_attention"}
                             },
                      };
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.cancelButton];
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return 1;
    } else {
        return 3;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MAMeCell *cell = [MAMeCell cellWithTableView:tableView];
    
    NSDictionary *sectionDic = [self.titleDic objectForKey:[NSString stringWithFormat:@"%ld", indexPath.section]];
    NSDictionary *rowDic = [sectionDic objectForKey:[NSString stringWithFormat:@"%ld", indexPath.row]];
    NSString *key = [[rowDic allKeys] firstObject];
    NSString *values = [rowDic objectForKey:key];
    
    [cell cellWithTitle:key image:[UIImage imageNamed:values]];
    
    if (indexPath.row == [sectionDic allKeys].count - 1) {
        cell.lineView.hidden = YES;
    } else {
        cell.lineView.hidden = NO;
    }
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 25;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(0, 0, kscreenWidth, 25);
    view.backgroundColor = MRBackGroundCloor;
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
    } else if (indexPath.section == 1) {
        [[SDImageCache sharedImageCache] clearDisk];
        [self showLoading:nil];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self hideHUD];
        });
    } else {
        switch (indexPath.row) {
            case 0:
            { //直播
                
                
            }
                break;
            case 1:
            { //种植
                
                
            }
                break;
            case 2:
            { //版本号
                
                [self showToast:[NSString stringWithFormat:@"当前版本:%@", MRVersion]];
            }
                break;
                
            default:
                break;
        }
    }
}

- (void)cancelButtonClick
{
    [[MRUser sharedClient] clearUserInforMation];
    [self.navigationController popToRootViewControllerAnimated:YES];
}



#pragma mark - 懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, kscreenWidth, 325) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = MRBackGroundCloor;
        _tableView.scrollEnabled = NO;
    }
    return _tableView;
}
- (UIButton *)cancelButton
{
    if (!_cancelButton) {
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancelButton.frame = CGRectMake(35, kscreenHeight - 30 - 10 - 64, kscreenWidth - 70, 30);
        [_cancelButton setTitle:@"退出登录" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_cancelButton addTarget:self action:@selector(cancelButtonClick) forControlEvents:UIControlEventTouchUpInside];
        _cancelButton.backgroundColor = MRMainColor;
        _cancelButton.titleLabel.font = [UIFont systemFontOfSize:15];
    }
    return _cancelButton;
}



@end
