//
//  客人态
//  Magic
//
//  Created by 王 on 16/9/30.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAMeGuestCtl.h"
#import "MAMeHeaderView.h"
#import "MALiveLivingModel.h"
#import "MALiveCell.h"

@interface MAMeGuestCtl ()<MAMeHeaderViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UIView * navigationView;
@property (nonatomic, strong) MAMeHeaderView *headerView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSourceArray;


@end

@implementation MAMeGuestCtl

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}

- (void)setupUI
{
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.navigationView];
}

- (void)dataSource
{
    
}
#pragma mark - 按钮点击方法;
- (void)leftButtonClick
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MALiveCell *cell = [MALiveCell cellWithTableView:tableView];
    MALiveLivingModel *model = [self.dataSourceArray objectAtIndex:indexPath.row];
    [cell cellWithLiveModel:model];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 215 / 2 + kscreenWidth * 9 / 16 + 20;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    MALiveLivingModel *model = [self.attentionArray objectAtIndex:indexPath.row];
//    MMMeBoughtCtl *vc = [[MMMeBoughtCtl alloc] init];
//    [self pushToViewControllerWithController:vc];
}


#pragma mark - 懒加载
- (UIView *)navigationView
{
    if (!_navigationView) {
        _navigationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kscreenWidth, 64)];
        _navigationView.backgroundColor = [UIColor clearColor];
        
        UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        leftButton.frame = CGRectMake(5, 24, 40, 40);
        [leftButton setImage:[UIImage imageNamed:@"icon_live_purchase"] forState:UIControlStateNormal];
        [leftButton addTarget:self action:@selector(leftButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [_navigationView addSubview:leftButton];
        leftButton.contentMode = UIViewContentModeScaleAspectFill;
        leftButton.tag = 1001;
    }
    return _navigationView;
}
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, kscreenWidth, kscreenHeight - 64) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = MRBackGroundCloor;
        _tableView.tableHeaderView = self.headerView;
    }
    return _tableView;
}

- (MAMeHeaderView *)headerView
{
    if (!_headerView) {
        _headerView = [[MAMeHeaderView alloc] init];
        _headerView.frame = CGRectMake(0, 0, kscreenWidth, 0);
        _headerView.delegate = self;
        [_headerView updateWithModel:nil];
    }
    return _headerView;
}

@end
