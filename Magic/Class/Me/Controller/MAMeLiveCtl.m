//
//  我的直播
//  Magic
//
//  Created by 王 on 16/9/28.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAMeLiveCtl.h"
#import "MALiveCell.h"
#import "MALiveLivingModel.h"
#import "MMMeBoughtCtl.h"

@interface MAMeLiveCtl ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *attentionArray;

@end

@implementation MAMeLiveCtl

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self dataSource];
}

- (void)setupUI
{
    [self.view addSubview:self.tableView];
    self.title = @"直播历史";
}

- (void)dataSource
{

}

#pragma mark - TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.attentionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MALiveCell *cell = [MALiveCell cellWithTableView:tableView];
    MALiveLivingModel *model = [self.attentionArray objectAtIndex:indexPath.row];
    [cell cellWithLiveModel:model];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 215 / 2 + kscreenWidth * 9 / 16 + 20;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MALiveLivingModel *model = [self.attentionArray objectAtIndex:indexPath.row];
    MMMeBoughtCtl *vc = [[MMMeBoughtCtl alloc] init];
//    vc.liveModel = model;
    [self pushToViewControllerWithController:vc];
}


#pragma mark - 懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, kscreenWidth, kscreenHeight - 64) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = MRBackGroundCloor;
    }
    return _tableView;
}

- (NSMutableArray *)attentionArray
{
    if (!_attentionArray) {
        _attentionArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _attentionArray;
}
@end
