//
//  关注列表页
//  Magic
//
//  Created by 王 on 16/9/28.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAViewController.h"

typedef NS_ENUM(NSInteger, MAAttentionType) {
    MAAttentionTypeAttention = 0,     // 关注
    MAAttentionTypeFans,              // 粉丝
};

@interface MAMeAttentionCtl : MAViewController

@property (nonatomic, assign) MAAttentionType attentionType;

@end
