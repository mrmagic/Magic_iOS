//
//  成为魔术师
//  Magic
//
//  Created by 王 on 16/10/3.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAMeMagicCtl.h"


@interface MAMeMagicCtl ()<UITextFieldDelegate>

@property (nonatomic, strong) UIImageView *backImageView;
@property (nonatomic, strong) UIView * navigationView;

@property (nonatomic, strong) UIImageView *titleImageView;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIButton *nextButton;


@end

@implementation MAMeMagicCtl

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupUI];
}

- (void)setupUI
{
    [self.view addSubview:self.backImageView];
    [self.view addSubview:self.navigationView];
    [self.view addSubview:self.titleImageView];
    [self.view addSubview:self.textField];
    [self.view addSubview:self.nextButton];
}

#pragma mark - 按钮点击方法
- (void)leftButtonClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loginButtonClick
{
    NSDictionary *parameters = @{@"invitecode":self.textField.text};
    NSString *url = [NSURL stringURLWithSever:@"Video.setMagic" parameters:parameters];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 if (code == 0) {
                                     [[MRUser sharedClient] saveUserWithKey:@"ismagic" values:@"1"];
                                     [[NSNotificationCenter defaultCenter] postNotificationName:kBecomeMagicSuccessNotification object:nil];
                                     [self showToast:@"成为魔术师"];
                                 }
                             } failure:^(NSError *error) {
                                 [self showError:MRNetWorkError];
                             }];
}

#pragma mark - UITextFiledDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(range.length + range.location > textField.text.length){
        return NO;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 12;//最大字数长度
}

- (void)textFiledChange
{
    if (self.textField.text.length > 0) {
        self.nextButton.backgroundColor = MRMainColor;
        self.nextButton.userInteractionEnabled = YES;
    } else {
        self.nextButton.backgroundColor = MRTextColor;
        self.nextButton.userInteractionEnabled = NO;
    }
}

#pragma mak - 懒加载
- (UIImageView *)backImageView
{
    if (!_backImageView) {
        _backImageView = [[UIImageView alloc] init];
        _backImageView.frame = CGRectMake(0, 0, kscreenWidth, kscreenHeight);
        _backImageView.clipsToBounds = YES;
        _backImageView.contentMode = UIViewContentModeTop;
        _backImageView.image = [UIImage imageNamed:@"image_login_back"];
    }
    return _backImageView;
}

- (UIView *)navigationView
{
    if (!_navigationView) {
        _navigationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kscreenWidth, 64)];
        _navigationView.backgroundColor = [UIColor clearColor];
        
        UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        leftButton.frame = CGRectMake(5, 24, 40, 40);
        [leftButton setImage:[UIImage imageNamed:@"icon_live_purchase"] forState:UIControlStateNormal];
        [leftButton addTarget:self action:@selector(leftButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [_navigationView addSubview:leftButton];
        leftButton.contentMode = UIViewContentModeScaleAspectFill;
        leftButton.tag = 1001;
        
        UILabel *textLabel = [[UILabel alloc] init];
        textLabel.textColor = MRTextColor;
        textLabel.text = @"成为魔术师";
        textLabel.font = [UIFont systemFontOfSize:16];
        CGFloat width = [textLabel widthForLabelWithHeight:16];
        textLabel.frame = CGRectMake((kscreenWidth - width) / 2, 24, width, 40);
        [_navigationView addSubview:textLabel];
    }
    return _navigationView;
}

- (UIImageView *)titleImageView
{
    if (!_titleImageView) {
        _titleImageView = [[UIImageView alloc] init];
        _titleImageView.frame = CGRectMake((kscreenWidth - 50) / 2, 120, 50, 50);
        _titleImageView.clipsToBounds = YES;
        _titleImageView.contentMode = UIViewContentModeTop;
        _titleImageView.image = [UIImage imageNamed:@"icon_me_male"];
        _titleImageView.backgroundColor = [UIColor redColor];
    }
    return _titleImageView;
}

- (UITextField *)textField
{
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.frame = CGRectMake(35, CGRectGetMaxY(self.titleImageView.frame) + 100, kscreenWidth - 70, 30);
        _textField.borderStyle = UITextBorderStyleRoundedRect;
        _textField.keyboardType = UIKeyboardTypeASCIICapable;
        _textField.textAlignment = NSTextAlignmentCenter;
        _textField.placeholder = @"填写邀请码";
        _textField.font = [UIFont systemFontOfSize:13];
        _textField.secureTextEntry = YES;
        _textField.delegate = self;
        //添加方法
        [_textField addTarget:self action:@selector(textFiledChange) forControlEvents:UIControlEventEditingChanged];
    }
    return _textField;
}

- (UIButton *)nextButton
{
    if (!_nextButton) {
        _nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _nextButton.frame = CGRectMake(35, CGRectGetMaxY(self.textField.frame) + 17.5, kscreenWidth - 70, 30);
        _nextButton.backgroundColor = MRTextColor;
        _nextButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_nextButton setTitle:@"确认" forState:UIControlStateNormal];
        [_nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _nextButton.userInteractionEnabled = NO;
        
        [_nextButton addTarget:self action:@selector(loginButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _nextButton;
}




@end
