//
//  MAMeAttentionCtl.m
//  Magic
//
//  Created by 王 on 16/9/28.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAMeAttentionCtl.h"
#import "MAMeAttentionModel.h"
#import "MAMeAttentionCell.h"
#import "MAMeGuestCtl.h"

@interface MAMeAttentionCtl ()<UITableViewDelegate, UITableViewDataSource, MAMeAttentionCellDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *attentionArray;


@end

@implementation MAMeAttentionCtl

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self dataSource];
}

- (void)setupUI
{
    [self.view addSubview:self.tableView];
    if (self.attentionType == MAAttentionTypeAttention) {
        self.title = @"关注";
    } else {
        self.title = @"粉丝";
    }
}

- (void)dataSource
{
    [self showLoading:nil];
    NSDictionary *parameters = [NSDictionary dictionary];
    NSString *url = [NSString string];
    
    if (self.attentionType == MAAttentionTypeFans) {
        parameters = @{@"uid":[MRUser sharedClient].ID,
                       @"ucuid":[MRUser sharedClient].ID,
                       @"token":[MRUser sharedClient].token};
        url = [NSURL stringURLWithSever:@"User.getFans" authentication:NO parameters:parameters];
    } else {
        parameters = @{@"uid":[MRUser sharedClient].ID,
                       @"ucuid":[MRUser sharedClient].ID,
                       @"token":[MRUser sharedClient].token};
        url = [NSURL stringURLWithSever:@"User.getAttention" authentication:NO parameters:parameters];
    }
    
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 [self hideHUD];
                                 
                                 if(code ==0)
                                 {
                                     NSArray *info = [JSON valueForKey:@"info"];
                                     if (self.attentionArray.count > 0) {
                                         self.attentionArray = nil;
                                     }
                                     for (NSDictionary *dic in info) {
                                         MAMeAttentionModel *model = [[MAMeAttentionModel alloc] initWithJson:dic];
                                         [self.attentionArray addObject:model];
                                     }
                                     [self.tableView reloadData];
                                 }
                             } failure:^(NSError *error) {
                                 [self hideHUD];
                                 [self showError:MRNetWorkError];
                             }];
}

#pragma mark - TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.attentionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MAMeAttentionCell *cell = [MAMeAttentionCell cellWithTableView:tableView];
    cell.delegate = self;
    MAMeAttentionModel *model = [self.attentionArray objectAtIndex:indexPath.row];
    [cell cellWithModel:model];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MAMeGuestCtl *vc = [[MAMeGuestCtl alloc] init];
    MAMeAttentionModel *model = [self.attentionArray objectAtIndex:indexPath.row];
    vc.userId = model.userID;
    [self pushToViewControllerWithController:vc];
}

- (void)MAMeAttentionCellAttentionClick:(MAMeAttentionModel *)model
                                success:(void (^)(BOOL result))success
                                failure:(void (^)(NSError *error))failure
{
    NSString *url = [purl stringByAppendingFormat:@"?service=User.setAttention&uid=%@&showid=%@&token=%@",[MRUser sharedClient].ID,model.userID,[MRUser sharedClient].token];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 if(code == 0){
                                     BOOL result = !model.isAttention;
                                     [[NSNotificationCenter defaultCenter] postNotificationName:kAttentionSuccessNotification object:nil];
                                     success(result);
                                 } else {
                                     failure(nil);
                                 }
                             } failure:^(NSError *error) {
                                 failure(nil);
                             }];
}
#pragma mark - 懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, kscreenWidth, kscreenHeight - 64) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
    }
    return _tableView;
}

- (NSMutableArray *)attentionArray
{
    if (!_attentionArray) {
        _attentionArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _attentionArray;
}
@end
