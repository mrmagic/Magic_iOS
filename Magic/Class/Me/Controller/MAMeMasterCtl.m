//
//  主人状
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAMeMasterCtl.h"
#import "MAMeCell.h"
#import "MAMeHeaderView.h"
#import "MALoginCtl.h"
#import "MAMeAttentionCtl.h"
#import "MAMeLiveCtl.h"
#import "MAMeHistoryCtl.h"
#import "MAMeEnshrineCtl.h"
#import "MAMePayCtl.h"
#import "MAMeSetCtl.h"
#import "MAMeMagicCtl.h"

@interface MAMeMasterCtl ()<UITableViewDelegate, UITableViewDataSource, MAMeHeaderViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) MAMeHeaderView *headerView;
@property (nonatomic, copy) NSDictionary *titleDic;
@property (nonatomic, strong) UIView * navigationView;

@end

@implementation MAMeMasterCtl


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isNeedLogin = YES;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self updateDataSource];
    [self setupUI];
    [self datateFollow];
    [self addNotification];
    
}

- (void)setupUI
{
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.navigationView];
}

- (void)updateDataSource
{
    if ([MRUser sharedClient].ismagic) {
        self.titleDic = @{@"0":@{@"直播":@"icon_me_male"},
                          @"1":@{@"历史":@"icon_me_trash"},
                          @"2":@{@"收藏":@"icon_me_help"},
                          };
    } else {
        self.titleDic = @{@"0":@{@"历史":@"icon_me_trash"},
                          @"1":@{@"收藏":@"icon_me_help"},
                          };
    }
}
#pragma mark - 通知
- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(attentionSuccess:)
                                                 name:kAttentionSuccessNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(becomeMagicSucces:)
                                                 name:kBecomeMagicSuccessNotification
                                               object:nil];
}

/**
 关注成功
 */
- (void)attentionSuccess:(NSNotification *)note
{
    [self datateFollow];
}

/**
 成为魔术是成功
 */
- (void)becomeMagicSucces:(NSNotification *)note
{
    [self updateDataSource];
    [self.tableView reloadData];
}
#pragma mark - 数据请求
- (void)datateFollow
{
    NSString *url = [NSURL stringURLWithSever:@"User.getBaseInfo" parameters:nil];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil success:^(NSDictionary *JSON, NSInteger code) {
                              if(code == 0)
                              {
                                  //直播 关注 粉丝
                                  NSArray *info = [JSON valueForKey:@"info"];
                                  MAMeHeaderModel *model = [[MAMeHeaderModel alloc] init];
                                  model.follow = [NSString stringWithFormat:@"%@",[info valueForKey:@"attentionnum"]];
                                  model.fans = [NSString stringWithFormat:@"%@",[info valueForKey:@"fansnum"]];
                                  [self.headerView updateWithModel:model];
                              }else {
                                  [[MRUser sharedClient] clearUserInforMation];
                                  [self.navigationController popViewControllerAnimated:YES];
                                  [self showToast:@"登录过期"];
                              }
                          } failure:^(NSError *error) {
                              [self showError:MRNetWorkError];
                          }];
}

#pragma mark - 点击方法
/**
 返回点击
 */
- (void)leftButtonClick
{
    [self.navigationController popViewControllerAnimated:YES];
}


/**
 设置点击
 */
- (void)rightButtonClick
{
    MAMeSetCtl *vc = [[MAMeSetCtl alloc] init];
    [self pushToViewControllerWithController:vc];
}

/**
 关注点击
 */
- (void)MAMeHeaderViewAttentionClick:(UIButton *)sender
{
    MAMeAttentionCtl* vc = [[MAMeAttentionCtl alloc] init];
    vc.attentionType = MAAttentionTypeAttention;
    [self pushToViewControllerWithController:vc];
}


/**
 粉丝点击
 */
- (void)MAMeHeaderViewFansClick:(UIButton *)sender
{
    MAMeAttentionCtl* vc = [[MAMeAttentionCtl alloc] init];
    vc.attentionType = MAAttentionTypeFans;
    [self pushToViewControllerWithController:vc];
}


/**
 成为魔术师点击
 */
- (void)MAMeHeaderViewMagicClick:(UIButton *)sender
{
    if (![MRUser sharedClient].ismagic) {
        MAMeMagicCtl *vc = [[MAMeMagicCtl alloc] init];
        [self pushToViewControllerWithController:vc];
    }
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MAMeCell *cell = [MAMeCell cellWithTableView:tableView];
    
    NSDictionary *dic = [self.titleDic objectForKey:[NSString stringWithFormat:@"%ld", indexPath.row]];
    NSString *key = [[dic allKeys] firstObject];
    NSString *values = [dic objectForKey:key];
    [cell cellWithTitle:key image:[UIImage imageNamed:values]];
    
    if (indexPath.row == [self.titleDic allKeys].count - 1) {
        cell.lineView.hidden = YES;
    } else {
        cell.lineView.hidden = NO;
    }
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = [self.titleDic objectForKey:[NSString stringWithFormat:@"%ld", indexPath.row]];
    NSString *key = [[dic allKeys] firstObject];
    if ([key isEqualToString:@"直播"]) {
        MAMeLiveCtl *vc = [[MAMeLiveCtl  alloc] init];
        [self pushToViewControllerWithController:vc];
    } else if ([key isEqualToString:@"历史"]) {
        MAMeHistoryCtl *vc = [[MAMeHistoryCtl alloc] init];
        [self pushToViewControllerWithController:vc];
    } else if ([key isEqualToString:@"收藏"]) {
        MAMeEnshrineCtl *vc = [[MAMeEnshrineCtl alloc] init];
        [self pushToViewControllerWithController:vc];
    }
}
#pragma mark - 懒加载
- (UIView *)navigationView
{
    if (!_navigationView) {
        _navigationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kscreenWidth, 64)];
        _navigationView.backgroundColor = [UIColor clearColor];
        
        UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        leftButton.frame = CGRectMake(5, 24, 40, 40);
        [leftButton setImage:[UIImage imageNamed:@"icon_live_purchase"] forState:UIControlStateNormal];
        [leftButton addTarget:self action:@selector(leftButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [_navigationView addSubview:leftButton];
        leftButton.contentMode = UIViewContentModeScaleAspectFill;
        leftButton.tag = 1001;
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        rightButton.frame = CGRectMake(kscreenWidth - 45, 24, 40, 40);
        [rightButton setImage:[UIImage imageNamed:@"icon_live_purchase"] forState:UIControlStateNormal];
        [rightButton addTarget:self action:@selector(rightButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [_navigationView addSubview:rightButton];
        rightButton.contentMode = UIViewContentModeScaleToFill;
        rightButton.tag = 1002;
    }
    return _navigationView;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, kscreenWidth, kscreenHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = MRBackGroundCloor;
        _tableView.scrollEnabled = NO;
        
        _tableView.tableHeaderView = self.headerView;
    }
    return _tableView;
}

- (MAMeHeaderView *)headerView
{
    if (!_headerView) {
        _headerView = [[MAMeHeaderView alloc] init];
        _headerView.frame = CGRectMake(0, 0, kscreenWidth, 270 + 64);
        _headerView.delegate = self;
        [_headerView updateWithModel:nil];
    }
    return _headerView;
}

@end
