//
//  历史
//  Magic
//
//  Created by 王 on 16/9/28.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAMeHistoryCtl.h"

@interface MAMeHistoryCtl ()

@end

@implementation MAMeHistoryCtl

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"观看历史";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
