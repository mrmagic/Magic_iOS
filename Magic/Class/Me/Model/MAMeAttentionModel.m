//
//  MAMeAttentionModel.m
//  Magic
//
//  Created by 王 on 16/9/28.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAMeAttentionModel.h"

@implementation MAMeAttentionModel

- (instancetype)initWithJson:(NSDictionary *)json
{
    self = [super init];
    if (self) {
        _userID = [self checkForNull:[json objectForKey:@"id"]];
        _userIcon = [self checkForNull:[json objectForKey:@"avatar"]];
        _userName = [self checkForNull:[json objectForKey:@"user_nicename"]];
        _isAttention = [[self checkForNull:[json objectForKey:@"isattention"]] boolValue];
    }
    return self;
}

@end
