//
//  MAMeAttentionModel.h
//  Magic
//
//  Created by 王 on 16/9/28.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MRBaseModel.h"

@interface MAMeAttentionModel : MRBaseModel

@property (nonatomic, copy) NSString *userID;
@property (nonatomic, copy) NSString *userIcon;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, assign) BOOL isAttention;


@end
