//
//  关注
//  Magic
//
//  Created by 王 on 16/9/28.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAMeAttentionCell.h"

#define iconWidth 30

@interface MAMeAttentionCell ()

@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *attentionButton;

@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) MAMeAttentionModel *model;

@end

@implementation MAMeAttentionCell

+(MAMeAttentionCell *)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"MAMeAttentionCell";
    [tableView registerClass:[MAMeAttentionCell class] forCellReuseIdentifier:ID];
    MAMeAttentionCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)setupUI
{
    [self addSubview:self.titleLabel];
    [self addSubview:self.iconImageView];
    [self addSubview:self.attentionButton];
    [self addSubview:self.lineView];
}

- (void)attentionClick:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(MAMeAttentionCellAttentionClick:success:failure:)]) {
        [self.delegate MAMeAttentionCellAttentionClick:self.model
                                               success:^(BOOL result) {
                                                   if (!result) {
                                                       self.attentionButton.backgroundColor = [UIColor whiteColor];
                                                       [self.attentionButton setTitleColor:MRMainColor forState:UIControlStateNormal];
                                                       [self.attentionButton setTitle:@"关注" forState:UIControlStateNormal];
                                                   } else {
                                                       self.attentionButton.backgroundColor = MRMainColor;
                                                       [self.attentionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                                                       [self.attentionButton setTitle:@"已关注" forState:UIControlStateNormal];
                                                   }
                                               } failure:^(NSError *error) {
                                                   
                                               }];
    }
}

- (void)cellWithModel:(MAMeAttentionModel *)model
{
    self.model = model;
    [self setupUI];
    self.titleLabel.text = model.userName;
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.userIcon]
                          placeholderImage:[UIImage imageNamed:@""]
                                 completed:nil];
    
    if (model.isAttention) {
        self.attentionButton.backgroundColor = MRMainColor;
        [self.attentionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.attentionButton setTitle:@"已关注" forState:UIControlStateNormal];
    } else {
        self.attentionButton.backgroundColor = [UIColor whiteColor];
        [self.attentionButton setTitleColor:MRMainColor forState:UIControlStateNormal];
        [self.attentionButton setTitle:@"关注" forState:UIControlStateNormal];
    }
    
}

#pragma mark - 懒加载


- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.frame = CGRectMake(10, (50 - iconWidth) / 2, iconWidth, iconWidth);
        _iconImageView.layer.cornerRadius = iconWidth / 2;
        _iconImageView.clipsToBounds = YES;
    }
    return _iconImageView;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:13];
        _titleLabel.textColor = MRTextColor;
        _titleLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImageView.frame) + 10, 0, kscreenWidth - CGRectGetMaxX(self.iconImageView.frame) - 10 - 60 - 15, 50);
    }
    return _titleLabel;
}

- (UIButton *)attentionButton
{
    if (!_attentionButton) {
        _attentionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _attentionButton.frame = CGRectMake(kscreenWidth - 60 - 15, 15, 60, 20);
        _attentionButton.layer.borderColor = MRMainColor.CGColor;
        _attentionButton.layer.borderWidth = 0.5;
        _attentionButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [_attentionButton addTarget:self action:@selector(attentionClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _attentionButton;
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.frame = CGRectMake(50, 49.5, kscreenWidth - 50, 0.5);
        _lineView.backgroundColor = MRLineColor;
    }
    return _lineView;
}



@end
