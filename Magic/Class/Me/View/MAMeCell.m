//
//  MAMeCell.m
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAMeCell.h"

#define iconWidth 20

@interface MAMeCell ()

@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *titleLabel;


@end

@implementation MAMeCell

+(MAMeCell *)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"MAMeCell";
    [tableView registerClass:[MAMeCell class] forCellReuseIdentifier:ID];
    MAMeCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)setupUI
{
    [self addSubview:self.titleLabel];
    [self addSubview:self.iconImageView];
    [self addSubview:self.lineView];
}

- (void)cellWithTitle:(NSString *)title image:(UIImage *)image
{
    [self setupUI];
    self.titleLabel.text = title;
    self.iconImageView.image = image;
    self.titleLabel.frame = CGRectMake(45, 16, [self.titleLabel widthForLabelWithHeight:18], 18);
}

#pragma mark - 懒加载


- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.frame = CGRectMake(15, (50 - iconWidth) / 2, iconWidth, iconWidth);
    }
    return _iconImageView;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:18];
        _titleLabel.textColor = MRTextColor;
    }
    return _titleLabel;
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.frame = CGRectMake(45, 49.5, kscreenWidth, 0.5);
        _lineView.backgroundColor = MRLineColor;
    }
    return _lineView;
}




@end
