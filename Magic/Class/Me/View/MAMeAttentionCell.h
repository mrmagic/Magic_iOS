//
//  MAMeAttentionCell.h
//  Magic
//
//  Created by 王 on 16/9/28.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAMeAttentionModel.h"

@protocol MAMeAttentionCellDelegate <NSObject>

@optional

- (void)MAMeAttentionCellAttentionClick:(MAMeAttentionModel *)model
                                success:(void (^)(BOOL result))success
                                failure:(void (^)(NSError *error))failure;


@end

@interface MAMeAttentionCell : UITableViewCell

@property (nonatomic, assign)id<MAMeAttentionCellDelegate> delegate;

+(MAMeAttentionCell *)cellWithTableView:(UITableView *)tableView;

- (void)cellWithModel:(MAMeAttentionModel *)model;



@end
