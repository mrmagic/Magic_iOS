//
//  MAMeCell.h
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MAMeCell : UITableViewCell
@property (nonatomic, strong) UIView *lineView;


+(MAMeCell *)cellWithTableView:(UITableView *)tableView;

- (void)cellWithTitle:(NSString *)title image:(UIImage *)image;


@end
