//
//  MAMeHeaderView.h
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAMeHeaderModel.h"

@protocol MAMeHeaderViewDelegate <NSObject>

@optional

- (void)MAMeHeaderViewAttentionClick:(UIButton *)sender;

- (void)MAMeHeaderViewFansClick:(UIButton *)sender;

- (void)MAMeHeaderViewMagicClick:(UIButton *)sender;


@end

@interface MAMeHeaderView : UIView

@property (nonatomic, assign)id<MAMeHeaderViewDelegate> delegate;

- (void)updateWithModel:(MAMeHeaderModel *)model;

@end
