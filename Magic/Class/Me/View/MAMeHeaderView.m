//
//  MAMeHeaderView.m
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAMeHeaderView.h"

@interface MAMeHeaderView ()

@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) UIImageView *contentBackView;

@property (nonatomic, strong) UIView *iconBackView;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *nameLable;
@property (nonatomic, strong) UIButton *magicButton;

@property (nonatomic, strong) UILabel *contentLabel;

@property (nonatomic, strong) UIButton *followButton;
@property (nonatomic, strong) UILabel *followLable;
@property (nonatomic, strong) UILabel *followTitleLable;

@property (nonatomic, strong) UIButton *fansButton;
@property (nonatomic, strong) UILabel *fansLable;
@property (nonatomic, strong) UILabel *fansTitleLable;

@property (nonatomic, strong) UIView *lineView;

@end

@implementation MAMeHeaderView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)updateWithModel:(MAMeHeaderModel *)model
{
    [self setupUI];
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:@"asd"]
                          placeholderImage:[UIImage imageNamed:@"image_placeHolder"]
                                 completed:nil];
    
    self.nameLable.text = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:@"%@", [MRUser sharedClient].user_nicename]];
    self.nameLable.frame = CGRectMake((kscreenWidth - [self.nameLable widthForLabelWithHeight:18]) / 2, CGRectGetMaxY(self.iconBackView.frame) + 15, [self.nameLable widthForLabelWithHeight:18], 18);
    
    self.magicButton.frame = CGRectMake(CGRectGetMaxX(self.nameLable.frame) + 5, CGRectGetMaxY(self.iconBackView.frame) + 14, 20, 20);
    if ([MRUser sharedClient].ismagic) {
        self.magicButton.hidden = NO;
    } else {
        self.magicButton.hidden = YES;
    }

    self.contentLabel.text = [NSString stringWithFormat:@"%@", [NSString stringWithFormat:@"%@", [MRUser sharedClient].signature]];
    self.contentLabel.frame = CGRectMake(0, CGRectGetMaxY(self.nameLable.frame) + 7.5, kscreenWidth, 15);
    
    self.followLable.text = model.follow;
    self.fansLable.text = model.fans;
    
    CGFloat followWidth = [self.followLable widthForLabelWithHeight:13] > [self.followTitleLable widthForLabelWithHeight:15] ? [self.followLable widthForLabelWithHeight:13] : [self.followTitleLable widthForLabelWithHeight:15];
    
    CGFloat fansWidth = [self.fansLable widthForLabelWithHeight:13] > [self.fansTitleLable widthForLabelWithHeight:15] ? [self.fansLable widthForLabelWithHeight:13] : [self.fansTitleLable widthForLabelWithHeight:15];
    
    
    self.followLable.frame = CGRectMake(0, 0, followWidth, 13);
    self.followTitleLable.frame = CGRectMake(0, CGRectGetMaxY(self.followLable.frame) + 10, followWidth, 15);
    self.followButton.frame = CGRectMake((kscreenWidth - fansWidth - followWidth - 55) / 2, CGRectGetMaxY(self.contentLabel.frame) + 35, followWidth, 38);
    
    
    self.fansLable.frame = CGRectMake(0, 0, fansWidth, 13);
    self.fansTitleLable.frame = CGRectMake(0, CGRectGetMaxY(self.fansLable.frame) + 10, fansWidth, 15);
    self.fansButton.frame = CGRectMake(CGRectGetMaxX(self.followButton.frame) + 55, CGRectGetMaxY(self.contentLabel.frame) + 35, fansWidth, 38);
}

- (void)setupUI
{
    [self addSubview:self.contentView];
    [self.contentView addSubview:self.contentBackView];
    
    [self.contentView addSubview:self.iconBackView];
    [self.iconBackView addSubview:self.iconImageView];
    
    [self.contentView addSubview:self.nameLable];
    [self.contentView addSubview:self.magicButton];
    
    [self.contentView addSubview:self.contentLabel];
    
    [self.contentView addSubview:self.followButton];
    [self.followButton addSubview:self.followLable];
    [self.followButton addSubview:self.followTitleLable];
    
    [self.contentView addSubview:self.fansButton];
    [self.fansButton addSubview:self.fansLable];
    [self.fansButton addSubview:self.fansTitleLable];

    
    [self addSubview:self.lineView];
}

- (void)followButtonClick:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(MAMeHeaderViewAttentionClick:)]) {
        [self.delegate MAMeHeaderViewAttentionClick:sender];
    }
}

- (void)fansButtonClick:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(MAMeHeaderViewFansClick:)]) {
        [self.delegate MAMeHeaderViewFansClick:sender];
    }
}

- (void)magicButtonClick:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(MAMeHeaderViewMagicClick:)]) {
        [self.delegate MAMeHeaderViewMagicClick:sender];
    }
}

#pragma mark - 懒加载
- (UIView *)contentView
{
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        _contentView.frame = CGRectMake(0, 0, kscreenWidth, 250 + 64);
        _contentView.backgroundColor = [UIColor whiteColor];
    }
    return _contentView;
}

- (UIImageView *)contentBackView
{
    if (!_contentBackView) {
        _contentBackView = [[UIImageView alloc] init];
        _contentBackView.frame = CGRectMake(0, 60 + 64, kscreenWidth, 190);
        _contentBackView.contentMode = UIViewContentModeScaleAspectFill;
        _contentBackView.image = [UIImage imageNamed:@"image_me_backImage"];
    }
    return _contentBackView;
}

- (UIView *)iconBackView
{
    if (!_iconBackView) {
        _iconBackView = [[UIView alloc] init];
        _iconBackView.frame = CGRectMake((kscreenWidth - 80) / 2, 64 + 32.5, 80, 80);
        _iconBackView.backgroundColor = [UIColor whiteColor];
        _iconBackView.clipsToBounds = YES;
        _iconBackView.layer.cornerRadius = 40;
        _iconBackView.layer.borderWidth = 0.5;
        _iconBackView.layer.borderColor = MRTextColor.CGColor;
    }
    return _iconBackView;
}

- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.frame = CGRectMake(5, 5, 70, 70);
        _iconImageView.clipsToBounds = YES;
        _iconImageView.layer.cornerRadius = 35;
        _iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _iconImageView;
}

- (UILabel *)nameLable
{
    if (!_nameLable) {
        _nameLable = [[UILabel alloc] init];
        _nameLable.font = [UIFont systemFontOfSize:18];
        _nameLable.textColor = MRTextColor;
    }
    return _nameLable;
}

- (UIButton *)magicButton
{
    if (!_magicButton) {
        _magicButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_magicButton setImage:[UIImage imageNamed:@"icon_me_magic"] forState:UIControlStateNormal];
        [_magicButton addTarget:self action:@selector(magicButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _magicButton;
}

- (UILabel *)contentLabel
{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.font = [UIFont systemFontOfSize:15];
        _contentLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _contentLabel;
}

- (UIButton *)followButton
{
    if (!_followButton) {
        _followButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_followButton addTarget:self action:@selector(followButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _followButton;
}
- (UILabel *)followLable
{
    if (!_followLable) {
        _followLable = [[UILabel alloc] init];
        _followLable.font = [UIFont systemFontOfSize:13];
        _followLable.textColor = MRMainColor;
        _followLable.textAlignment = NSTextAlignmentCenter;

    }
    return _followLable;
}

- (UILabel *)followTitleLable
{
    if (!_followTitleLable) {
        _followTitleLable = [[UILabel alloc] init];
        _followTitleLable.font = [UIFont systemFontOfSize:15];
        _followTitleLable.textColor = MRTextColor;
        _followTitleLable.text = @"关注";
        _followTitleLable.textAlignment = NSTextAlignmentCenter;
    }
    return _followTitleLable;
}

- (UIButton *)fansButton
{
    if (!_fansButton) {
        _fansButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_fansButton addTarget:self action:@selector(fansButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _fansButton;
}
- (UILabel *)fansLable
{
    if (!_fansLable) {
        _fansLable = [[UILabel alloc] init];
        _fansLable.font = [UIFont systemFontOfSize:13];
        _fansLable.textColor = MRMainColor;
        _fansLable.textAlignment = NSTextAlignmentCenter;

    }
    return _fansLable;
}

- (UILabel *)fansTitleLable
{
    if (!_fansTitleLable) {
        _fansTitleLable = [[UILabel alloc] init];
        _fansTitleLable.font = [UIFont systemFontOfSize:15];
        _fansTitleLable.textColor = MRTextColor;
        _fansTitleLable.text = @"粉丝";
        _fansTitleLable.textAlignment = NSTextAlignmentCenter;

    }
    return _fansTitleLable;
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = MRBackGroundCloor;
        _lineView.frame = CGRectMake(0, 250 + 64, kscreenWidth, 20);
    }
    return _lineView;
}





@end
