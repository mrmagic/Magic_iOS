//
//  MASelectListFotterView.m
//  Magic
//
//  Created by 王 on 16/9/22.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MASelectListFotterView.h"
#import "MASelectFootCell.h"
#import "MASelectClassModel.h"

@interface MASelectListFotterView ()<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, strong) UILabel *classView;
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *flowlayout;
@property (nonatomic, strong) UIView *footView;

@property (nonatomic, copy) NSArray *classArray;

@property (nonatomic, assign) BOOL isSetup;

@end

@implementation MASelectListFotterView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

/**
 更新试图
 
 @param classArray 轮播数组
 */
- (void)updateWithClassArray:(NSArray *)classArray
{
    self.classArray = classArray;
    [self setupUI];
    [self.collectionView reloadData];
}

- (void)setupUI
{
    if (!self.isSetup) {
        [self addSubview:self.classView];
        [self addSubview:self.backView];
        [self.backView addSubview:self.collectionView];
        [self addSubview:self.footView];
    }
    self.isSetup = YES;
}

- (void)moreButtonClick:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(MASelectListFotterViewMoreClick)]) {
        [self.delegate MASelectListFotterViewMoreClick];
    }
}

#pragma mark -
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.classArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MASelectFootCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MASelectFootCell" forIndexPath:indexPath];
    MASelectClassModel *model = [self.classArray objectAtIndex:indexPath.item];
    [cell cellWithModel:model];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(MASelectListFotterViewWithIndex:)]) {
        [self.delegate MASelectListFotterViewWithIndex:indexPath.item];
    }
}
#pragma mark - 懒加载

- (UILabel *)classView
{
    if (!_classView) {
        _classView = [[UILabel alloc] init];
        _classView.frame = CGRectMake(0, 0, kscreenWidth, 57.5);
        _classView.text = @"魔术课堂";
        _classView.textColor = MRMainColor;
        _classView.font = [UIFont systemFontOfSize:15];
        _classView.textAlignment = NSTextAlignmentCenter;
        
    }
    return _classView;
}

- (UIView *)backView
{
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.frame = CGRectMake(0, 57.5, kscreenWidth, 200);
        _backView.backgroundColor = MRBackGroundCloor;
    }
    return _backView;
}
- (UICollectionViewFlowLayout *)flowlayout
{
    if (!_flowlayout) {
        _flowlayout = [[UICollectionViewFlowLayout alloc]init];
        _flowlayout.itemSize = CGSizeMake((kscreenWidth - 60) / 3,200);
        _flowlayout.minimumLineSpacing = 20;
        _flowlayout.minimumInteritemSpacing = 0;
        _flowlayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _flowlayout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10);
    }
    return _flowlayout;
}
- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kscreenWidth, 200) collectionViewLayout:self.flowlayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.backgroundColor = MRBackGroundCloor;
        
        [_collectionView registerClass:[MASelectFootCell class] forCellWithReuseIdentifier:@"MASelectFootCell"];
        
        //注册区尾视图
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FoundFooter"];
    }
    return _collectionView;
}

- (UIView *)footView
{
    if (!_footView) {
        _footView = [[UIView alloc] init];
        _footView.backgroundColor = MRBackGroundCloor;
        _footView.frame = CGRectMake(0, 257.5, kscreenWidth, 45);
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(kscreenWidth - 100, 0, 100, 45);
        [button setTitle:@"更多 >" forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:13];
        [button setTitleColor:MRMainColor forState:UIControlStateNormal];
        [button addTarget:self action:@selector(moreButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [_footView addSubview:button];
    }
    return _footView;
}


@end
