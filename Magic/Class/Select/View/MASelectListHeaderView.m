//
//  MASelectListHeaderView.m
//  Magic
//
//  Created by 王 on 16/9/22.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MASelectListHeaderView.h"
#import "MMShowcaseView.h"
#import "MRSelectCarouselModel.h"

@interface MASelectListHeaderView ()<MMShowcaseViewDelegate, MMShowcaseViewDataSource>

@property (nonatomic, strong) MMShowcaseView *showcaseView;
@property (nonatomic, strong) UIImageView *chooseView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, copy)   NSArray *carouselArray;
@property (nonatomic, assign) BOOL isSetup;


@end

@implementation MASelectListHeaderView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}


- (void)setupUI
{
    if (!self.isSetup) {
        [self addSubview:self.showcaseView];
        [self addSubview:self.chooseView];
        [self.chooseView addSubview:self.pageControl];
    }
    self.isSetup = YES;
}


- (void)updateWithCarouselArray:(NSArray *)carouselArray
{
    self.carouselArray = carouselArray;
    [self setupUI];
    [self.showcaseView reloadData];
}

#pragma mark - 构建试图
- (UIView *)viewForChooseWithImage:(NSString *)image name:(NSString *)name
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    imageView.image = [UIImage imageNamed:image];
    [view addSubview:imageView];
    imageView.userInteractionEnabled = YES;
    
    UILabel *label = [[UILabel alloc] init];
    label.text = name;
    label.font = [UIFont systemFontOfSize:13];
    label.textColor = MRTextColor;
    label.frame = CGRectMake(0, 30, [label widthForLabelWithHeight:13], 13);
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    
    imageView.frame = CGRectMake(([label widthForLabelWithHeight:13] - 25) / 2 , 0, 25, 25);
    view.frame = CGRectMake(0, 0, [label widthForLabelWithHeight:13], 45);
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(chooseViewClick:)];
    [view addGestureRecognizer:tap];
    return view;
}

- (void)chooseViewClick:(UITapGestureRecognizer *)tap
{
    UIView *view = [tap view];
    if ([self.delegate respondsToSelector:@selector(MASelectListHeaderViewChooseClickWithIndex:)]) {
        [self.delegate MASelectListHeaderViewChooseClickWithIndex:view.tag - 1000];
    }
    
}
#pragma mark - MMEvaluateListRecruitViewDelegate

/**
 轮播图点击
 
 @param showcaseView 试图
 @param index        点击的个数
 */
- (void)MMShowcaseView:(MMShowcaseView *)showcaseView cellDidClickAtIndex:(NSUInteger)index
{
    if ([self.delegate respondsToSelector:@selector(MASelectListHeaderViewIndex:)]) {
        [self.delegate MASelectListHeaderViewIndex:index];
    }
}

- (NSInteger)numberOfViewsInMMShowcaseView:(MMShowcaseView *)showcaseView
{
    self.pageControl.numberOfPages = self.carouselArray.count;
    return self.carouselArray.count;
}


- (UIView *)MMShowcaseView:(MMShowcaseView *)showcaseView viewAtCellIndex:(NSUInteger)cellIndex index:(NSUInteger)index
{
    UIImageView *view = [[UIImageView alloc] init];
    view.frame = CGRectMake(0, 0, kscreenWidth, 175);
    MRSelectCarouselModel *model = [self.carouselArray objectAtIndex:index];
    [view sd_setImageWithURL:[NSURL URLWithString:model.imageUrl]
            placeholderImage:[UIImage imageNamed:@""]
                   completed:nil];
    return view;
}

- (void)MMShowcaseViewWithIndex:(NSInteger)index
{
    self.pageControl.currentPage = index;
}

- (MMShowcaseView *)showcaseView
{
    if (!_showcaseView) {
        _showcaseView = [[MMShowcaseView alloc] initWithFrame:CGRectMake(0, 0, kscreenWidth, 175)];
        _showcaseView.backgroundColor = [UIColor whiteColor];
        _showcaseView.size = CGSizeMake(kscreenWidth, 175);
        _showcaseView.scale = 1;
        _showcaseView.uniformScale = NO;
        _showcaseView.offset = 0;
        _showcaseView.interval = 3;
        _showcaseView.dataSource = self;
        _showcaseView.delegate = self;
    }
    return _showcaseView;
}

- (UIImageView *)chooseView
{
    if (!_chooseView) {
        _chooseView = [[UIImageView alloc] init];
        _chooseView.userInteractionEnabled = YES;
        _chooseView.frame = CGRectMake(0, 157.5, kscreenWidth, 92.5);
        _chooseView.image = [UIImage imageNamed:@"image_select_back"];
        
        NSArray *imageArray = @[@"icon_select_like",@"icon_select_thumb",@"icon_select_like"];
        NSArray *titleArray = @[@"最新资讯", @"今日最佳", @"我的收藏"];
        
        CGFloat width = 62.5;
        for (int i = 0; i < 3; i ++) {
            UIView *view = [self viewForChooseWithImage:[imageArray objectAtIndex:i] name:[titleArray objectAtIndex:i]];
            view.tag = 1000 + i;
            CGFloat offset = (kscreenWidth - 125 - view.frame.size.width * 3) / 2;
            view.frame = CGRectMake(width + offset * i + view.frame.size.width * i, 35, view.frame.size.width, view.frame.size.height);
            [_chooseView addSubview:view];
        }
    }
    return _chooseView;
}

- (UIPageControl *)pageControl
{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 15, kscreenWidth, 20)];
        _pageControl.currentPage = 0;
        _pageControl.pageIndicatorTintColor = MRTextColor;
        _pageControl.currentPageIndicatorTintColor = MRMainColor;
    }
    return _pageControl;
}


@end
