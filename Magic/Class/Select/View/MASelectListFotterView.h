//
//  MASelectListFotterView.h
//  Magic
//
//  Created by 王 on 16/9/22.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MASelectListFotterViewDelegate <NSObject>

@optional

/**
 点击课堂
 
 @param index 个数
 */
- (void)MASelectListFotterViewWithIndex:(NSInteger)index;


/**
 点击更多
 */
- (void)MASelectListFotterViewMoreClick;

@end

@interface MASelectListFotterView : UIView

@property (nonatomic, assign) id<MASelectListFotterViewDelegate> delegate;

/**
 更新试图
 
 @param classArray 轮播数组
 */
- (void)updateWithClassArray:(NSArray *)classArray;

@end
