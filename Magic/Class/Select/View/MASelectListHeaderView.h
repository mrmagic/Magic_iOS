//
//  MASelectListHeaderView.h
//  Magic
//
//  Created by 王 on 16/9/22.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MASelectListHeaderViewDelegate <NSObject>

@optional


/**
 点击轮播图
 */
- (void)MASelectListHeaderViewIndex:(NSInteger)index;


/**
 点击选择器

 @param Index 个数
 */
- (void)MASelectListHeaderViewChooseClickWithIndex:(NSInteger)Index;

@end

@interface MASelectListHeaderView : UIView

@property (nonatomic, assign) id<MASelectListHeaderViewDelegate> delegate;


/**
 更新试图

 @param carouselArray 轮播数组
 */
- (void)updateWithCarouselArray:(NSArray *)carouselArray;

@end
