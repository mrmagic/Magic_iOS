//
//  MASelectVideoCell.m
//  Magic
//
//  Created by 王 on 16/9/22.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MASelectVideoCell.h"


#define width (kscreenWidth - 30) / 2
#define height width * 9 / 16 + 60
@interface MASelectVideoCell ()

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *nameLable;
@property (nonatomic, strong) UIImageView *remindImageView;
@property (nonatomic, strong) UILabel *remindLabel;

@end

@implementation MASelectVideoCell

- (void)cellWithModel:(MAVideoListModel *)model
{
    [self setupUI];
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.thumb]
                          placeholderImage:[UIImage imageNamed:@"image_placeHolder"]
                                 completed:nil];
    self.nameLable.text = [NSString stringWithFormat:@"%@", model.title];
    self.remindLabel.text = [NSString stringWithFormat:@"%@", model.hit];
    
}

- (void)setupUI
{
    [self addSubview:self.backView];
    [self.backView addSubview:self.iconImageView];
    [self.backView addSubview:self.nameLable];
    
    [self.backView addSubview:self.remindImageView];
    self.remindImageView.image = [UIImage imageNamed:@"icon_live_visibleBlack"];
    [self.backView addSubview:self.remindLabel];
}

#pragma mark -- 懒加载
- (UIView *)backView
{
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.frame = CGRectMake(0, 0, width, height);
        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}

- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.frame = CGRectMake(4, -4, width, width * 9 / 16);
        _iconImageView.clipsToBounds = YES;
        _iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _iconImageView;
}
- (UILabel *)nameLable
{
    if (!_nameLable) {
        _nameLable = [[UILabel alloc] init];
        _nameLable.font = [UIFont systemFontOfSize:15];
        _nameLable.textColor = MRTextColor;
        _nameLable.frame = CGRectMake(5, CGRectGetMaxY(self.iconImageView.frame) + 10, width - 10, 13);
    }
    return _nameLable;
}

- (UIImageView *)remindImageView
{
    if (!_remindImageView) {
        _remindImageView = [[UIImageView alloc] init];
        _remindImageView.frame = CGRectMake(5, CGRectGetMaxY(self.nameLable.frame) + 15, 20, 20);
    }
    return _remindImageView;
}

- (UILabel *)remindLabel
{
    if (!_remindLabel) {
        _remindLabel = [[UILabel alloc] init];
        _remindLabel.font = [UIFont systemFontOfSize:13];
        _remindLabel.textColor = MRMainColor;
        _remindLabel.frame = CGRectMake(CGRectGetMaxX(self.remindImageView.frame) + 5, CGRectGetMaxY(self.nameLable.frame) + 15, width - 35, 20);
    }
    return _remindLabel;
}

@end
