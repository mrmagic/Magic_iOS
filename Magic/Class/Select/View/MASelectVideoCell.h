//
//  MASelectVideoCell.h
//  Magic
//
//  Created by 王 on 16/9/22.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAVideoListModel.h"

@interface MASelectVideoCell : UICollectionViewCell

- (void)cellWithModel:(MAVideoListModel *)model;


@end
