//
//  MASelectFootCell.h
//  Magic
//
//  Created by 王 on 16/9/22.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MASelectClassModel.h"

@interface MASelectFootCell : UICollectionViewCell

- (void)cellWithModel:(MASelectClassModel *)model;


@end
