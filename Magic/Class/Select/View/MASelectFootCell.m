//
//  MASelectFootCell.m
//  Magic
//
//  Created by 王 on 16/9/22.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MASelectFootCell.h"

#define width (kscreenWidth - 60) / 3
#define height 200

@interface MASelectFootCell ()

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIImageView *backImageView;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *nameLable;
@property (nonatomic, strong) UIImageView *remindImageView;
@property (nonatomic, strong) UILabel *remindLabel;

@end

@implementation MASelectFootCell

- (void)cellWithModel:(MASelectClassModel *)model
{
    [self setupUI];
    
    [self.backImageView sd_setImageWithURL:[NSURL URLWithString:model.thumb]
                          placeholderImage:[UIImage imageNamed:@"image_placeHolder"]
                                 completed:nil];
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.thumb]
                          placeholderImage:[UIImage imageNamed:@"image_placeHolder"]
                                 completed:nil];
    
    self.nameLable.text = [NSString stringWithFormat:@"%@", model.videoType];
    self.remindLabel.text = [NSString stringWithFormat:@"%@", model.participants];
    
}

- (void)setupUI
{
    [self addSubview:self.backView];
    [self.backView addSubview:self.backImageView];
    [self.backView addSubview:self.iconImageView];
    [self.backView addSubview:self.nameLable];
    
    [self.backView addSubview:self.remindImageView];
    self.remindImageView.image = [UIImage imageNamed:@"icon_live_visibleBlack"];
    [self.backView addSubview:self.remindLabel];
}


#pragma mark -- 懒加载
- (UIView *)backView
{
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.frame = CGRectMake(0, 0, width, height);
        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}

- (UIImageView *)backImageView
{
    if (!_backImageView) {
        _backImageView = [[UIImageView alloc] init];
        _backImageView.frame = CGRectMake(-4, 4, width + 8, width + 8);
        _backImageView.clipsToBounds = YES;
        _backImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _backImageView;
}

- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.frame = CGRectMake(26, width - 10, 48, 48);
        _iconImageView.clipsToBounds = YES;
        _iconImageView.layer.cornerRadius = 24;
        _iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _iconImageView;
}

- (UILabel *)nameLable
{
    if (!_nameLable) {
        _nameLable = [[UILabel alloc] init];
        _nameLable.font = [UIFont systemFontOfSize:15];
        _nameLable.textColor = MRTextColor;
        _nameLable.frame = CGRectMake(5, CGRectGetMaxY(self.iconImageView.frame) + 10, width - 10, 13);
        _nameLable.textAlignment = NSTextAlignmentCenter;
    }
    return _nameLable;
}

- (UIImageView *)remindImageView
{
    if (!_remindImageView) {
        _remindImageView = [[UIImageView alloc] init];
        _remindImageView.frame = CGRectMake(5, CGRectGetMaxY(self.nameLable.frame) + 15, 20, 20);
    }
    return _remindImageView;
}

- (UILabel *)remindLabel
{
    if (!_remindLabel) {
        _remindLabel = [[UILabel alloc] init];
        _remindLabel.font = [UIFont systemFontOfSize:13];
        _remindLabel.textColor = MRMainColor;
        _remindLabel.frame = CGRectMake(CGRectGetMaxX(self.remindImageView.frame) + 5, CGRectGetMaxY(self.nameLable.frame) + 15, width - 35, 20);
    }
    return _remindLabel;
}

@end
