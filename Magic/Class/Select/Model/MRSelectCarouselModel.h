//
//  MRSelectTopModel.h
//  iphoneLive
//
//  Created by Ty on 2016/8/31.
//  Copyright © 2016年 cat. All rights reserved.
//

#import "MRBaseModel.h"

@interface MRSelectCarouselModel : MRBaseModel

@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, copy) NSString *link;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *des;


- (instancetype)initWithDic:(NSDictionary *)dic;


@end
