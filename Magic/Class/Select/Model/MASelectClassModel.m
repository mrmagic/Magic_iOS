//
//  MASelectClassModel.m
//  Magic
//
//  Created by 王 on 16/9/22.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MASelectClassModel.h"

@implementation MASelectClassModel

- (instancetype)initWithJson:(NSDictionary *)json
{
    self = [super init];
    if (self) {
        _videoId = [self checkForNull:[json objectForKey:@"id"]];
        _thumb = [self checkForNull:[json objectForKey:@"thumb"]];
        _title = [self checkForNull:[json objectForKey:@"title"]];
        _videoUrl = [self checkForNull:[json objectForKey:@"video_url"]];
        _videoType = [self checkForNull:[json objectForKey:@"video_type"]];
        _days = [self checkForNull:[json objectForKey:@"need_days"]];
        _participants = [self checkForNull:[json objectForKey:@"participants"]];

    }
    return self;
}

@end
