//
//  MRSelectTopModel.m
//  iphoneLive
//
//  Created by Ty on 2016/8/31.
//  Copyright © 2016年 cat. All rights reserved.
//

#import "MRSelectCarouselModel.h"

@implementation MRSelectCarouselModel

- (instancetype)initWithDic:(NSDictionary *)dic
{
    self = [super init];
    if (self) {
        _imageUrl = [self checkForNull:[dic objectForKey:@"slide_pic"]];
        _link = [self checkForNull:[dic objectForKey:@"slide_url"]];
        _name = [self checkForNull:[dic objectForKey:@"slide_name"]];
        _content =  [self checkForNull:[dic objectForKey:@"slide_content"]];
        _des = [self checkForNull:[dic objectForKey:@"slide_des"]];
    }
    return self;
}

@end
