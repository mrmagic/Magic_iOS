//
//  MASelectClassModel.h
//  Magic
//
//  Created by 王 on 16/9/22.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MRBaseModel.h"

@interface MASelectClassModel : MRBaseModel

@property (nonatomic, copy) NSString *videoId;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *videoType;
@property (nonatomic, copy) NSString *thumb;
@property (nonatomic, copy) NSString *videoUrl;
@property (nonatomic, copy) NSString *days;
@property (nonatomic, copy) NSString *participants; 

@end
