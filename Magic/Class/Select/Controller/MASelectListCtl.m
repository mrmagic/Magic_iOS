//
//  MASelectListCtl.m
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MASelectListCtl.h"
#import "MASelectView.h"
#import "MAMeMasterCtl.h"
#import "MMShowcaseView.h"
#import "MRSelectCarouselModel.h"
#import "MAVideoListModel.h"
#import "MASelectVideoCell.h"
#import "MASelectClassModel.h"
#import "MASelectListHeaderView.h"
#import "MASelectListFotterView.h"
#import "MATeachIntroduceCtl.h"
#import "MAMeEnshrineCtl.h"
#import <MediaPlayer/MediaPlayer.h>



@interface MASelectListCtl ()<UICollectionViewDelegate, UICollectionViewDataSource, MASelectListHeaderViewDelegate, MASelectListFotterViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *flowlayout;
@property (nonatomic, strong) MASelectListHeaderView *headerView;
@property (nonatomic, strong) MASelectListFotterView *fotterView;


@property (nonatomic, strong) UILabel *videoTitleLabel;
@property (nonatomic, strong) UIView *videoMoreView;

@property (nonatomic, strong) NSMutableArray *carouselArray;
@property (nonatomic, strong) NSMutableArray *videoArray;
@property (nonatomic, strong) NSMutableArray *classArray;

@end

@implementation MASelectListCtl

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self.collectionView.mj_header beginRefreshing];
}

- (void)setupUI
{
    [self.view addSubview:self.collectionView];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_me_magic"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(leftBarButtonItemClick)];
}


#pragma mark - 网络请求
/**
 刷新方法
 */
- (void)dataSource
{
    [self getMyViewMessage];
    [self getDataSource];
    [self getLittleMessage];
}
- (void)loadMore
{
    
}
/**
 *  轮播图数据
 */
- (void)getMyViewMessage
{
    NSString *url = [NSURL stringURLWithSever:@"User.GetSlide" parameters:nil];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 if (code == 0){
                                     NSArray *info = [JSON valueForKey:@"info"];
                                     if ([info isKindOfClass:[NSArray class]]) {
                                         if (self.carouselArray.count > 0) {
                                             self.carouselArray = nil;
                                         }
                                         for (NSDictionary *dic in info) {
                                             MRSelectCarouselModel *model = [[MRSelectCarouselModel alloc] initWithDic:dic];
                                             [self.carouselArray addObject:model];
                                         }
                                     }
                                     [self.collectionView reloadData];
                                 } else {
                                     
                                 }
                             } failure:^(NSError *error) {
                                 //提示一次 不进行第二次提示
                             }];
}

/**
 * 精选数据
 */
-(void)getDataSource
{
    NSString *url = [NSURL stringURLWithSever:@"Video.GetRecommend" parameters:nil];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 [self.collectionView.mj_header endRefreshing];
                                 if(code == 0)
                                 {
                                     NSArray *info = [JSON valueForKey:@"info"];
                                     if (self.videoArray.count > 0) {
                                         self.videoArray = nil;
                                     }
                                     
                                     for (NSDictionary *dic in info) {
                                         MAVideoListModel *model = [[MAVideoListModel alloc] initWithJson:dic];
                                         [self.videoArray addObject:model];
                                     }
                                     [self.collectionView reloadData];
                                 } else {
                                     
                                 }
                             } failure:^(NSError *error) {
                                 [self.collectionView.mj_header endRefreshing];
                             }];
}

/**
 *  小视频获取
 */
-(void)getLittleMessage
{
    NSString *url = [NSURL stringURLWithSever:@"TeachingVideos.indexVideos" parameters:nil];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                     if (code == 0) {
                                         NSDictionary *info = [JSON valueForKey:@"indexVideos"];
                                         if (self.classArray.count > 0) {
                                             self.classArray = nil;
                                         }
                                         for (NSDictionary *dic in info) {
                                             MASelectClassModel *model = [[MASelectClassModel alloc] initWithJson:dic];
                                             [self.classArray addObject:model];
                                         }
                                         [self.collectionView reloadData];
                                     } else{
                                         
                                     }
                             } failure:^(NSError *error) {
                                 //提示一次 不进行第二次提示
                             }];
}

#pragma mark - 按钮点击方法

/**
 头部快速选择按钮
 */
- (void)chooseViewClick:(UITapGestureRecognizer *)sender
{
    
}


/**
 个人中心
 */
- (void)leftBarButtonItemClick
{
    MAMeMasterCtl *vc = [[MAMeMasterCtl alloc] init];
    [self pushToViewControllerWithController:vc];
}

#pragma mark - MASelectListHeaderViewDelagate
/**
 点击轮播图
  */
- (void)MASelectListHeaderViewIndex:(NSInteger)index
{
    MRSelectCarouselModel *model = [self.carouselArray objectAtIndex:index];
    [self pushToViewControllerWithUrl:model.link title:model.name];
}


/**
 点击选择器
 
 @param Index 个数
 */
- (void)MASelectListHeaderViewChooseClickWithIndex:(NSInteger)Index
{
    switch (Index) {
        case 0:
        {
            [self pushToViewControllerWithUrl:@"ihg" title:@"最新资讯"];
        }
            break;
        case 1:
        {
            [self pushToViewControllerWithUrl:@"jhg" title:@"今日最佳"];
        }
            break;
        case 2:
        {
            MAMeEnshrineCtl *vc = [[MAMeEnshrineCtl alloc] init];
            [self pushToViewControllerWithController:vc];
        }
            break;
            
        default:
            break;
    }
}

/**
 点击课堂
 
 @param index 个数
 */
- (void)MASelectListFotterViewWithIndex:(NSInteger)index
{
    MASelectClassModel *model = [self.classArray objectAtIndex:index];
    MPMoviePlayerViewController *player = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:model.videoUrl]];
    [player.moviePlayer prepareToPlay];
    [player.moviePlayer shouldAutoplay];
    player.moviePlayer.scalingMode = MPMovieScalingModeFill;
    player.moviePlayer.shouldAutoplay = YES;
    //设置横屏播放
    CGAffineTransform landscapeTransform = CGAffineTransformMakeRotation(M_PI / 2);
    player.view.transform = landscapeTransform;
    [player.moviePlayer play];
    
    [self presentMoviePlayerViewControllerAnimated:player];
}


/**
 魔术课堂 更多
 */
- (void)MASelectListFotterViewMoreClick
{
    
}


/**
 精彩视频 更多
 */
- (void)moreButtonClick:(UIButton *)sender
{
    
}

#pragma mark - UICollectionViewDelagate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MATeachIntroduceCtl *vc = [[MATeachIntroduceCtl alloc] init];
    MAVideoListModel *model = [self.videoArray objectAtIndex:indexPath.item];
    vc.VideoId = model.ID;
    [self pushToViewControllerWithController:vc];
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.videoArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MASelectVideoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MALiveUserCollectionCell" forIndexPath:indexPath];
    MAVideoListModel *model = [self.videoArray objectAtIndex:indexPath.item];
    [cell cellWithModel:model];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView * header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"FoundHeader"  forIndexPath:indexPath];
        
        for (UIView *view in [header subviews]) {
            [view removeFromSuperview];
        }
        if (self.carouselArray.count > 0) {
            [self.headerView updateWithCarouselArray:self.carouselArray];
            [header addSubview:self.videoTitleLabel];
        }
        if (self.videoArray.count > 0) {
            if (self.carouselArray.count > 0) {
                self.videoTitleLabel.frame = CGRectMake(0, 250, kscreenWidth, 57.5);
            } else {
                self.videoTitleLabel.frame = CGRectMake(0, 0, kscreenWidth, 57.5);
            }
            [header addSubview:self.headerView];
        }
        return header;
    } else{
        UICollectionReusableView * footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FoundFooter"  forIndexPath:indexPath];
        
        for (UIView *view in [footer subviews]) {
            [view removeFromSuperview];
        }
        if (self.videoArray.count > 0) {
            [footer addSubview:self.videoMoreView];
        }
        if (self.classArray.count > 0) {
            self.fotterView.frame = CGRectMake(0, CGRectGetMaxY(self.videoMoreView.frame), kscreenWidth, 307.5);
            [self.fotterView updateWithClassArray:self.classArray];
            [footer addSubview:self.fotterView];
        }
        return footer;
    }
}

-(CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (self.carouselArray.count > 0 || self.videoArray.count > 0) {
        if (self.carouselArray.count > 0 && self.videoArray.count > 0) {
            CGSize size = {kscreenWidth, 250 + 57.5};
            return size;
        } else if (self.carouselArray.count > 0) {
            CGSize size = {kscreenWidth, 250};
            return size;
        } else {
            CGSize size = {kscreenWidth, 57.5};
            return size;
        }
    } else {
        CGSize size = {kscreenWidth, 0};
        return size;
    }
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    if (self.videoArray.count > 0 || self.classArray.count > 0) {
        if (self.videoArray.count > 0 && self.classArray.count > 0) {
            CGSize size = {kscreenWidth, 200 + 45 + 57.5 + 45};
            return size;
        } else if (self.videoArray.count > 0) {
            CGSize size = {kscreenWidth, 45};
            return size;
        } else {
            CGSize size = {kscreenWidth, 200 + 45 + 57.5};
            return size;
        }
    } else {
        CGSize size = {kscreenWidth, 0};
        return size;
    }
}


#pragma mark - 懒加载
- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.frame = CGRectMake(0, 0, kscreenWidth, kscreenHeight);
    }
    return _scrollView;
}

- (UICollectionViewFlowLayout *)flowlayout
{
    if (!_flowlayout) {
        _flowlayout = [[UICollectionViewFlowLayout alloc]init];
        CGFloat width = (kscreenWidth - 30) / 2;
        _flowlayout.itemSize = CGSizeMake(width,width * 9 / 16 + 70);
        _flowlayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _flowlayout.minimumLineSpacing = 15;
        _flowlayout.minimumInteritemSpacing = 0;
        _flowlayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _flowlayout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10);
    }
    return _flowlayout;
}
- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kscreenWidth, kscreenHeight - 64 - 49) collectionViewLayout:self.flowlayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.backgroundColor = MRBackGroundCloor;
        
   
        
        // 上下拉刷新
        [self initMJRefreshWithScrollView:_collectionView target:self refreshingAction:@selector(dataSource) pageAction:@selector(loadMore)];

        [_collectionView registerClass:[MASelectVideoCell class] forCellWithReuseIdentifier:@"MALiveUserCollectionCell"];
        
        //注册区头视图
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"FoundHeader"];

        //注册区尾视图
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FoundFooter"];
    }
    return _collectionView;
}

- (MASelectListHeaderView *)headerView
{
    if (!_headerView) {
        _headerView = [[MASelectListHeaderView alloc] init];
        _headerView.frame = CGRectMake(0, 0, kscreenWidth, 250);
        _headerView.delegate = self;
    }
    return _headerView;
}

- (MASelectListFotterView *)fotterView
{
    if (!_fotterView) {
        _fotterView = [[MASelectListFotterView alloc] init];
        _fotterView.frame = CGRectMake(0, 0, kscreenWidth, 307.5);
        _fotterView.delegate = self;
    }
    return _fotterView;
}

- (UILabel *)videoTitleLabel
{
    if (!_videoTitleLabel) {
        _videoTitleLabel = [[UILabel alloc] init];
        _videoTitleLabel.frame = CGRectMake(0, 250, kscreenWidth, 57.5);
        _videoTitleLabel.text = @"精彩视频";
        _videoTitleLabel.textColor = MRMainColor;
        _videoTitleLabel.font = [UIFont systemFontOfSize:15];
        _videoTitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _videoTitleLabel;
}


- (UIView *)videoMoreView
{
    if (!_videoMoreView) {
        _videoMoreView = [[UIView alloc] init];
        _videoMoreView.backgroundColor = MRBackGroundCloor;
        _videoMoreView.frame = CGRectMake(0, 0, kscreenWidth, 45);
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(kscreenWidth - 100, 0, 100, 45);
        [button setTitle:@"更多 >" forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:13];
        [button setTitleColor:MRMainColor forState:UIControlStateNormal];
        [button addTarget:self action:@selector(moreButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [_videoMoreView addSubview:button];
    }
    return _videoMoreView;
}

- (NSMutableArray *)carouselArray
{
    if (!_carouselArray) {
        _carouselArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _carouselArray;
}

- (NSMutableArray *)videoArray
{
    if (!_videoArray) {
        _videoArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _videoArray;
}

- (NSMutableArray *)classArray
{
    if (!_classArray) {
        _classArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _classArray;
}



@end
