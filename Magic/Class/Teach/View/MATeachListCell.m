//
//  MATeachListCel.m
//  Magic
//
//  Created by 王 on 16/9/27.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MATeachListCell.h"

@interface MATeachListCell ()

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UILabel *titleLabel;


@property (nonatomic, strong) UIImageView *backImageView;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIImageView *remindImageView;
@property (nonatomic, strong) UILabel *remindLabel;

@property (nonatomic, strong) UIImageView *priceImageView;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UIButton *followButton;

@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) MAVideoListModel *model;


@end

@implementation MATeachListCell

+(MATeachListCell *)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"MATeachListCell";
    [tableView registerClass:[MATeachListCell class] forCellReuseIdentifier:ID];
    MATeachListCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = MRBackGroundCloor;
    
    return cell;
}


- (void)updateFollowImage
{
    self.model.collected = !self.model.collected;
    if (self.model.collected) {
        UIImageView *imageView = [self.followButton viewWithTag:1000];
        imageView.image = [UIImage imageNamed:@"icon_live_collectClick"];
    } else {
        UIImageView *imageView = [self.followButton viewWithTag:1000];
        imageView.image = [UIImage imageNamed:@"icon_live_collect"];
    }
}


- (void)followButtonButtonClick:(UIButton *)sender
{
    __weak typeof(self) weakSelf = self;
    if ([self.delegate respondsToSelector:@selector(MATeachListCellFollowButtonButtonClickWithModel:success:failure:)]) {
        [self.delegate MATeachListCellFollowButtonButtonClickWithModel:self.model
                                                               success:^(NSInteger code) {
                                                                   [weakSelf updateFollowImage];
                                                               } failure:^(NSError *error) {
                                                                   
                                                               }];
    }
}

- (void)cellWithModel:(MAVideoListModel *)model
{
    self.model = model;
    [self setupUI];
    self.titleLabel.text = [NSString stringWithFormat:@"%@", model.title];
    
    [self.backImageView sd_setImageWithURL:[NSURL URLWithString:model.thumb]
                          placeholderImage:[UIImage imageNamed:@"image_placeHolder"]
                                 completed:nil];
    
    
    NSInteger now = [model.duration integerValue];
    //避免出现抖动
    if (now >= 0) {
        NSInteger hour = now / 3600;
        NSInteger min = now / 60 - hour * 60;
        NSInteger sec = now - hour * 3600 - min * 60;
        if (hour > 0) {
            self.timeLabel.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", hour, min, sec];
        } else {
            if (min > 0) {
                self.timeLabel.text = [NSString stringWithFormat:@"%02ld:%02ld", min, sec];
            } else {
                self.timeLabel.text = [NSString stringWithFormat:@"%02ld'", sec];
            }
        }
    }
    self.timeLabel.frame = CGRectMake(kscreenWidth - 12 - [self.timeLabel widthForLabelWithHeight:14], 12.5, [self.timeLabel widthForLabelWithHeight:14] + 20, 20);
    
    //观看人数
    self.remindLabel.text = [NSString stringWithFormat:@"%@", model.hit];
    self.remindLabel.frame = CGRectMake(CGRectGetMaxX(self.remindImageView.frame) + 10, CGRectGetMinY(self.remindImageView.frame), [self.remindLabel widthForLabelWithHeight:13], 20);
    
    //价格
    self.priceLabel.text = [NSString stringWithFormat:@"%@", model.fee];
    self.priceLabel.frame = CGRectMake(5, CGRectGetMaxY(self.backImageView.frame), [self.priceLabel widthForLabelWithHeight:15], 50);
    self.priceImageView.frame = CGRectMake(CGRectGetMaxX(self.priceLabel.frame) + 5, CGRectGetMaxY(self.backImageView.frame) + 10, 30, 30);
    
    if (model.collected) {
        UIImageView *imageView = [self.followButton viewWithTag:1000];
        imageView.image = [UIImage imageNamed:@"icon_live_collectClick"];
    } else {
        UIImageView *imageView = [self.followButton viewWithTag:1000];
        imageView.image = [UIImage imageNamed:@"icon_live_collect"];
    }
}

- (void)setupUI
{
    [self addSubview:self.backView];
    [self.backView addSubview:self.titleLabel];
    [self.backView addSubview:self.backImageView];
    
    [self.backImageView addSubview:self.timeLabel];
    [self.backImageView addSubview:self.remindImageView];
    [self.backImageView addSubview:self.remindLabel];
    [self.backView addSubview:self.priceLabel];
    [self.backView addSubview:self.priceImageView];
    [self.backView addSubview:self.followButton];
}

#pragma mark - 按钮点击方法
#pragma mark - 懒加载
- (UIView *)backView
{
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.frame = CGRectMake(15, 12.5, kscreenWidth - 30, kscreenWidth * 9 / 16 + 50 + 42.5);
        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:15];
        _titleLabel.frame = CGRectMake(10, 0, kscreenWidth - 40, 42.5);
    }
    return _titleLabel;
}
- (UIImageView *)backImageView
{
    if (!_backImageView) {
        _backImageView = [[UIImageView alloc] init];
        _backImageView.frame = CGRectMake(-15, 42.5, kscreenWidth, kscreenWidth * 9 / 16);
        _backImageView.contentMode = UIViewContentModeScaleToFill;
        _backImageView.clipsToBounds = YES;
        _backImageView.backgroundColor = MRMainColor;
    }
    return _backImageView;
}

- (UILabel *)timeLabel
{
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.font = [UIFont systemFontOfSize:14];
        _timeLabel.backgroundColor = MRMainColor;
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        _timeLabel.layer.cornerRadius = 3;
        _timeLabel.clipsToBounds = YES;
        _timeLabel.textColor = [UIColor whiteColor];
    }
    return _timeLabel;
}

- (UIImageView *)remindImageView
{
    if (!_remindImageView) {
        _remindImageView = [[UIImageView alloc] init];
        _remindImageView.frame = CGRectMake(15, kscreenWidth * 9 / 16 - 25, 20, 20);
        _remindImageView.image = [UIImage imageNamed:@"icon_live_visibleWhite"];
    }
    return _remindImageView;
}

- (UILabel *)remindLabel
{
    if (!_remindLabel) {
        _remindLabel = [[UILabel alloc] init];
        _remindLabel.font = [UIFont systemFontOfSize:13];
        _remindLabel.textColor = [UIColor whiteColor];
    }
    return _remindLabel;
}


- (UILabel *)priceLabel
{
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.font = [UIFont systemFontOfSize:15];
        _priceLabel.textColor = MRMainColor;
    }
    return _priceLabel;
}


- (UIImageView *)priceImageView
{
    if (!_priceImageView) {
        _priceImageView = [[UIImageView alloc] init];
        _priceImageView.image = [UIImage imageNamed:@"icon_main_coinRed"];
        _priceLabel.contentMode = UIViewContentModeScaleAspectFill;
        _priceLabel.clipsToBounds = YES;
    }
    return _priceImageView;
}

- (UIButton *)followButton
{
    if (!_followButton) {
        _followButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _followButton.frame = CGRectMake(kscreenWidth - 30 - 50, CGRectGetMaxY(self.backImageView.frame), 50, 50);
        [_followButton addTarget:self action:@selector(followButtonButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *imageview = [[UIImageView alloc] init];
        imageview.tag = 1000;
        imageview.frame = CGRectMake(12.5, 12.5, 25, 25);
        [_followButton addSubview:imageview];
    }
    return _followButton;
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.frame = CGRectMake(0, CGRectGetMaxY(self.remindLabel.frame), kscreenWidth - 30, 7.5);
        _lineView.backgroundColor = MRBackGroundCloor;
    }
    return _lineView;
}


@end
