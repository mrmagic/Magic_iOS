//
//  MATeachListCel.h
//  Magic
//
//  Created by 王 on 16/9/27.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAVideoListModel.h"

@protocol MATeachListCellDelegate <NSObject>

@optional
- (void)MATeachListCellFollowButtonButtonClickWithModel:(MAVideoListModel *)model
                                                success:(void (^)(NSInteger code))success
                                                failure:(void (^)(NSError *error))failure;


@end

@interface MATeachListCell : UITableViewCell

@property (nonatomic, assign) id<MATeachListCellDelegate> delegate;


+(MATeachListCell *)cellWithTableView:(UITableView *)tableView;


- (void)cellWithModel:(MAVideoListModel *)model;

@end
