//
//  MATeachIntroduceModel.h
//  Magic
//
//  Created by 王 on 16/9/28.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MRBaseModel.h"

@interface MATeachIntroduceModel : MRBaseModel

@property (nonatomic, copy) NSString *ID;           //视频ID
@property (nonatomic, copy) NSString *duration;     //视频时长
@property (nonatomic, copy) NSString *title;        //视频标题
@property (nonatomic, copy) NSString *viewNums;     //观看人数
@property (nonatomic, copy) NSString *price;        //价格
@property (nonatomic, copy) NSString *isbuy;        //是否购买
@property (nonatomic, copy) NSString *videoTestUrl; //视频试看地址
@property (nonatomic, copy) NSString *videoUrl;     //视频播放地址
@property (nonatomic, copy) NSString *thumb;        //封面图
@property (nonatomic, copy) NSString *performer;    //表演者
@property (nonatomic, copy) NSString *producers;    //出品方
@property (nonatomic, copy) NSString* time;         //表演时间
@property (nonatomic, copy) NSString *content;      //介绍

@end
