//
//  教学
//  Magic
//
//  Created by 王 on 16/9/22.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MATeachListCtl.h"
#import "MATeachListCell.h"
#import "MAVideoListModel.h"
#import "MRSelectCarouselModel.h"
#import "MATeachIntroduceCtl.h"
#import "MATeachPurchasedCtl.h"

@interface MATeachListCtl ()<UITableViewDelegate, UITableViewDataSource, MATeachListCellDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSourceArray;
@property (nonatomic, strong) NSMutableArray *carouselArray;
@property (nonatomic, assign) NSInteger page;


@end

@implementation MATeachListCtl
- (instancetype)init
{
    self = [super init];
    if (self) {
    
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"魔术教学";
    
    [self setupUI];
    [self.tableView.mj_header beginRefreshing];
}
#pragma mark - 构建UI
- (void)setupUI
{
    [self.view addSubview:self.tableView];
    
//    self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_teach_product"]
//                                                                             style:UIBarButtonItemStylePlain
//                                                                            target:self
//                                                                            action:@selector(rightBarButtonItemClick)];

}

#pragma mark - 数据请求

/**
 刷新
 */
- (void)relodaData
{
    self.page = 0;
    [self dataSource];
}

/**
 加载更多
 */
- (void)loadMore
{
    [self dataSource];
}

/**
 教学视频列表
 */
-(void)dataSource
{
    NSString *url = [NSURL stringURLWithSever:@"Video.getVideoList" parameters:nil];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 //翻页
                                 self.page++;
                                 [self.tableView.mj_header endRefreshing];
                                 if(code == 0)
                                 {
                                     if (self.dataSourceArray.count > 0) {
                                         self.dataSourceArray = nil;
                                     }
                                     NSArray *info = [JSON valueForKey:@"info"];
                                     for (NSDictionary *dic in info) {
                                         MAVideoListModel *model = [[MAVideoListModel alloc] initWithJson:dic];
                                         [self.dataSourceArray addObject:model];
                                     }
                                     [self.tableView reloadData];
                                 }
                             } failure:^(NSError *error) {
                                 [self.tableView.mj_header endRefreshing];
                                 [self showError:MRNetWorkError];
                             }];
}

#pragma mark - TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MATeachListCell *cell = [MATeachListCell cellWithTableView:tableView];
    cell.delegate = self;
    MAVideoListModel *model = [self.dataSourceArray objectAtIndex:indexPath.row];
    [cell cellWithModel:model];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kscreenWidth * 9 / 16 + 50 + 42.5 + 12.5;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MATeachIntroduceCtl *vc = [[MATeachIntroduceCtl alloc] init];
    MAVideoListModel *model = [self.dataSourceArray objectAtIndex:indexPath.row];
    vc.VideoId = model.ID;
    [self pushToViewControllerWithController:vc];
}

#pragma mark - MAVideoHeaderViewDelegate
- (void)MAVideoHeaderViewViewClickWithIndex:(NSInteger)index
{
    MRSelectCarouselModel *model = [self.carouselArray objectAtIndex:index];
    [self pushToViewControllerWithUrl:model.link title:model.name];
}

- (void)rightBarButtonItemClick
{
    MATeachPurchasedCtl *vc = [[MATeachPurchasedCtl alloc] init];
    [self pushToViewControllerWithController:vc];
}

- (void)MATeachListCellFollowButtonButtonClickWithModel:(MAVideoListModel *)model
                                                success:(void (^)(NSInteger code))success
                                                failure:(void (^)(NSError *error))failure
{
    if ([MRUser sharedClient].ID == 0) {
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:[[MALoginCtl alloc] init]];
        [self presentViewController:nav
                           animated:YES
                         completion:nil];
    } else {
        //忽略鉴权
        NSDictionary *parameters = @{@"cid": model.ID,
                                     @"ctype": @"3",
                                     @"action": [NSNumber numberWithBool:!model.collected],
                                     };
        NSString *url = [NSURL stringURLWithSever:@"Liverecord.GetPlaybackList"
                                       parameters:parameters];
        [[MRHttpClient sharedClient] get:url
                              parameters:nil
                                 success:^(NSDictionary *JSON, NSInteger code) {
                                     success(!model.collected);
                                 } failure:^(NSError *error) {
                                     failure(nil);
                                 }];
    }
}

#pragma mark - 懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, kscreenWidth, kscreenHeight - 49 - 64) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = MRBackGroundCloor;
        
        // 上下拉刷新
        [self initMJRefreshWithScrollView:_tableView target:self refreshingAction:@selector(relodaData) pageAction:@selector(loadMore)];
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceArray
{
    if (!_dataSourceArray) {
        _dataSourceArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _dataSourceArray;
}

- (NSMutableArray *)carouselArray
{
    if (!_carouselArray) {
        _carouselArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _carouselArray;
}


@end
