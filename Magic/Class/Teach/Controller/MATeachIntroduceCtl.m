//
//  MATeachIntroduceCtl.m
//  Magic
//
//  Created by 王 on 16/9/27.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MATeachIntroduceCtl.h"
#import "MATeachIntroduceModel.h"
#import "MAPlayer.h"

@interface MATeachIntroduceCtl ()

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIImageView *backImageView;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIImageView *remindImageView;
@property (nonatomic, strong) UILabel *remindLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *priceImageView;
@property (nonatomic, strong) UILabel *priceLabel;

@property (nonatomic, strong) UIView *producerView;
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) MATeachIntroduceModel *model;
@property (nonatomic, strong) MAPlayer *player;
@property (nonatomic, strong) UIView *timeView;
@property (nonatomic, strong) UILabel *timeNowLabel;
@property (nonatomic, strong) UISlider *timeSlider;
@property (nonatomic, strong) UILabel *timeTotleLabel;
@property (nonatomic, strong) NSTimer *timer;


@end

@implementation MATeachIntroduceCtl

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isNeedLogin = NO;
    }
    return self;
}



- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self dataSource];
}

- (void)dataSource
{
    self.model = [[MATeachIntroduceModel alloc] initWithJson:nil];
    [self setupUI];
}

- (void)setupUI
{
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.backView];
    [self.scrollView addSubview:self.backImageView];
    [self.backImageView addSubview:self.timeLabel];
    [self.backImageView addSubview:self.remindImageView];
    [self.backImageView addSubview:self.remindLabel];
    
    [self.backView addSubview:self.titleLabel];
    
    if ([self.model.price integerValue] > 0) {
        [self.backView addSubview:self.priceLabel];
        [self.backView addSubview:self.priceImageView];
        self.titleLabel.frame = CGRectMake(10, 10, kscreenWidth - 30 - CGRectGetMinX(self.priceLabel.frame) - 20, 50);
    } else {
        self.titleLabel.frame = CGRectMake(10, 10, kscreenWidth - 30 - 20, 50);
    }

    
    [self.scrollView addSubview:self.producerView];
    [self.scrollView addSubview:self.contentView];
    
    self.title = self.model.title;

}

- (UIView *)setHeaderViewWithImage:(UIImage *)image title:(NSString *)title
{
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake(10, 7.5, 20, 20);
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    imageView.image = image;
    
    UILabel *label = [[UILabel alloc] init];
    label.text = title;
    label.textColor = MRTextColor;
    label.font = [UIFont systemFontOfSize:13];
    label.frame = CGRectMake(CGRectGetMaxX(imageView.frame) + 10, 0, [label widthForLabelWithHeight:13], 35);
    
    UIView *line = [[UIView alloc] init];
    line.frame = CGRectMake(0, 34.5, kscreenWidth - 30, 0.5);
    line.backgroundColor = MRLineColor;
    
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    view.frame = CGRectMake(0, 0, kscreenWidth - 30, 35);
    
    [view addSubview:imageView];
    [view addSubview:label];
    [view addSubview:line];
    
    return view;
}

- (UILabel *)setLabelWithText:(NSString *)text freamY:(CGFloat)freamY
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = MRTextColor;
    label.font = [UIFont systemFontOfSize:12];
    label.text = text;
    label.frame = CGRectMake(10, freamY, kscreenWidth - 30 - 20, [label heightForLabelWithWidth:kscreenWidth - 30 - 20]);
    return label;
}

/**
 播放视频
 */
- (void)playVideo
{
    [self.player removeFromParentViewController];
    [self.player.view removeFromSuperview];
    
    self.player.url = self.model.videoUrl;
    self.player.view.frame = CGRectMake(0, 0, kscreenWidth, kscreenWidth * 9 / 16);
    [self addChildViewController:self.player];
    [self.view addSubview:self.player.view];
    [self.player play];
}

#pragma mark - 懒加载
- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.frame = CGRectMake(0, 0, kscreenWidth, kscreenHeight - 64);
    }
    return _scrollView;
}

- (UIView *)backView
{
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.frame = CGRectMake(15, kscreenWidth * 9 / 16 - 10, kscreenWidth - 30, 60);
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.cornerRadius = 6;
        _backView.clipsToBounds = YES;
    }
    return _backView;
}
- (UIImageView *)backImageView
{
    if (!_backImageView) {
        _backImageView = [[UIImageView alloc] init];
        _backImageView.frame = CGRectMake(0, 0, kscreenWidth, kscreenWidth * 9 / 16);
        _backImageView.contentMode = UIViewContentModeScaleToFill;
        _backImageView.clipsToBounds = YES;
        _backImageView.userInteractionEnabled = YES;
        [_backImageView sd_setImageWithURL:[NSURL URLWithString:self.model.thumb]
                              placeholderImage:[UIImage imageNamed:@"image_placeHolder"]
                                     completed:nil];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playVideo)];
        [_backImageView addGestureRecognizer:tap];
    }
    return _backImageView;
}

- (UILabel *)timeLabel
{
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.font = [UIFont systemFontOfSize:14];
        _timeLabel.backgroundColor = MRMainColor;
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        _timeLabel.layer.cornerRadius = 3;
        _timeLabel.clipsToBounds = YES;
        _timeLabel.textColor = [UIColor whiteColor];
        
        NSInteger now = [self.model.duration integerValue];
        //避免出现抖动
        if (now >= 0) {
            NSInteger hour = now / 3600;
            NSInteger min = now / 60 - hour * 60;
            NSInteger sec = now - hour * 3600 - min * 60;
            if (hour > 0) {
                self.timeLabel.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", hour, min, sec];
            } else {
                if (min > 0) {
                    _timeLabel.text = [NSString stringWithFormat:@"%02ld:%02ld", min, sec];
                } else {
                    self.timeLabel.text = [NSString stringWithFormat:@"%02ld'", sec];
                }
            }
        }
        _timeLabel.frame = CGRectMake(kscreenWidth - 12 - [_timeLabel widthForLabelWithHeight:14], 12.5, [self.timeLabel widthForLabelWithHeight:14] + 20, 20);
    }
    return _timeLabel;
}

- (UIImageView *)remindImageView
{
    if (!_remindImageView) {
        _remindImageView = [[UIImageView alloc] init];
        _remindImageView.frame = CGRectMake(15, kscreenWidth * 9 / 16 - 25, 20, 20);
        _remindImageView.image = [UIImage imageNamed:@"icon_live_visibleWhite"];
    }
    return _remindImageView;
}

- (UILabel *)remindLabel
{
    if (!_remindLabel) {
        _remindLabel = [[UILabel alloc] init];
        _remindLabel.font = [UIFont systemFontOfSize:13];
        _remindLabel.textColor = [UIColor whiteColor];
        _remindLabel.text = [NSString stringWithFormat:@"%@", self.model.viewNums];
        _remindLabel.frame = CGRectMake(CGRectGetMaxX(self.remindImageView.frame) + 10, CGRectGetMinY(self.remindImageView.frame), [_remindLabel widthForLabelWithHeight:13], 20);
    }
    return _remindLabel;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:15];
        _titleLabel.text = [NSString stringWithFormat:@"%@", self.model.title];

    }
    return _titleLabel;
}

- (UILabel *)priceLabel
{
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        _priceLabel.font = [UIFont systemFontOfSize:15];
        _priceLabel.textColor = MRMainColor;
        _priceLabel.text = [NSString stringWithFormat:@"价格 %@", self.model.price];
        _priceLabel.frame = CGRectMake(kscreenWidth - 30 - 30 - 10 - [_priceLabel widthForLabelWithHeight:15] - 5, 10 + 17.5, [_priceLabel widthForLabelWithHeight:15], 15);
    }
    return _priceLabel;
}


- (UIImageView *)priceImageView
{
    if (!_priceImageView) {
        _priceImageView = [[UIImageView alloc] init];
        _priceImageView.image = [UIImage imageNamed:@"icon_main_coinRed"];
        _priceImageView.contentMode = UIViewContentModeScaleAspectFill;
        _priceImageView.clipsToBounds = YES;
        _priceImageView.frame = CGRectMake(kscreenWidth - 30 - 30 - 10, 10 + 10, 30, 30);
    }
    return _priceImageView;
}

- (UIView *)producerView
{
    if (!_producerView) {
        _producerView = [[UIView alloc] init];
        _producerView.backgroundColor = [UIColor whiteColor];
        _producerView.layer.cornerRadius = 6;
        _producerView.clipsToBounds = YES;
        
        UIView *view = [self setHeaderViewWithImage:[UIImage imageNamed:@"icon_live_visibleBlack"] title:@"出品人"];
        [_producerView addSubview:view];
        UILabel *performerLabel = [self setLabelWithText:[NSString stringWithFormat:@"表演者:%@", self.model.performer] freamY:45];
        UILabel *producersLabel = [self setLabelWithText:[NSString stringWithFormat:@"出品方:%@", self.model.producers] freamY:CGRectGetMaxY(performerLabel.frame) + 5];
        
        UILabel *timeLabel = [self setLabelWithText:[NSString stringWithFormat:@"出品时间:%@", self.model.time] freamY:CGRectGetMaxY(producersLabel.frame) + 5];
        
        [_producerView addSubview:performerLabel];
        [_producerView addSubview:producersLabel];
        [_producerView addSubview:timeLabel];
        
        _producerView.frame = CGRectMake(15, CGRectGetMaxY(self.backView.frame) + 5, kscreenWidth - 30, CGRectGetMaxY(timeLabel.frame) + 10);
    }
    return _producerView;
}

- (UIView *)contentView
{
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        _contentView.backgroundColor = [UIColor whiteColor];
        _contentView.layer.cornerRadius = 6;
        _contentView.clipsToBounds = YES;
        UIView *view = [self setHeaderViewWithImage:[UIImage imageNamed:@"icon_live_visibleBlack"] title:@"介绍"];
        [_contentView addSubview:view];
        
        UILabel *contentLabel = [self setLabelWithText:self.model.content freamY:45];
        [_contentView addSubview:contentLabel];
        _contentView.frame = CGRectMake(15, CGRectGetMaxY(self.producerView.frame) + 5, kscreenWidth - 30, CGRectGetMaxY(contentLabel.frame) + 10);
        
        self.scrollView.contentSize = CGSizeMake(kscreenWidth, CGRectGetMaxY(_contentView.frame) + 10);
    }
    return _contentView;
}

- (MAPlayer *)player
{
    if (!_player) {
        _player = [[MAPlayer alloc] init];
    }
    return _player;
}






@end
