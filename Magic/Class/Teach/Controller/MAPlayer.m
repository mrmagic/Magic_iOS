//
//  MAPlayer.m
//  Magic
//
//  Created by 王 on 16/9/28.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAPlayer.h"
#import <MediaPlayer/MediaPlayer.h>
#import "MAFullViewCtl.h"


@interface MAPlayer ()

@property (nonatomic, strong) MPMoviePlayerController *player;
@property (nonatomic, strong) UIView *timeView;
@property (nonatomic, strong) UILabel *timeNowLabel;
@property (nonatomic, strong) UISlider *timeSlider;
@property (nonatomic, strong) UILabel *timeTotleLabel;
@property (nonatomic, strong) UIButton *startButton;
@property (nonatomic, strong) UIButton *fullButton;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) BOOL isSetup;
@property (nonatomic, assign) BOOL isMoving;
@property (nonatomic, assign) BOOL isStop;
@property (nonatomic, assign) BOOL isFull;

@property (nonatomic, strong) MAFullViewCtl *fullPlayer;

@end

@implementation MAPlayer
- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self pause];
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beginPlay:) name: MPMovieDurationAvailableNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stateChange:) name: MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
}
- (void)playerView
{
    [self.player.view removeFromSuperview];
    self.player = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:self.url]];
    self.player.controlStyle = MPMovieControlStyleNone;
    self.player.scalingMode = MPMovieScalingModeNone;
    [self.player prepareToPlay];
    self.player.shouldAutoplay = YES;
    [self.player.view setFrame:CGRectMake(0, 0, kscreenWidth, kscreenWidth * 9 / 16)];  // player的尺寸
    [self.view addSubview:self.player.view];
    [self.player play];
}

#pragma mark - 播放器设置
//点击方法  扩大进度条的响应区域
- (void)backViewClick:(UITapGestureRecognizer *)sender
{
    CGPoint point = [sender locationInView:[sender view]];
    if (point.x >= self.timeSlider.frame.origin.x && point.x <= CGRectGetMaxX(self.timeSlider.frame)) {
        CGFloat width = point.x - self.timeSlider.frame.origin.x;
        CGFloat lineWidth = self.timeSlider.frame.size.width;
        CGFloat values = width * self.timeSlider.maximumValue / lineWidth;
        
        [self.timeSlider setValue:values animated:YES];
        [self.player setCurrentPlaybackTime:values];
        
        [self play];
    }
}

//滑动方法  扩大进度条的响应区域
- (void)backViewPan:(UIPanGestureRecognizer *)sender
{
    CGPoint point = [sender locationInView:[sender view]];
    if (sender.state == UIGestureRecognizerStateBegan) {
        if (point.x >= self.timeSlider.frame.origin.x && point.x <= CGRectGetMaxX(self.timeSlider.frame)) {
            self.isMoving = YES;
        } else {
            self.isMoving = NO;
        }
    }else if (sender.state == UIGestureRecognizerStateChanged) {
        if (self.isMoving) {
            [self pause];
            CGFloat width = point.x - self.timeSlider.frame.origin.x;
            CGFloat lineWidth = self.timeSlider.frame.size.width;
            CGFloat values = width * self.timeSlider.maximumValue / lineWidth;
            [self.timeSlider setValue:values animated:YES];
            self.isStop = YES;
        }
    }else if (sender.state == UIGestureRecognizerStateEnded){
        if (self.isMoving) {
            self.isStop = NO;
            [self.player setCurrentPlaybackTime:self.timeSlider.value];
            [self play];
        }
    }
}

- (void)addTimer
{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.01
                                                  target:self
                                                selector:@selector(exchangeSlider)
                                                userInfo:nil
                                                 repeats:YES];
    [self.timer fire];
}

- (void)removeTimer
{
    [self.timer invalidate];
    self.timer = nil;
}

- (void)beginPlay:(NSNotification *)note
{
    [self addTimer];
    NSInteger tot = self.player.duration;
    self.timeSlider.minimumValue = 0;
    self.timeSlider.maximumValue = tot;
    NSInteger min = tot / 60;
    NSInteger sec = tot - min * 60;
    self.timeTotleLabel.text = [NSString stringWithFormat:@"%02ld:%02ld", (long)min, (long)sec];
}

- (void)exchangeSlider
{
    NSInteger now = self.player.currentPlaybackTime;
    //避免出现抖动
    if (now >= 0) {
        NSInteger min = now / 60;
        NSInteger sec = now - min * 60;
        self.timeNowLabel.text = [NSString stringWithFormat:@"%02ld:%02ld", (long)min, (long)sec];
        self.timeSlider.value = self.player.currentPlaybackTime;
    }
}

- (void)timeSliderChange
{
    [self removeTimer];
    [self.player pause];
}

- (void)timeSliderChangeEnd
{
    self.player.currentPlaybackTime = self.timeSlider.value;
    [self.player play];
    [self addTimer];
}

- (void)stateChange:(NSNotification *)note
{
    if (self.player.playbackState == MPMoviePlaybackStateStopped) {
        [self removeTimer];
    }
}

- (void)startButtonClick
{
    if (self.player.playbackState == MPMoviePlaybackStatePlaying) {
        [self pause];
        [self.startButton setImage:[UIImage imageNamed:@"icon_evaluate_videoPlay"] forState:UIControlStateNormal];
    } else {
        [self play];
        [self.startButton setImage:[UIImage imageNamed:@"icon_evaluate_videoStop"] forState:UIControlStateNormal];
    }
}

- (void)fullClick
{
    self.isFull = !self.isFull;
    if (self.isFull) {
        [self updateFream];

        CGAffineTransform landscapeTransform = CGAffineTransformMakeRotation(M_PI / 2);
        self.fullPlayer.view.transform = landscapeTransform;
        [self.navigationController presentViewController:self.fullPlayer animated:NO completion:^{
            [self.fullPlayer.view addSubview:self.player.view];
            self.player.view.center = self.fullPlayer.view.center;
            [self.player play];
            [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionLayoutSubviews animations:^{
                self.player.view.frame = self.fullPlayer.view.bounds;
            }
                             completion:nil];
        }];
    } else {
        [self updateFream];
        [self.fullPlayer dismissViewControllerAnimated:NO completion:^{
            [self.player play];

            [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionLayoutSubviews animations:^{
                [self.view addSubview:self.player.view];
                self.player.view.frame = CGRectMake(0,0,kscreenWidth,kscreenWidth * 9 / 16);
            }
                             completion:nil];
           
        }];
    }
}
- (void)updateFream
{
    if (self.isFull) {
        self.startButton.hidden = YES;
        self.timeNowLabel.frame = CGRectMake(15, 3, [self.timeNowLabel widthForLabelWithHeight:12] + 2, 12);
        self.timeView.frame = CGRectMake(0, kscreenWidth - 30, kscreenHeight, 30);
        self.timeSlider.frame = CGRectMake(CGRectGetMaxX(self.timeNowLabel.frame) + 5, 3, kscreenHeight - 2 * (CGRectGetMaxX(self.timeNowLabel.frame) + 5) - 30, 12);
        self.timeTotleLabel.frame = CGRectMake(CGRectGetMaxX(self.timeSlider.frame) + 5, 3, [self.timeTotleLabel widthForLabelWithHeight:12] + 2, 12);
        self.fullButton.frame = CGRectMake(kscreenHeight - 15 - 13,0, 13, 18);

    } else {
        self.startButton.hidden = NO;
        self.timeNowLabel.frame = CGRectMake(40, 3, [self.timeNowLabel widthForLabelWithHeight:12] + 2, 12);
        self.timeView.frame = CGRectMake(0, self.view.frame.size.height - 30, kscreenWidth, 30);
        self.timeSlider.frame = CGRectMake(CGRectGetMaxX(self.timeNowLabel.frame) + 5, 3, kscreenWidth - 2 * (CGRectGetMaxX(self.timeNowLabel.frame) + 5), 12);
        self.timeTotleLabel.frame = CGRectMake(CGRectGetMaxX(self.timeSlider.frame) + 5, 3, [self.timeTotleLabel widthForLabelWithHeight:12] + 2, 12);
        self.fullButton.frame = CGRectMake(kscreenWidth - 15 - 13,0, 13, 18);
    }
}

- (void)setControl
{
    if (!self.isSetup) {
        
        [self playerView];
        [self addNotification];
        
        //播放
        self.timeView = [[UIView alloc] init];
        self.timeView.tag = 1000;

        self.timeView.frame = CGRectMake(0, self.view.frame.size.height - 30, kscreenWidth, 30);
        self.timeView.backgroundColor = [UIColor clearColor];
        [self.player.view addSubview:self.timeView];
        
        //暂停按钮
        self.startButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.startButton.tag = 1001;
        self.startButton.enabled = YES;
        self.startButton.tintColor = [UIColor whiteColor];
        [self.startButton setImage:[UIImage imageNamed:@"icon_evaluate_videoStop"] forState:UIControlStateNormal];
        [self.startButton addTarget:self action:@selector(startButtonClick) forControlEvents:UIControlEventTouchUpInside];
        self.startButton.frame = CGRectMake(15,0, 13, 18);
        [self.timeView addSubview:self.startButton];
        
        //播放时间
        self.timeNowLabel = [[UILabel alloc] init];
        self.timeNowLabel.tag = 1002;
        self.timeNowLabel.font = [UIFont systemFontOfSize:12];
        self.timeNowLabel.textColor = [UIColor whiteColor];
        self.timeNowLabel.text = @"00:00";
        self.timeNowLabel.frame = CGRectMake(40, 3, [self.timeNowLabel widthForLabelWithHeight:12] + 2, 12);
        [self.timeView addSubview:self.timeNowLabel];
        
        //进度条
        self.timeSlider = [[UISlider alloc] init];
        self.timeSlider.tag = 1003;
        self.timeSlider.frame = CGRectMake(CGRectGetMaxX(self.timeNowLabel.frame) + 5, 3, kscreenWidth - 2 * (CGRectGetMaxX(self.timeNowLabel.frame) + 5), 12);
        [self.timeSlider addTarget:self action:@selector(timeSliderChange) forControlEvents:UIControlEventTouchDown];
        [self.timeSlider addTarget:self action:@selector(timeSliderChangeEnd) forControlEvents:UIControlEventTouchUpInside];
        [self.timeSlider setMinimumTrackImage:[UIImage imageNamed:@"image_evaluate_videoLine"] forState:UIControlStateNormal];
        [self.timeSlider setMaximumTrackImage:[UIImage imageNamed:@"image_evaluate_videoLine"] forState:UIControlStateNormal];
        [self.timeSlider setThumbImage:[UIImage imageNamed:@"icon_evaluate_videoTime"] forState:UIControlStateNormal];
        [self.timeView addSubview:self.timeSlider];
        
        //总时间
        self.timeTotleLabel = [[UILabel alloc] init];
        self.timeTotleLabel.tag = 1004;

        self.timeTotleLabel.font = [UIFont systemFontOfSize:12];
        self.timeTotleLabel.textColor = [UIColor whiteColor];
        self.timeTotleLabel.text = @"00:00";
        self.timeTotleLabel.frame = CGRectMake(CGRectGetMaxX(self.timeSlider.frame) + 5, 3, [self.timeTotleLabel widthForLabelWithHeight:12] + 2, 12);
        [self.timeView addSubview:self.timeTotleLabel];
        
        //全屏按钮
        self.fullButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.fullButton.enabled = YES;
        self.fullButton.tag = 1005;
        self.fullButton.tintColor = [UIColor whiteColor];
        [self.fullButton setBackgroundImage:[UIImage imageNamed:@"icon_evaluate_videoStop"] forState:UIControlStateNormal];
        [self.fullButton addTarget:self action:@selector(fullClick) forControlEvents:UIControlEventTouchUpInside];
        self.fullButton.frame = CGRectMake(kscreenWidth - 15 - 13,0, 13, 18);
        [self.timeView addSubview:self.fullButton];
    }
}


- (void)play
{
    [self setControl];
    [self.player play];
}

- (void)pause
{
    [self.player pause];
}

- (void)stop
{
    [self.player stop];
}

- (MAFullViewCtl *)fullPlayer
{
    if (!_fullPlayer) {
        _fullPlayer = [[MAFullViewCtl alloc] init];
    }
    return _fullPlayer;
}


@end
