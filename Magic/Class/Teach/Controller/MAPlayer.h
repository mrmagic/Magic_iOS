//
//  MAPlayer.h
//  Magic
//
//  Created by 王 on 16/9/28.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAViewController.h"

@protocol MAPlayerDelegate <NSObject>

@optional


@end

@interface MAPlayer : MAViewController

//播放链接
@property (nonatomic, copy) NSString *url;


/**
 播放
 */
- (void)play;


/**
 暂停
 */
- (void)pause;


/**
 停止
 */
- (void)stop;

@end
