//
//  MATeachPurchasedCtl.m
//  Magic
//
//  Created by 王 on 16/9/27.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MATeachPurchasedCtl.h"

@interface MATeachPurchasedCtl ()

@end

@implementation MATeachPurchasedCtl

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isNeedLogin = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"教学库";
}


@end
