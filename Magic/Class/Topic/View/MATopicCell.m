//
//  MATopicCell.m
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MATopicCell.h"
#import "NSDate+Util.h"

#define buttonWidth 20

@interface MATopicCell ()

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *titleLable;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *countLabel;
@property (nonatomic, strong) UIButton *followButton;
@property (nonatomic, strong) UIButton *chatButton;
@property (nonatomic, strong) UIButton *shareButton;
@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) MATopicModel *model;


@end

@implementation MATopicCell

+(MATopicCell *)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"MATopicCell";
    [tableView registerClass:[MATopicCell class] forCellReuseIdentifier:ID];
    MATopicCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = MRBackGroundCloor;
    
    return cell;
}

- (void)cellWithModel:(MATopicModel *)model
{
    [self setupUI];
    self.model = model;
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.imageUrl]
                          placeholderImage:[UIImage imageNamed:@"image_placeHolder"]
                                 completed:nil];
    
    self.titleLable.text = model.title;
    CGFloat titleHeight = [self.titleLable heightForLabelWithWidth:kscreenWidth - 30 - 120 - 10];
    if (titleHeight > 37) {
        titleHeight = 37;
    }
    self.titleLable.frame = CGRectMake(120, 20, kscreenWidth - 30 - 120 - 10, titleHeight);
    
    self.contentLabel.text = model.content;
    CGFloat contentHeight = [self.contentLabel heightForLabelWithWidth:kscreenWidth - 30 - 120 - 10];
    if (contentHeight > 37) {
        contentHeight = 37;
    }
    self.contentLabel.frame = CGRectMake(120, CGRectGetMaxY(self.titleLable.frame) + 5, kscreenWidth - 30 - 120 - 10, contentHeight);
    
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"M月d日"];
    NSString *str =  [NSDate timeWithUnixTimestamp:[model.time integerValue] formatter:formatter];
    self.timeLabel.text = str;
    self.timeLabel.frame = CGRectMake(10, CGRectGetMaxY(self.iconImageView.frame) + 12.5, [self.timeLabel widthForLabelWithHeight:10], 10);
    
    
    self.countLabel.text = [NSString stringWithFormat:@"帖子数%@", model.count];
    self.countLabel.frame = CGRectMake(CGRectGetMaxY(self.iconImageView.frame) - [self.countLabel widthForLabelWithHeight:10], CGRectGetMaxY(self.iconImageView.frame) + 12.5, [self.countLabel widthForLabelWithHeight:10], 10);
}

- (void)setupUI
{
    [self addSubview:self.backView];
    [self.backView addSubview:self.iconImageView];
    [self.backView addSubview:self.titleLable];
    [self.backView addSubview:self.contentLabel];
    [self.backView addSubview:self.timeLabel];
    [self.backView addSubview:self.countLabel];
    [self.backView addSubview:self.followButton];
    [self.backView addSubview:self.chatButton];
    [self.backView addSubview:self.shareButton];
    [self.backView addSubview:self.lineView];
}

#pragma mark - 按钮点击方法
- (void)followButtonButtonClick:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(MATopicCellFollowButtonButtonClickWithModel:)]) {
        [self.delegate MATopicCellFollowButtonButtonClickWithModel:self.model];
    }
}
- (void)chatButtonClick:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(MATopicCellChatButtonClickWithModel:)]) {
        [self.delegate MATopicCellChatButtonClickWithModel:self.model];
    }
}
- (void)shareButtonButtonClick:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(MATopicCellShareButtonButtonClickWithModel:)]) {
        [self.delegate MATopicCellShareButtonButtonClickWithModel:self.model];
    }
}

#pragma mark - 懒加载
- (UIView *)backView
{
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.frame = CGRectMake(15, 0, kscreenWidth - 30, 157.5);
        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}

- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.frame = CGRectMake(10, 10, 100, 100);
    }
    return _iconImageView;
}
- (UILabel *)titleLable
{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc] init];
        _titleLable.font = [UIFont systemFontOfSize:15];
    }
    return _titleLable;
}

- (UILabel *)contentLabel
{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.font = [UIFont systemFontOfSize:15];
    }
    return _contentLabel;
}

- (UILabel *)timeLabel
{
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.font = [UIFont systemFontOfSize:10];
    }
    return _timeLabel;
}

- (UILabel *)countLabel
{
    if (!_countLabel) {
        _countLabel = [[UILabel alloc] init];
        _countLabel.font = [UIFont systemFontOfSize:10];
    }
    return _countLabel;
}

- (UIButton *)followButton
{
    if (!_followButton) {
        _followButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _followButton.frame = CGRectMake(kscreenWidth - 30 - buttonWidth * 3 - 10 * 3, 135 - buttonWidth, buttonWidth, buttonWidth);
        _followButton.backgroundColor = MRMainColor;
        [_followButton addTarget:self action:@selector(followButtonButtonClick:) forControlEvents:UIControlEventTouchUpInside];

    }
    return _followButton;
}

- (UIButton *)chatButton
{
    if (!_chatButton) {
        _chatButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _chatButton.frame = CGRectMake(CGRectGetMaxX(self.followButton.frame) + 10, CGRectGetMinY(self.followButton.frame), buttonWidth, buttonWidth);
        _chatButton.backgroundColor = MRMainColor;
        [_chatButton addTarget:self action:@selector(chatButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _chatButton;
}

- (UIButton *)shareButton
{
    if (!_shareButton) {
        _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _shareButton.frame = CGRectMake(CGRectGetMaxX(self.chatButton.frame) + 10, CGRectGetMinY(self.chatButton.frame), buttonWidth, buttonWidth);
        _shareButton.backgroundColor = MRMainColor;
        [_shareButton addTarget:self action:@selector(shareButtonButtonClick:) forControlEvents:UIControlEventTouchUpInside];

    }
    return _shareButton;
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.frame = CGRectMake(0, 150, kscreenWidth - 30, 7.5);
        _lineView.backgroundColor = MRBackGroundCloor;
    }
    return _lineView;
}

@end
