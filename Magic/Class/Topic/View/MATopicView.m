//
//  MATopicView.m
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MATopicView.h"

@interface MATopicView ()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *countLabel;
@property (nonatomic, strong) MATopicSearchModel *model;

@end


@implementation MATopicView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)updateWithModel:(MATopicSearchModel *)model
{
    [self setupUI];
    
    self.model = model;
    
    self.titleLabel.text = [NSString stringWithFormat:@"#%@#",model.title];
    self.countLabel.text = [NSString stringWithFormat:@"帖子数 %@",model.count];

    self.titleLabel.frame = CGRectMake(10, 7.5, [self.titleLabel widthForLabelWithHeight:10], 10);
    self.countLabel.frame = CGRectMake(10, CGRectGetMaxY(self.titleLabel.frame) + 10, [self.countLabel widthForLabelWithHeight:10], 10);
    
    CGFloat width = [self.titleLabel widthForLabelWithHeight:10] > [self.countLabel widthForLabelWithHeight:10] ? [self.titleLabel widthForLabelWithHeight:10] + 20 : [self.countLabel widthForLabelWithHeight:10] + 20;
    self.frame = CGRectMake(0, 0, width, 40);
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewClick:)];
    [self addGestureRecognizer:tap];
    
}

- (void)viewClick:(UITapGestureRecognizer *)sender
{
    if ([self.delegate respondsToSelector:@selector(MMATopicViewClickWithModel:)]) {
        [self.delegate MMATopicViewClickWithModel:self.model];
    }
}

- (void)setupUI
{
    [self addSubview:self.titleLabel];
    [self addSubview:self.countLabel];
}

#pragma mark - 懒加载
- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:10];
    }
    return _titleLabel;
}

- (UILabel *)countLabel
{
    if (!_countLabel) {
        _countLabel = [[UILabel alloc] init];
        _countLabel.font = [UIFont systemFontOfSize:10];
        _countLabel.textColor = MRMainColor;

    }
    return _countLabel;
}

@end
