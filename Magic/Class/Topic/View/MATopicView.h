//
//  MATopicView.h
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MATopicSearchModel.h"

@protocol MATopicViewDelegate <NSObject>

@optional

- (void)MMATopicViewClickWithModel:(MATopicSearchModel *)model;

@end


@interface MATopicView : UIView

@property (nonatomic, assign) id<MATopicViewDelegate> delegate;


- (void)updateWithModel:(MATopicSearchModel *)model;


@end
