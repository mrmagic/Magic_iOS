//
//  MATopicCell.h
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MATopicModel.h"

@protocol MATopicCellDelegate <NSObject>

@optional


- (void)MATopicCellFollowButtonButtonClickWithModel:(MATopicModel *)model;

- (void)MATopicCellChatButtonClickWithModel:(MATopicModel *)model;

- (void)MATopicCellShareButtonButtonClickWithModel:(MATopicModel *)model;


@end

@interface MATopicCell : UITableViewCell

@property(nonatomic, assign) id<MATopicCellDelegate> delegate;

+(MATopicCell *)cellWithTableView:(UITableView *)tableView;

- (void)cellWithModel:(MATopicModel *)model;

@end
