//
//  MAFoundListCtl.m
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MATopicListCtl.h"
#import "MATopicCell.h"
#import "MATopicView.h"
#import "MATopicModel.h"
#import "MATopicSearchModel.h"

@interface MATopicListCtl ()<UITableViewDelegate, UITableViewDataSource, MATopicCellDelegate, MATopicViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) NSMutableArray *recommendArray;
@property (nonatomic, strong) NSMutableArray *dataSourceArray;

@end

@implementation MATopicListCtl

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self dataSource];
    
    [self.view addSubview:self.tableView];
    [self navSet];
}

- (void)navSet
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_live_magicBlack"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(searchButtonClick:)];
}
- (void)dataSource
{
    for (int i = 0; i < 10; i ++) {
        MATopicSearchModel *model = [[MATopicSearchModel alloc] initWithJson:nil];
        [self.recommendArray addObject:model];
    }
    
    for (int i = 0; i < 10; i ++) {
        MATopicModel *model = [[MATopicModel alloc] initWithJson:nil];
        [self.dataSourceArray addObject:model];
    }
    [self.tableView reloadData];
}

#pragma mark - 自定义方法
- (UIView *)titleViewWithTitle:(NSString *)title
{
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(0, 0, kscreenWidth, 40);
    view.backgroundColor = MRBackGroundCloor;
    UIView *lineView = [[UIView alloc] init];
    lineView.frame = CGRectMake(5, 17.5, 2.5, 12.5);
    lineView.backgroundColor = MRMainColor;
    
    UILabel *label = [[UILabel alloc] init];
    label.font = [UIFont systemFontOfSize:13];
    label.text = title;
    label.frame = CGRectMake(CGRectGetMaxX(lineView.frame) + 5, 17.5, [label widthForLabelWithHeight:13], 13);

    
    [view addSubview:lineView];
    [view addSubview:label];
    return view;
}

- (void)searchButtonClick:(UIBarButtonItem *)sender
{
    NSLog(@"搜索");
}

//热门搜索更多点击
- (void)moreButtonClick:(UIButton *)sender
{
    NSLog(@"更多热搜");
}

- (void)MMATopicViewClickWithModel:(MATopicSearchModel *)model
{
    NSLog(@"热搜点击");
}

#pragma mark - MATopicCellDelegate
- (void)MATopicCellFollowButtonButtonClickWithModel:(MATopicModel *)model
{
   NSLog(@"点赞");
}

- (void)MATopicCellChatButtonClickWithModel:(MATopicModel *)model
{
    NSLog(@"聊天");

}

- (void)MATopicCellShareButtonButtonClickWithModel:(MATopicModel *)model
{
    NSLog(@"分享");

}

#pragma mark -UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MATopicCell *cell = [MATopicCell cellWithTableView:tableView];
    MATopicModel *model = [self.dataSourceArray objectAtIndex:indexPath.row];
    [cell cellWithModel:model];
    cell .delegate = self;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 157.5;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
}


#pragma mark - 懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, kscreenWidth, kscreenHeight - 49 - 64) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = MRBackGroundCloor;
        
        _tableView.tableHeaderView = self.headerView;
    }
    return _tableView;
}

- (UIView *)headerView
{
    if (!_headerView) {
        _headerView = [[UIView alloc] init];
        _headerView.frame = CGRectMake(0, 0, kscreenWidth, 120);
        _headerView.backgroundColor = MRBackGroundCloor;
        
        UIView *topView = [self titleViewWithTitle:@"热门搜索"];
        [_headerView addSubview:topView];
        
        CGFloat width = 15;
        for (int i = 0; i < self.recommendArray.count; i++) {
            MATopicView *view = [[MATopicView alloc] init];
            MATopicSearchModel *model = [self.recommendArray objectAtIndex:i];
            [view updateWithModel:model];
            view.frame = CGRectMake(width, 40, view.frame.size.width, 40);
            width = view.frame.size.width + width + 10;
            view.delegate = self;
            if (width < kscreenWidth - 30 ) {
                [_headerView addSubview:view];
            }
        }
        if (width > kscreenWidth - 30) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(kscreenWidth - 15 - 20, 40, 15, 40);
            button.backgroundColor = [UIColor redColor];
            [button addTarget:self action:@selector(moreButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            [_headerView addSubview:button];
        }
        
        UIView *tableTitleView = [self titleViewWithTitle:@"热门话题"];
        tableTitleView.frame = CGRectMake(0, 80, kscreenWidth, 40);
        
        [_headerView addSubview:topView];
        
        [_headerView addSubview:tableTitleView];
    }
    return _headerView;
}

-(NSMutableArray *)recommendArray
{
    if (!_recommendArray) {
        _recommendArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _recommendArray;
}

- (NSMutableArray *)dataSourceArray
{
    if (!_dataSourceArray) {
        _dataSourceArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _dataSourceArray;
}

@end
