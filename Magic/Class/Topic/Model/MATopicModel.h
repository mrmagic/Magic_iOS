//
//  MATopicModel.h
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MRBaseModel.h"

@interface MATopicModel : MRBaseModel



@property (nonatomic, copy) NSString *title;      //标题
@property (nonatomic, copy) NSString *content;     //描述
@property (nonatomic, copy) NSString *time;       //时间
@property (nonatomic, copy) NSString *count;      //帖子数量
@property (nonatomic, copy) NSString *imageUrl;   //大图地址
@property (nonatomic, assign) BOOL    isFollow;   //是否关注

@end
