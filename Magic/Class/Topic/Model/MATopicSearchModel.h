//
//  MATopicSearchModel.h
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MRBaseModel.h"

@interface MATopicSearchModel : MRBaseModel

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *count;
@property (nonatomic, copy) NSString *url;

@end
