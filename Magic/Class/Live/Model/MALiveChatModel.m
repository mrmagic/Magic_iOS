//
//  MALiveChatModel.m
//  Magic
//
//  Created by 王 on 16/9/30.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALiveChatModel.h"

@implementation MALiveChatModel

- (instancetype)initWithJson:(NSDictionary *)json
{
    self = [super init];
    if (self) {
        _iconUrl = [self checkForNull:[json objectForKey:@"uhead"]];
        _userName = [self checkForNull:[json objectForKey:@"uname"]];
        _content = [self checkForNull:[json objectForKey:@"ct"]];
        if (_userName.length > 0) {
            UILabel *nameLable = [[UILabel alloc] init];
            nameLable.font = [UIFont systemFontOfSize:15];
            nameLable.text = _userName;
            CGFloat nameWidth = [nameLable widthForLabelWithHeight:15] + 30;
            
            UILabel *contentlabel = [[UILabel alloc] init];
            contentlabel.font = [UIFont systemFontOfSize:13];
            contentlabel.text = _content;
            CGFloat contentWidth = [contentlabel widthForLabelWithHeight:13];
            if (contentWidth > kscreenWidth - 30 - 10 - 30) {
                _viewHeight = 10 + 20 + [contentlabel heightForLabelWithWidth:kscreenWidth - 30 - 10 - 30] + 7;
            } else {
                _viewHeight = 50;
            }
            
            CGFloat width = nameWidth > contentWidth ? nameWidth : contentWidth;
            _viewWidth = width > kscreenWidth - 30 - 30 - 10 ? kscreenWidth - 30 : 30 + 10 + width;
        } else {
            UILabel *label = [[UILabel alloc] init];
            label.text = _content;
            label.font = [UIFont systemFontOfSize:15];
            CGFloat width = [label widthForLabelWithHeight:15];
            _viewHeight = [label heightForLabelWithWidth:kscreenWidth - 30 - 10] + 10;
            _viewWidth = width > kscreenWidth - 30 - 10 ? kscreenWidth - 30 : 10 + width;
        }
    }
    return self;
}

@end
