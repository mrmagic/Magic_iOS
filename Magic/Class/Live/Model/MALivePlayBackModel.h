//
//  MALivePlayBackModel.h
//  Magic
//
//  Created by 王 on 16/9/29.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MRBaseModel.h"

@interface MALivePlayBackModel : MRBaseModel

@property (nonatomic, copy) NSString *userID;           //主播ID
@property (nonatomic, copy) NSString *userName;         //主播名字
@property (nonatomic, copy) NSString *avatar;           //头像
@property (nonatomic, copy) NSString *starttime;        //开始时间
@property (nonatomic, copy) NSString *cover;            //封面
@property (nonatomic, copy) NSString *title;            //主题
@property (nonatomic, copy) NSString *videoId;          //视频ID
@property (nonatomic, copy) NSString *userLogin;        //主播电话
@property (nonatomic, copy) NSString *viewnums;         //人数
@property (nonatomic, copy) NSString *videoUrl;         //视频地址
@property (nonatomic, assign) BOOL collected;           //收藏

@end
