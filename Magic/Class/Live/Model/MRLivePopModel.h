
//
//  弹窗模型
//  iphoneLive
//
//  Created by Ty
//  Copyright © 2016年 cat. All rights reserved.
//

#import "MRBaseModel.h"

@interface MRLivePopModel : MRBaseModel

@property(nonatomic,copy)NSString *avatar;

@property(nonatomic,copy)NSString *name;

@property(nonatomic,copy)NSString *fansnum;

@property(nonatomic,copy)NSString *attention;

@property(nonatomic,copy)NSString * introduce;

@property(nonatomic,copy)NSString *doAttention;


-(instancetype)initWithDic:(NSDictionary *)dic;

+(instancetype)modelWithDic:(NSDictionary *)dic;

@end
