
//
//  弹窗模型
//  iphoneLive
//
//  Created by Ty
//  Copyright © 2016年 cat. All rights reserved.
//

#import "MRLivePopModel.h"

@implementation MRLivePopModel


-(instancetype)initWithDic:(NSDictionary *)dic{
    self = [super init];
    if (self) {
        self.avatar = [dic valueForKey:@"avatar"];
        self.name = [dic valueForKey:@"user_nicename"];
        self.fansnum = [dic valueForKey:@"fansnum"];
        //self.attention = [dic valueForKey:@"attention"];
        self.introduce = [dic valueForKey:@"signature"];
    }
    return self;
}

+(instancetype)modelWithDic:(NSDictionary *)dic{
    return [[self alloc]initWithDic:dic];
}

@end
