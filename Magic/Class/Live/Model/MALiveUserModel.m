//
//  围观用户
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALiveUserModel.h"

@interface MALiveUserModel ()


@end

@implementation MALiveUserModel

-(instancetype)initWithJson:(NSDictionary *)json
{
    self = [super init];
    if (self) {
        _iconName = [self checkForNull:[json valueForKey:@"avatar"]];
        _userID = [self checkForNull:[json valueForKey:@"id"]];
        _user_nicename = [self checkForNull:[json valueForKey:@"user_nicename"]];
        _signature = [self checkForNull:[json valueForKey:@"signature"]];
        _sex = [self checkForNull:[json valueForKey:@"sex"]];
        _level = [self checkForNull:[json valueForKey:@"level"]];
        _city = [self checkForNull:[json valueForKey:@"city"]];
        
        _avatar = [self checkForNull:[json valueForKey:@"avatar"]];
        _name = [self checkForNull:[json valueForKey:@"user_nicename"]];
        _fansnum = [self checkForNull:[json valueForKey:@"fansnum"]];
        _introduce = [self checkForNull:[json valueForKey:@"signature"]];
        
        _isMagic = [self checkForNull:[json valueForKey:@"ismagic"]];
        
    }
    return self;
    
}


@end
