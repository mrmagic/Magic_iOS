//
//  MALiveAnnounceModel.h
//  Magic
//
//  Created by 王 on 16/9/29.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MRBaseModel.h"

@interface MALiveAnnounceModel : MRBaseModel

@property (nonatomic, copy) NSString *userID;           //主播id
@property (nonatomic, copy) NSString *userName;         //主播名字
@property (nonatomic, copy) NSString *avatar;           //头像
@property (nonatomic, copy) NSString *cover;            //封面
@property (nonatomic, copy) NSString *fee;              //直播id
@property (nonatomic, copy) NSString *poster;           //海报
@property (nonatomic, copy) NSString *showtime;         //开始时间
@property (nonatomic, copy) NSString *showtype;         //直播类型
@property (nonatomic, copy) NSString *title;            //主题




@end
