//
//  MALiveMagicModel.m
//  Magic
//
//  Created by 王 on 16/9/26.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALiveMagicModel.h"

@implementation MALiveMagicModel

- (instancetype)initWithJson:(NSDictionary *)json
{
    self = [super init];
    if (self) {
        _ID = [self checkForNull:[json objectForKey:@"id"]];
        _avatar = [self checkForNull:[json objectForKey:@"avatar"]];
        _userName = [self checkForNull:[json objectForKey:@"user_nicename"]];
        _votestotal = [self checkForNull:[json objectForKey:@"votestotal"]];
    }
    return self;
}

@end
