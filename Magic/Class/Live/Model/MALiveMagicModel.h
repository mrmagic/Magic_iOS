//
//  MALiveMagicModel.h
//  Magic
//
//  Created by 王 on 16/9/26.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MRBaseModel.h"

@interface MALiveMagicModel : MRBaseModel

@property (nonatomic, copy) NSString *ID;         //魔术师ID
@property (nonatomic, copy) NSString *avatar;     //头像
@property (nonatomic, copy) NSString *userName;   //魔术师名字
@property (nonatomic, copy) NSString *votestotal; //投票数量


@end
