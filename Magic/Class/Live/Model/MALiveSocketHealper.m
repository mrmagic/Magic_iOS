//
//  MALiveSocketHealper.m
//  Magic
//
//  Created by 王 on 16/10/5.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALiveSocketHealper.h"

@implementation MALiveSocketHealper

/**
 回去时间戳
 */
+ (CGFloat)getTimeStamp
{
    NSTimeZone *zone = [NSTimeZone defaultTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate:[NSDate date]];
    NSDate *localeDate = [[NSDate date] dateByAddingTimeInterval:interval];
    NSTimeInterval timeInterval = [localeDate timeIntervalSince1970];
    return timeInterval;
}

/**
 发送心跳包
 
 @param socket  socket
 */
+ (void)SocketHealperSendHeartbeatWithSocket:(SocketIOClient *)socket
{
    [socket emit:@"heartbeat" with:@[@"i am a live"]];
}


/**
 关闭直播间
 */
+(void)SocketHealperCloseRoomWithSocket:(SocketIOClient *)socket
{
    CGFloat timestamp = [self getTimeStamp];
    NSArray *msgData =@[
                        @{
                            @"msg": @[
                                    @{
                                        @"_method_": @"SendMsg",
                                        @"action": @"18",
                                        @"ct":@"直播关闭",
                                        @"msgtype": @"1",
                                        @"timestamp": [NSString stringWithFormat:@"%f", timestamp],
                                        @"tougood": @"",
                                        @"touid": @"",
                                        @"touname": @"",
                                        @"ugood": [MRUser sharedClient].ID,
                                        @"uid": [MRUser sharedClient].ID,
                                        @"uname": [MRUser sharedClient].user_nicename,
                                        @"equipment": @"app",
                                        @"roomnum": [MRUser sharedClient].ID
                                        }
                                    ],
                            @"retcode": @"000000",
                            @"retmsg": @"OK"
                            }
                        ];
    [socket emit:@"broadcast" with:msgData];
    
    [socket disconnect];
    [socket off:@""];

}

/**
 关闭直播间
 
 @param socket                          socket
 @param success                         成功的回调
 @param failure                         失败的回调
 */
+ (void)SocketHealperCloseRoomWithSocket:(SocketIOClient *)socket
                                 success:(void (^)(NSInteger code))success
                                 failure:(void (^)(NSString *error))failure
{
    
    NSString *url = [NSURL stringURLWithSever:@"User.stopRoom" parameters:nil];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 if (code == 0) {
                                     [self SocketHealperCloseRoomWithSocket:socket];
                                 } else {
                                     NSString *msg = [JSON valueForKey:@"msg"];
                                     failure(msg);
                                 }
                             } failure:^(NSError *error) {
                                 failure(MRNetWorkError);
                             }];

}


/**
 发送聊天
 
 @param socket  socket
 @param roomID 主播ID
 @param content 聊天内容
 */
+ (void)SocketHealperSendChatWithSocket:(SocketIOClient *)socket
                                 roomID:(NSString *)roomID
                                content:(NSString *)content
{
    CGFloat timestamp = [self getTimeStamp];
    
    NSArray *msgData =@[
                        @{
                            @"msg": @[
                                    @{
                                        @"_method_": @"SendMsg",
                                        @"action": @"0",
                                        @"ct": content,
                                        @"msgtype": @"2",
                                        @"timestamp": [NSString stringWithFormat:@"%f", timestamp],
                                        @"tougood": @"",
                                        @"touid": @"0",
                                        @"city":[MRUser sharedClient].city,
                                        @"touname": @"",
                                        @"ugood": [MRUser sharedClient].ID,
                                        @"uid": [MRUser sharedClient].ID,
                                        @"uname": [MRUser sharedClient].user_nicename,
                                        @"equipment": @"app",
                                        @"roomnum": roomID,
                                        @"usign":[MRUser sharedClient].signature,
                                        @"uhead":[MRUser sharedClient].avatar,
                                        @"level":[NSNumber numberWithInteger:0],
                                        @"sex":[MRUser sharedClient].sex
                                        }
                                    ],
                            @"retcode": @"000000",
                            @"retmsg": @"OK"
                            }
                        ];
    [socket emit:@"broadcast" with:msgData];
}

/**
 请求僵尸粉
 
 @param socket  socket
 */
+ (void)SocketHealperRequestZombieWithSocket:(SocketIOClient *)socket
{
    CGFloat timestamp = [self getTimeStamp];
    NSArray *msgData =@[
                        @{
                            @"msg": @[
                                    @{
                                        @"_method_": @"requestFans",
                                        @"timestamp":[NSString stringWithFormat:@"%f", timestamp],
                                        @"msgtype": @"0",
                                        }
                                    ],
                            @"retcode": @"000000",
                            @"retmsg": @"OK"
                            }
                        ];
    
    [socket emit:@"broadcast" with:msgData];
}


@end
