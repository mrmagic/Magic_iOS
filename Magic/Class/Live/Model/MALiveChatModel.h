//
//  MALiveChatModel.h
//  Magic
//
//  Created by 王 on 16/9/30.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MRBaseModel.h"

@interface MALiveChatModel : MRBaseModel

@property (nonatomic, copy) NSString *iconUrl;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *content;

@property (nonatomic, assign) CGFloat viewHeight;
@property (nonatomic, assign) CGFloat viewWidth;


@end
