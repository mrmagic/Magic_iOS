//
//  MALiveAnnounceModel.m
//  Magic
//
//  Created by 王 on 16/9/29.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALiveAnnounceModel.h"

@implementation MALiveAnnounceModel

- (instancetype)initWithJson:(NSDictionary *)json
{
    self = [super init];
    if (self) {
        _userID = [self checkForNull:[json objectForKey:@"uid"]];
        _userName = [self checkForNull:[json objectForKey:@"user_nicename"]];
        _avatar = [self checkForNull:[json objectForKey:@"avatar"]];
        _cover = [self checkForNull:[json objectForKey:@"cover"]];
        _fee = [self checkForNull:[json objectForKey:@"fee"]];
        _showtime = [self checkForNull:[json objectForKey:@"showtime"]];
        _showtype = [self checkForNull:[json objectForKey:@"showtype"]];
        _title = [self checkForNull:[json objectForKey:@"subject"]];
        _poster = [self checkForNull:[json objectForKey:@"poster"]];
    }
    return self;
}

@end
