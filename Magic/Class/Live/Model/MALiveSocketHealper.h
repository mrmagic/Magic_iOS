//
//  MALiveSocketHealper.h
//  Magic
//
//  Created by 王 on 16/10/5.
//  Copyright © 2016年 王. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MALiveSocketHealper : NSObject


/**
 发送心跳包
 
 @param socket  socket
 */
+ (void)SocketHealperSendHeartbeatWithSocket:(SocketIOClient *)socket;


/**
 请求僵尸粉
 
 @param socket  socket
 */
+ (void)SocketHealperRequestZombieWithSocket:(SocketIOClient *)socket;

/**
 关闭直播间

 @param socket                          socket
 @param success                         成功的回调
 @param failure                         失败的回调
 */
+ (void)SocketHealperCloseRoomWithSocket:(SocketIOClient *)socket
                                 success:(void (^)(NSInteger code))success
                                 failure:(void (^)(NSString *error))failure;


/**
 发送聊天

 @param socket  socket
 @param roomID 主播ID
 @param content 聊天内容
 */
+ (void)SocketHealperSendChatWithSocket:(SocketIOClient *)socket
                                 roomID:(NSString *)roomID
                                content:(NSString *)content;






@end
