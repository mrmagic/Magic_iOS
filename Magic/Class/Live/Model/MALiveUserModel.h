//
//  围观用户
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//
#import "MRBaseModel.h"

@interface MALiveUserModel : MRBaseModel


@property(nonatomic,copy)NSString *iconName;
@property(nonatomic,copy)NSString *userID;
@property(nonatomic,copy)NSString *user_nicename;
@property(nonatomic,copy)NSString *level;
@property(nonatomic,copy)NSString *city;
@property(nonatomic,copy)NSString *signature;
@property(nonatomic,copy)NSString *sex;
@property(nonatomic,copy)NSString *avatar;
@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *fansnum;
@property(nonatomic,copy)NSString *attention;
@property(nonatomic,copy)NSString *introduce;
@property(nonatomic,copy)NSString *doAttention;
@property(nonatomic,copy)NSString *isMagic;

@end
