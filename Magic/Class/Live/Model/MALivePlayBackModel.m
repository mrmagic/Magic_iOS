//
//  MALivePlayBackModel.m
//  Magic
//
//  Created by 王 on 16/9/29.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALivePlayBackModel.h"

@implementation MALivePlayBackModel

- (instancetype)initWithJson:(NSDictionary *)json
{
    self = [super init];
    if (self) {
        _userID = [self checkForNull:[json valueForKey:@"id"]];
        _userName = [self checkForNull:[json valueForKey:@"user_nicename"]];
        _avatar = [self checkForNull:[json valueForKey:@"avatar"]];
        _starttime = [self checkForNull:[json valueForKey:@"starttime"]];
        _cover = [self checkForNull:[json valueForKey:@"thumb"]];
        _title = [self checkForNull:[json valueForKey:@"title"]];
        _videoId = [self checkForNull:[json valueForKey:@"uid"]];
        _userLogin = [self checkForNull:[json valueForKey:@"user_login"]];
        _viewnums = [self checkForNull:[json valueForKey:@"viewnums"]];
        _videoUrl = [self checkForNull:[json valueForKey:@"video_url"]];
        _collected = [[self checkForNull:[json valueForKey:@"collected"]] boolValue];
    }
    return self;
}

@end
