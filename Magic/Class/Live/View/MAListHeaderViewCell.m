//
//  MAListHeaderViewCell.m
//  Magic
//
//  Created by 王 on 16/9/26.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAListHeaderViewCell.h"

@interface MAListHeaderViewCell ()

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *nameLable;


@end

@implementation MAListHeaderViewCell

- (void)cellWithModel:(MALiveMagicModel *)model
{
    [self setupUI];
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.avatar]
                          placeholderImage:[UIImage imageNamed:@"image_placeHolder"]
                                 completed:nil];
    
    self.nameLable.text = [NSString stringWithFormat:@"%@", model.userName];
}

- (void)setupUI
{
    [self addSubview:self.backView];
    [self.backView addSubview:self.iconImageView];
    [self addSubview:self.nameLable];
}

#pragma mark -- 懒加载
- (UIView *)backView
{
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.frame = CGRectMake(0, 12.5, 52, 52);
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.clipsToBounds = YES;
        _backView.layer.cornerRadius = 26;
        _backView.layer.borderWidth = 0.5;
        _backView.layer.borderColor = MRTextColor.CGColor;
    }
    return _backView;
}

- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.frame = CGRectMake(3.5, 3.5, 45, 45);
        _iconImageView.clipsToBounds = YES;
        _iconImageView.contentMode = UIViewContentModeScaleAspectFill;
        _iconImageView.clipsToBounds = YES;
        _iconImageView.layer.cornerRadius = 22.5;
    }
    return _iconImageView;
}
- (UILabel *)nameLable
{
    if (!_nameLable) {
        _nameLable = [[UILabel alloc] init];
        _nameLable.font = [UIFont systemFontOfSize:13];
        _nameLable.textColor = MRTextColor;
        _nameLable.frame = CGRectMake(5, CGRectGetMaxY(self.backView.frame) + 5, 52, 13);
    }
    return _nameLable;
}



@end
