//
//  围观用户cell
//  Magic
//
//  Created by 王 on 16/9/21.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MALiveUserModel.h"

@interface MALiveUserCollectionCell : UICollectionViewCell


- (void)cellWithModel:(MALiveUserModel *)model;



@end
