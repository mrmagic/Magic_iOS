//
//  MALiveUserCell.h
//  Magic
//
//  Created by 王 on 16/9/30.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MALiveUserModel.h"

@interface MALiveUserCell : UICollectionViewCell

- (void)cellWithModel:(MALiveUserModel *)model;


@end
