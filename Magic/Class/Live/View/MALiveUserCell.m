//
//  MALiveUserCell.m
//  Magic
//
//  Created by 王 on 16/9/30.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALiveUserCell.h"

@interface MALiveUserCell ()

@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation MALiveUserCell

- (void)cellWithModel:(MALiveUserModel *)model
{
    [self addSubview:self.imageView];
}

- (UIImageView *)imageView
{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.frame = CGRectMake(0, 0, 30, 30);
        _imageView.clipsToBounds = YES;
        _imageView.layer.cornerRadius = 15;
        _imageView.backgroundColor = [UIColor redColor];
    }
    return _imageView;
}


@end
