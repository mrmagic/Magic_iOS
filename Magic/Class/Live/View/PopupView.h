

#import <UIKit/UIKit.h>
#import "MALiveUserModel.h"

@interface PopupView : UIView


@property(nonatomic,strong) MALiveUserModel *listmodel;

@property(nonatomic,strong)UIImageView *iconImage;

@property(nonatomic,strong)UILabel *nameL;

@property(nonatomic,strong)UILabel *fans;

@property(nonatomic,strong)UILabel *fansCount;

@property(nonatomic,strong)UIButton *attentionBTN;

@property(nonatomic,strong)UILabel *introduceL;

@property(nonatomic,strong)UITextView *introduceLS;

@property(nonatomic,strong)UIButton *DoAttentionBTN;

@end
