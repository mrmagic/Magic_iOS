//
//  预告cell
//  Magic
//
//  Created by 王 on 16/9/20.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MALiveLivingModel.h"
#import "MALiveAnnounceModel.h"
#import "MALivePlayBackModel.h"

@protocol MALiveCellDelegate <NSObject>

@optional
- (void)MATopicCellFollowButtonButtonClickWithModel:(MALivePlayBackModel *)model
                                            success:(void (^)(NSInteger code))success
                                            failure:(void (^)(NSError *error))failure;


@end


@interface MALiveCell : UITableViewCell

@property (nonatomic, assign) id<MALiveCellDelegate> delegate;


+(MALiveCell *)cellWithTableView:(UITableView *)tableView;

/**
 回放
 */
- (void)cellWithPlayBackModel:(MALivePlayBackModel *)playBackModel;

/**
 直播
 */
- (void)cellWithLiveModel:(MALiveLivingModel *)livemodel;


/**
 预告
 */
- (void)cellWithAnnounceModel:(MALiveAnnounceModel *)announcemodel;

@end
