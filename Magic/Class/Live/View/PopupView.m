

#import "PopupView.h"
#import "MRLivePopModel.h"
#import "UIImageView+WebCache.h"
#import <AFNetworking/AFNetworking.h>

#define selfHeight   _window_height*0.5

#define iconW kscreenHeight*0.5 /4.3

#define iconY 12

#define nameY kscreenHeight*0.5 * 0.3

#define fansX kscreenWidth*0.7 *0.25

#define fansY kscreenHeight*0.5 * 0.47

#define introduceY kscreenHeight*0.5*0.57

#define introduceYL kscreenHeight*0.5 * 0.65




@interface PopupView ()

@end



@implementation PopupView


-(instancetype)init{
    self = [super init];
    if (self) {
        
        [self setView];
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 5;
        
    }
    return self;
    
}
-(void)setListmodel:(MALiveUserModel *)listmodel
{
    _listmodel = listmodel;
    
    [_iconImage sd_setImageWithURL:[NSURL URLWithString:_listmodel.avatar] placeholderImage:[UIImage imageNamed:@"moren"]];
    
    _nameL.text = [NSString stringWithFormat:@"%@",self.listmodel.user_nicename];
    _fansCount.text = [NSString stringWithFormat:@"%@",self.listmodel.fansnum];
    _introduceLS.text =[NSString stringWithFormat:@"%@", self.listmodel.signature];
    
    
    if ([_listmodel.fansnum isEqual:[NSNull null]] || _listmodel.fansnum == nil ) {
        _fansCount.text = [NSString stringWithFormat:@"%@",@"0"];

    }
    if ([_listmodel.signature isEqual:[NSNull null]] || _listmodel.signature == nil ) {
        _introduceLS.text = [NSString stringWithFormat:@"%@",@"暂无简介"];
    }
}

-(void)setView{
    //添加头像
    _iconImage = [[UIImageView alloc]init];
    _iconImage.layer.masksToBounds = YES;
    _iconImage.layer.cornerRadius = iconW /2;
    //添加名称
    _nameL = [[UILabel alloc]init];
    _nameL.textAlignment = NSTextAlignmentCenter;
    _nameL.font = [UIFont systemFontOfSize:14];
    _nameL.textColor = MRTextColor;
    
    _fans = [[UILabel alloc]init];
    _fans.font = [UIFont systemFontOfSize:14];
    _fans.textColor = [UIColor colorWithRed:97/255.0 green:97/255.0 blue:97/255.0 alpha:1.0];
    _fans.text = @"粉丝:";
    
    _fansCount = [[UILabel alloc]init];
    _fansCount.font = [UIFont systemFontOfSize:14];
    _fansCount.textColor = [UIColor colorWithRed:97/255.0 green:97/255.0 blue:97/255.0 alpha:1.0];
    
    _attentionBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    [_attentionBTN setTitle:@"未关注" forState:UIControlStateNormal];
    [_attentionBTN setTitleColor:[UIColor colorWithRed:58/255.0 green:180/255.0 blue:193/255.0 alpha:1.0] forState:UIControlStateNormal];
    _attentionBTN.titleLabel.font = [UIFont systemFontOfSize:11];
    
    _introduceL = [[UILabel alloc]init];
    _introduceL.textAlignment = NSTextAlignmentCenter;
    _introduceL.font = [UIFont systemFontOfSize:14];
    _introduceL.textColor = MRTextColor;
    _introduceL.text = @"简介";
    
    _introduceLS = [[UITextView alloc]init];
    _introduceLS.font = [UIFont systemFontOfSize:11];
    _introduceLS.userInteractionEnabled = NO;
    _introduceLS.allowsEditingTextAttributes = NO;
    _introduceLS.textColor = MRTextColor;
    _DoAttentionBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    _DoAttentionBTN.titleLabel.font = [UIFont systemFontOfSize:15];
    [_DoAttentionBTN setTitle:@"关注" forState:UIControlStateNormal];
    [_DoAttentionBTN setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_DoAttentionBTN addTarget:self action:@selector(makeAtten) forControlEvents:UIControlEventTouchUpInside];
    [_DoAttentionBTN setBackgroundColor:[UIColor colorWithRed:39/255.0 green:41/255.0 blue:44/255.0 alpha:1.0]];
    
    
    
    _iconImage.frame = CGRectMake((kscreenWidth * 0.7)/2 - iconW/2,iconY,iconW,iconW);
    _iconImage.backgroundColor = [UIColor clearColor];
    
    _nameL.frame = CGRectMake(0,nameY, (kscreenWidth*0.7), 20);
    
    _fans.frame = CGRectMake(fansX, fansY,40, 40);
    
    _fansCount.frame = CGRectMake(fansX + _fans.frame.size.width,fansY,40,40);
    _fansCount.textAlignment = NSTextAlignmentLeft;
    _attentionBTN.frame = CGRectMake(_fansCount.frame.size.width + _fansCount.frame.origin.x, fansY + 10, 60, 20);
    _introduceL.frame =CGRectMake(0, introduceY, kscreenWidth*0.7, 30);
    
    
    CGFloat xx  = (kscreenWidth*0.7) * 0.1;
    CGFloat yy = kscreenHeight*0.6;
    CGFloat ww = (kscreenWidth*0.7)*0.8;
    CGFloat hh = kscreenHeight*0.5 - kscreenHeight*0.5 * 0.88 - introduceYL;
    
    _introduceLS.frame = CGRectMake(xx, yy,ww,hh);
    
    
    _DoAttentionBTN.frame = CGRectMake(0,kscreenHeight*0.5 * 0.87, kscreenWidth*0.7, kscreenHeight*0.5 * 0.13);

    
    _nameL.text = @"MR.WANG";
    _iconImage.image = [UIImage imageNamed:@"moren.jpg"];
    _fansCount.text = @"6785";
    _introduceLS.text = @"dajnjnvja";
    
    _introduceLS.backgroundColor = [UIColor clearColor];
    
    
    [self addSubview:_iconImage];
    [self addSubview:_nameL];
    [self addSubview:_fans];
    [self addSubview:_fansCount];
    [self addSubview:_attentionBTN];
    [self addSubview:_introduceL];
    [self addSubview:_introduceLS];
    [self addSubview:_DoAttentionBTN];
}

-(void)makeAtten
{
[[NSNotificationCenter defaultCenter]postNotificationName:@"magicGuanZhu" object:nil];
}

@end
