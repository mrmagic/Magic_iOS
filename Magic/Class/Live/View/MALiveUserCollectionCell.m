//
//  围观用户cell
//  Magic
//
//  Created by 王 on 16/9/21.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALiveUserCollectionCell.h"

@interface MALiveUserCollectionCell ()

@property (nonatomic, strong) UIImageView *iconImageView;

@end

@implementation MALiveUserCollectionCell


- (void)cellWithModel:(MALiveUserModel *)model
{
    [self setupUI];
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.avatar]
                          placeholderImage:[UIImage imageNamed:@"image_placeHolder"]
                                 completed:nil];
}

- (void)setupUI
{
    [self addSubview:self.iconImageView];
}

- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.frame = CGRectMake(0, 0, 40, 40);
        _iconImageView.layer.cornerRadius = 20;
        _iconImageView.clipsToBounds = YES;
        _iconImageView.contentMode = UIViewContentModeScaleAspectFill;
        _iconImageView.backgroundColor = [UIColor blueColor];
    }
    return _iconImageView;
}


@end
