//
//  MALiveHeaderView.h
//  Magic
//
//  Created by 王 on 16/9/26.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MALiveMagicModel.h"


@protocol MALiveHeaderViewDelegate <NSObject>

@optional

- (void)MALiveHeaderViewViewClickWithModel:(MALiveMagicModel *)model;

@end

@interface MALiveHeaderView : UIView

@property (nonatomic, assign)id<MALiveHeaderViewDelegate> delegate;

- (void)updateWithHeaderView:(NSArray *)magicArray;

@end
