//
//  预告cell
//  Magic
//
//  Created by 王 on 16/9/20.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALiveCell.h"

typedef NS_ENUM(NSInteger, MALiveCellType) {
    MALiveCellTypeAnnounce = 0,     // 预告
    MALiveCellTypeLiving,           // 直播中
    MALiveCellTypePlayback,         // 回放
};

@interface MALiveCell ()

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *nameLable;

@property (nonatomic, strong) UIView *timeView;
@property (nonatomic, strong) UIImageView *timeImageView;
@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UIView *remindView;
@property (nonatomic, strong) UIImageView *remindImageView;
@property (nonatomic, strong) UILabel *remindLabel;

@property (nonatomic, strong) UIImageView *backImageView;
@property (nonatomic, strong) UIImageView *palyImageView;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIButton *followButton;
@property (nonatomic, strong) UIButton *chatButton;
@property (nonatomic, strong) UIButton *shareButton;

@property (nonatomic, strong) UIView *centerLineView;
@property (nonatomic, strong) UIView *rightLineView;

@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, assign) MALiveCellType type;

@property (nonatomic, strong) MALivePlayBackModel *playBackModel;


@end

@implementation MALiveCell

+(MALiveCell *)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"MALiveCell";
    [tableView registerClass:[MALiveCell class] forCellReuseIdentifier:ID];
    MALiveCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = MRBackGroundCloor;
    
    return cell;
}




/**
 预告
 */
- (void)cellWithAnnounceModel:(MALiveAnnounceModel *)announcemodel
{
    self.type = MALiveCellTypeAnnounce;
    [self setupUI];

    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:announcemodel.avatar]
                          placeholderImage:[UIImage imageNamed:@"image_placeHolder"]
                                 completed:nil];
    
    self.nameLable.text = [NSString stringWithFormat:@"%@", announcemodel.userName];
    self.titleLabel.frame = CGRectMake(10, CGRectGetMaxY(self.backImageView.frame), kscreenWidth - 30, 50);
    
    [self.backImageView sd_setImageWithURL:[NSURL URLWithString:announcemodel.cover]
                          placeholderImage:[UIImage imageNamed:@"image_placeHolder"]
                                 completed:nil];
    
    self.titleLabel.text = [NSString stringWithFormat:@"%@", announcemodel.title];
 
    [self setTimeLabelWithLabel:self.timeLabel time:announcemodel.showtime];
    self.timeLabel.frame = CGRectMake(CGRectGetMaxX(self.timeImageView.frame) + 10, 2.5, [self.timeLabel widthForLabelWithHeight:15], 15);
    self.timeView.frame = CGRectMake(CGRectGetMaxX(self.iconImageView.frame) + 15, 45, 15, CGRectGetMaxX(self.timeLabel.frame));
}

/**
 直播
 */
- (void)cellWithLiveModel:(MALiveLivingModel *)livemodel
{
    self.type = MALiveCellTypeLiving;
    [self setupUI];
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:livemodel.avatar]
                          placeholderImage:[UIImage imageNamed:@"image_placeHolder"]
                                 completed:nil];
    
    self.nameLable.text = [NSString stringWithFormat:@"%@", livemodel.userName];
    
    [self.backImageView sd_setImageWithURL:[NSURL URLWithString:livemodel.cover]
                          placeholderImage:[UIImage imageNamed:@"image_placeHolder"]
                                 completed:nil];
    self.titleLabel.text = [NSString stringWithFormat:@"%@", livemodel.title];
    self.titleLabel.frame = CGRectMake(10, CGRectGetMaxY(self.backImageView.frame), kscreenWidth - 30, 50);

    
    //正在直播
    self.timeLabel.text = @"正在直播";
    self.timeLabel.frame = CGRectMake(0, 2.5, [self.timeLabel widthForLabelWithHeight:15], 15);
    self.timeView.frame = CGRectMake(CGRectGetMaxX(self.iconImageView.frame) + 15, 45, 15, CGRectGetMaxX(self.timeLabel.frame));
    
    //观看人数
    self.remindLabel.text = [NSString stringWithFormat:@"%@", livemodel.viewnums];
    self.remindLabel.frame = CGRectMake(CGRectGetMaxX(self.remindImageView.frame) + 10, 3.5, [_remindLabel widthForLabelWithHeight:13], 13);
    self.remindView.frame = CGRectMake(kscreenWidth - 30 - CGRectGetMaxX(_remindLabel.frame) - 12.5, 12.5, CGRectGetMaxX(_remindLabel.frame), 13);
    if (livemodel.collected) {
        UIImageView *imageView = [self.followButton viewWithTag:1000];
        imageView.image = [UIImage imageNamed:@"icon_live_collectClick"];
    } else {
        UIImageView *imageView = [self.followButton viewWithTag:1000];
        imageView.image = [UIImage imageNamed:@"icon_live_collect"];
    }
}

/**
 回放
 */
- (void)cellWithPlayBackModel:(MALivePlayBackModel *)playBackModel
{
    self.playBackModel = playBackModel;
    self.type = MALiveCellTypePlayback;
    [self setupUI];
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:playBackModel.avatar]
                          placeholderImage:[UIImage imageNamed:@"image_placeHolder"]
                                 completed:nil];
    
    self.nameLable.text = [NSString stringWithFormat:@"%@", playBackModel.userName];
    
    [self.backImageView sd_setImageWithURL:[NSURL URLWithString:playBackModel.cover]
                          placeholderImage:[UIImage imageNamed:@"image_placeHolder"]
                                 completed:nil];
    self.titleLabel.text = [NSString stringWithFormat:@"%@", playBackModel.title];
    self.titleLabel.frame = CGRectMake(10, CGRectGetMaxY(self.backImageView.frame), kscreenWidth - 30 - 50, 50);

    //结束时间
    NSUInteger dateValue = [playBackModel.starttime integerValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:dateValue];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"M月d日 HH:mm"];
    NSString *timeStr = [formatter stringFromDate:date];
    self.timeLabel.text = [NSString stringWithFormat:@"%@", timeStr];
    self.timeLabel.frame = CGRectMake(CGRectGetMaxX(self.timeImageView.frame) + 10, 2.5, [self.timeLabel widthForLabelWithHeight:15], 15);
    self.timeView.frame = CGRectMake(CGRectGetMaxX(self.iconImageView.frame) + 15, 45, 15, CGRectGetMaxX(self.timeLabel.frame));
    
    //观看人数
    self.remindLabel.text = [NSString stringWithFormat:@"%@", playBackModel.viewnums];
    self.remindLabel.frame = CGRectMake(CGRectGetMaxX(self.remindImageView.frame) + 10, 3.5, [_remindLabel widthForLabelWithHeight:13], 13);
    self.remindView.frame = CGRectMake(kscreenWidth - 30 - CGRectGetMaxX(_remindLabel.frame) - 12.5, 12.5, CGRectGetMaxX(_remindLabel.frame), 13);
    if (playBackModel.collected) {
        UIImageView *imageView = [self.followButton viewWithTag:1000];
        imageView.image = [UIImage imageNamed:@"icon_live_collectClick"];
    } else {
        UIImageView *imageView = [self.followButton viewWithTag:1000];
        imageView.image = [UIImage imageNamed:@"icon_live_collect"];
    }
}

- (void)setTimeLabelWithLabel:(UILabel *)label time:(NSString *)time
{
    NSTimeInterval now = [NSDate date].timeIntervalSince1970;
    NSInteger nowTime = now;
    NSInteger nowDay = nowTime / 86400;
    
    NSUInteger dateValue = [time integerValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:dateValue];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSInteger startDay = dateValue / 86400;
    
    if (startDay >= nowDay) { //当天
        if (startDay == nowDay) {
            [formatter setDateFormat:@"HH:mm"];
            NSString *timeStr = [formatter stringFromDate:date];
            label.text = [NSString stringWithFormat:@"今天 %@", timeStr];
        }else if (startDay == nowDay + 1) {
            [formatter setDateFormat:@"HH:mm"];
            NSString *timeStr = [formatter stringFromDate:date];
            label.text = [NSString stringWithFormat:@"明天 %@", timeStr];
        } else if (startDay == nowDay + 2) {
            [formatter setDateFormat:@"HH:mm"];
            NSString *timeStr = [formatter stringFromDate:date];
            label.text = [NSString stringWithFormat:@"后天 %@", timeStr];
        } else {
            [formatter setDateFormat:@"M月d日 HH:mm"];
            NSString *timeStr = [formatter stringFromDate:date];
            label.text = [NSString stringWithFormat:@"%@", timeStr];
        }
    } else { //特殊处理 有问题的只是为了显示
        [formatter setDateFormat:@"M月d日 HH:mm"];
        NSString *timeStr = [formatter stringFromDate:date];
        label.text = [NSString stringWithFormat:@"%@", timeStr];
    }
}



- (void)setupUI
{
    for (UIView *view in [self.backView subviews]) {
        if ([view subviews].count > 0) {
            if (view != self.followButton) {
                for (UIView *smallView in [view subviews]) {
                    [smallView removeFromSuperview];
                }
            }
        }
        [view removeFromSuperview];
    }

    if (self.type == MALiveCellTypePlayback) { //回放
        [self addSubview:self.backView];
        //头像名字
        [self.backView addSubview:self.iconImageView];
        [self.backView addSubview:self.nameLable];
        
        //背景图
        [self.backView addSubview:self.backImageView];

        //观看人数
        [self.backView addSubview:self.remindView];
        [self.remindView addSubview:self.remindImageView];
        self.remindImageView.image = [UIImage imageNamed:@"icon_live_visibleBlack"];
        [self.remindView addSubview:self.remindLabel];
        
        //回访时间
        [self.backView addSubview:self.timeView];
        [self.timeView addSubview:self.timeImageView];
        self.timeImageView.image = [UIImage imageNamed:@"icon_live_time"];
        [self.timeView addSubview:self.timeLabel];
        //关注
        [self.backView addSubview:self.followButton];
    } else if (self.type == MALiveCellTypeLiving) { //直播中
        [self addSubview:self.backView];
        //头像名字
        [self.backView addSubview:self.iconImageView];
        [self.backView addSubview:self.nameLable];
        //背景图
        [self.backView addSubview:self.backImageView];
        
        //观看人数
        [self.backView addSubview:self.remindView];
        [self.remindView addSubview:self.remindImageView];
        self.remindImageView.image = [UIImage imageNamed:@"icon_live_visibleBlack"];
        
        //直播中
        [self.remindView addSubview:self.remindLabel];
        
        //正在直播
        [self.backView addSubview:self.timeView];
        [self.timeView addSubview:self.timeLabel];
        
//        //关注按钮
//        [self.backView addSubview:self.followButton];
    } else { //预告
        [self addSubview:self.backView];
        //头像名字
        [self.backView addSubview:self.iconImageView];
        [self.backView addSubview:self.nameLable];
        //背景图
        [self.backView addSubview:self.backImageView];
        
        //开始时间
        [self.backView addSubview:self.timeView];
        [self.timeView addSubview:self.timeImageView];
        self.timeImageView.image = [UIImage imageNamed:@"icon_live_time"];
        [self.timeView addSubview:self.timeLabel];
    }
    
    if (self.type == MALiveCellTypeLiving) {
        [self.backImageView addSubview:self.palyImageView];
    } else {
        [self.palyImageView removeFromSuperview];
    }
    [self.backView addSubview:self.titleLabel];
    [self.backView addSubview:self.lineView];

//    [self.backView addSubview:self.chatButton];
//    [self.backView addSubview:self.shareButton];
    //    [self.backView addSubview:self.centerLineView];
    //    [self.backView addSubview:self.rightLineView];
    
}

- (void)updateFollowImage
{
    self.playBackModel.collected = !self.playBackModel.collected;
    if (self.playBackModel.collected) {
        UIImageView *imageView = [self.followButton viewWithTag:1000];
        imageView.image = [UIImage imageNamed:@"icon_live_collectClick"];
    } else {
        UIImageView *imageView = [self.followButton viewWithTag:1000];
        imageView.image = [UIImage imageNamed:@"icon_live_collect"];
    }
}

#pragma mark - 按钮点击方法
- (void)followButtonButtonClick:(UIButton *)sender
{
    __weak typeof(self) weakSelf = self;
    if ([self.delegate respondsToSelector:@selector(MATopicCellFollowButtonButtonClickWithModel:success:failure:)]) {
        [self.delegate MATopicCellFollowButtonButtonClickWithModel:self.playBackModel
                                                           success:^(NSInteger code) {
                                                               [weakSelf updateFollowImage];
                                                           } failure:^(NSError *error) {
                                                               
                                                           }];
    }
}
- (void)chatButtonClick:(UIButton *)sender
{

}
- (void)shareButtonButtonClick:(UIButton *)sender
{

}
#pragma mark - 懒加载
- (UIView *)backView
{
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.frame = CGRectMake(15, 0, kscreenWidth - 30, kscreenWidth * 9 / 16 + 130);
    }
    return _backView;
}

- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.frame = CGRectMake(15, 15, 45, 45);
        _iconImageView.layer.cornerRadius = 22.5;
        _iconImageView.clipsToBounds = YES;

    }
    return _iconImageView;
}
- (UILabel *)nameLable
{
    if (!_nameLable) {
        _nameLable = [[UILabel alloc] init];
        _nameLable.font = [UIFont systemFontOfSize:15];
        _nameLable.textColor = MRTextColor;
        _nameLable.frame = CGRectMake(CGRectGetMaxX(self.iconImageView.frame) + 15, 20, kscreenWidth - 30 - CGRectGetMaxX(self.iconImageView.frame) - 30 - 70, 15);
    }
    return _nameLable;
}

- (UIView *)timeView
{
    if (!_timeView) {
        _timeView = [[UIView alloc] init];
    }
    return _timeView;
}

- (UIImageView *)timeImageView
{
    if (!_timeImageView) {
        _timeImageView = [[UIImageView alloc] init];
        _timeImageView.frame = CGRectMake(0, 0, 20, 20);
    }
    return _timeImageView;
}

- (UILabel *)timeLabel
{
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.font = [UIFont systemFontOfSize:10];
        _timeLabel.textColor = MRMainColor;
    }
    return _timeLabel;
}

- (UIView *)remindView
{
    if (!_remindView) {
        _remindView = [[UIView alloc] init];
    }
    return _remindView;
}

- (UIImageView *)remindImageView
{
    if (!_remindImageView) {
        _remindImageView = [[UIImageView alloc] init];
        _remindImageView.frame = CGRectMake(0, 0, 20, 20);
    }
    return _remindImageView;
}

- (UILabel *)remindLabel
{
    if (!_remindLabel) {
        _remindLabel = [[UILabel alloc] init];
        _remindLabel.font = [UIFont systemFontOfSize:13];
    }
    return _remindLabel;
}


- (UIImageView *)backImageView
{
    if (!_backImageView) {
        _backImageView = [[UIImageView alloc] init];
        _backImageView.frame = CGRectMake(-15, 70, kscreenWidth, kscreenWidth * 9 / 16);
        _backImageView.contentMode = UIViewContentModeScaleToFill;
        _backImageView.clipsToBounds = YES;
    }
    return _backImageView;
}

- (UIImageView *)palyImageView
{
    if (!_palyImageView) {
        _palyImageView = [[UIImageView alloc] init];
        _palyImageView.frame = CGRectMake((kscreenWidth - 100 ) / 2 , (kscreenWidth * 9 / 16 - 100 ) / 2, 100, 100);
        _palyImageView.contentMode = UIViewContentModeScaleToFill;
        _palyImageView.clipsToBounds = YES;
        _palyImageView.image = [UIImage imageNamed:@"icon_live_time"];
    }
    return _palyImageView;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:15];
    }
    return _titleLabel;
}

- (UIButton *)followButton
{
    if (!_followButton) {
        _followButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _followButton.frame = CGRectMake(kscreenWidth - 30 - 50, CGRectGetMaxY(self.backImageView.frame), 50, 50);
        [_followButton addTarget:self action:@selector(followButtonButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *imageview = [[UIImageView alloc] init];
        imageview.tag = 1000;
        imageview.frame = CGRectMake(12.5, 12.5, 25, 25);
        [_followButton addSubview:imageview];
    }
    return _followButton;
}

- (UIButton *)chatButton
{
    if (!_chatButton) {
        _chatButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _chatButton.frame = CGRectMake(CGRectGetMaxX(self.followButton.frame), CGRectGetMinY(self.followButton.frame), 50, 37.5);
        _chatButton.backgroundColor = MRMainColor;
        [_chatButton addTarget:self action:@selector(chatButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _chatButton;
}

- (UIButton *)shareButton
{
    if (!_shareButton) {
        _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _shareButton.frame = CGRectMake(CGRectGetMaxX(self.chatButton.frame), CGRectGetMinY(self.chatButton.frame), 50, 37.5);
        _shareButton.backgroundColor = MRMainColor;
        [_shareButton addTarget:self action:@selector(shareButtonButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _shareButton;
}

- (UIView *)centerLineView
{
    if (!_centerLineView) {
        _centerLineView = [[UIView alloc] init];
        _centerLineView.frame = CGRectMake(CGRectGetMaxX(self.followButton.frame), CGRectGetMaxY(self.backImageView.frame) +2.5, 0.5, 32.5);
        _centerLineView.backgroundColor = MRLineColor;
    }
    return _centerLineView;
}

- (UIView *)rightLineView
{
    if (!_rightLineView) {
        _rightLineView = [[UIView alloc] init];
        _rightLineView.frame =  CGRectMake(CGRectGetMaxX(self.chatButton.frame), CGRectGetMaxY(self.backImageView.frame) +2.5, 0.5, 32.5);
        _rightLineView.backgroundColor = MRLineColor;
    }
    return _rightLineView;
}

- (UIView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame), kscreenWidth - 30, 10);
        _lineView.backgroundColor = MRBackGroundCloor;

    }
    return _lineView;
}

@end
