//
//  MAListHeaderViewCell.h
//  Magic
//
//  Created by 王 on 16/9/26.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MALiveMagicModel.h"

@interface MAListHeaderViewCell : UICollectionViewCell

- (void)cellWithModel:(MALiveMagicModel *)model;

@end
