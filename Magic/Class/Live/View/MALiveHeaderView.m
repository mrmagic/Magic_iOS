//
//  MALiveHeaderView.m
//  Magic
//
//  Created by 王 on 16/9/26.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALiveHeaderView.h"
#import "MASelectVideoCell.h"
#import "MALiveMagicModel.h"
#import "MAListHeaderViewCell.h"

@interface MALiveHeaderView ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *flowlayout;
@property (nonatomic, strong) NSMutableArray *magiclArray;
@property (nonatomic, strong) UIView *headerView;

@property (nonatomic, strong) UIImageView *leftImageView;
@property (nonatomic, strong) UIImageView *rightImageView;

@property (nonatomic, assign) BOOL isSetup;
@property (nonatomic, assign) BOOL isMore;



@end

@implementation MALiveHeaderView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = MRBackGroundCloor;
    }
    return self;
}

- (void)updateWithHeaderView:(NSArray *)magicArray
{
    if (!self.isSetup) {
        [self addSubview:self.headerView];
        [self addSubview:self.backView];
        [self.backView addSubview:self.collectionView];
        [self.backView addSubview:self.leftImageView];
        [self.backView addSubview:self.rightImageView];
        self.leftImageView.hidden = YES;
        self.rightImageView.hidden = YES;
        self.isSetup = YES;
    }
    self.magiclArray = [NSMutableArray arrayWithArray:magicArray];
    if (self.magiclArray.count * 92.5 + (self.magiclArray.count - 1) * 32.5 + 30 >= kscreenWidth - 30) {
        self.isMore = YES;
        self.rightImageView.hidden = NO;
    }
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDelagate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(MALiveHeaderViewViewClickWithModel:)]) {
        MALiveMagicModel *model = [self.magiclArray objectAtIndex:indexPath.item];
        [self.delegate MALiveHeaderViewViewClickWithModel:model];
    }
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.magiclArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MAListHeaderViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MAListHeaderViewCell" forIndexPath:indexPath];
    MALiveMagicModel *model = [self.magiclArray objectAtIndex:indexPath.item];
    [cell cellWithModel:model];
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offSet = scrollView.contentOffset.x;
    if (offSet > 0 && self.isMore) {
        self.rightImageView.hidden = YES;
        self.leftImageView.hidden = NO;
    } else {
        self.rightImageView.hidden = NO;
        self.leftImageView.hidden = YES;
    }
}

#pragma mark - 懒加载

- (UIView *)backView
{
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.frame = CGRectMake(15, 45, kscreenWidth - 30, 92.5);
        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}
- (UICollectionViewFlowLayout *)flowlayout
{
    if (!_flowlayout) {
        _flowlayout = [[UICollectionViewFlowLayout alloc]init];
        CGFloat width = 52;
        CGFloat height = 92.5;
        _flowlayout.itemSize = CGSizeMake(width,height);
        _flowlayout.minimumLineSpacing = 32.5;
        _flowlayout.minimumInteritemSpacing = 0;
        _flowlayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _flowlayout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 15);
    }
    return _flowlayout;
}
- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kscreenWidth - 30, 92.5) collectionViewLayout:self.flowlayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.backgroundColor = [UIColor whiteColor];

        [_collectionView registerClass:[MAListHeaderViewCell class] forCellWithReuseIdentifier:@"MAListHeaderViewCell"];
    }
    return _collectionView;
}

- (UIView *)headerView
{
    if (!_headerView) {
        _headerView = [[UIView alloc] init];
        _headerView.frame = CGRectMake(0, 0, kscreenWidth, 45);
        
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.frame = CGRectMake(27.5, 12.5, 20, 20);
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        imageView.image = [UIImage imageNamed:@"icon_live_starred"];
        
        [_headerView addSubview:imageView];
        
        UILabel *label = [[UILabel alloc] init];
        label.text = @"人气主播";
        label.font = [UIFont systemFontOfSize:15];
        label.textColor = MRTextColor;
        label.frame = CGRectMake(CGRectGetMaxX(imageView.frame), 0, [label widthForLabelWithHeight:45], 45);
        [_headerView addSubview:label];
    }
    return _headerView;
}

- (UIImageView *)leftImageView
{
    if (!_leftImageView) {
        _leftImageView = [[UIImageView alloc] init];
        _leftImageView.frame = CGRectMake(0, 0, 15, 92.5);
        _leftImageView.clipsToBounds = YES;
        _leftImageView.contentMode = UIViewContentModeScaleAspectFill;
        _leftImageView.image = [UIImage imageNamed:@"icon_live_visibleBlack"];
    }
    return _leftImageView;
}

- (UIImageView *)rightImageView
{
    if (!_rightImageView) {
        _rightImageView = [[UIImageView alloc] init];
        _rightImageView.frame = CGRectMake(kscreenWidth - 30 - 15, 0, 15, 92.5);
        _rightImageView.clipsToBounds = YES;
        _rightImageView.contentMode = UIViewContentModeScaleAspectFill;
        _rightImageView.image = [UIImage imageNamed:@"icon_live_visibleBlack"];

    }
    return _rightImageView;
}


-(NSMutableArray *)magiclArray
{
    if (!_magiclArray) {
        _magiclArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _magiclArray;
}


@end
