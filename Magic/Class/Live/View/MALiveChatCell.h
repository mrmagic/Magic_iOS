//
//  MALiveChatCell.h
//  Magic
//
//  Created by 王 on 16/9/30.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MALiveChatModel.h"

@interface MALiveChatCell : UITableViewCell

+(MALiveChatCell *)cellWithTableView:(UITableView *)tableView;

- (void)cellWithModel:(MALiveChatModel *)model;

@end
