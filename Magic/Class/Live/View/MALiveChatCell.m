//
//  MALiveChatCell.m
//  Magic
//
//  Created by 王 on 16/9/30.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALiveChatCell.h"

@interface MALiveChatCell ()

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *nameLable;
@property (nonatomic, strong) UILabel *contentLable;

@end

@implementation MALiveChatCell

+(MALiveChatCell *)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"MALiveChatCell";
    [tableView registerClass:[MALiveChatCell class] forCellReuseIdentifier:ID];
    MALiveChatCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)cellWithModel:(MALiveChatModel *)model
{
    for (UIView *view in [self.backView subviews]) {
        [view removeFromSuperview];
    }
    if (model.userName.length > 0) {
        [self addSubview:self.backView];
        [self.backView addSubview:self.iconImageView];
        [self.backView addSubview:self.nameLable];
        [self.backView addSubview:self.contentLable];
        
        self.backView.frame = CGRectMake(0, 5, model.viewWidth, model.viewHeight - 10);

        [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.iconUrl]
                              placeholderImage:[UIImage imageNamed:@""]
                                     completed:nil];
        self.iconImageView.frame = CGRectMake(0, 0, 30, model.viewHeight - 10);

        self.nameLable.text = model.userName;
        self.nameLable.frame = CGRectMake(35, 0, model.viewWidth - 30 - 30 - 10, 20);

        self.contentLable.text = model.content;
        self.contentLable.frame = CGRectMake(35, 20, model.viewWidth - 30 - 10, model.viewHeight - 20 - 10);
    } else {
        [self addSubview:self.backView];
        [self.backView addSubview:self.nameLable];
        self.backView.frame = CGRectMake(0, 5, model.viewWidth, model.viewHeight - 10);
        
        self.nameLable.text = model.content;
        self.nameLable.frame = CGRectMake(5, 0, model.viewWidth - 10, model.viewHeight - 10);
    }
}


- (UIView *)backView
{
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.layer.cornerRadius = 2;
        _backView.clipsToBounds = YES;
        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}

- (UIImageView *)iconImageView
{
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.contentMode = UIViewContentModeScaleAspectFill;
        _iconImageView.clipsToBounds = YES;
    }
    return _iconImageView;
}
- (UILabel *)nameLable
{
    if (!_nameLable) {
        _nameLable = [[UILabel alloc] init];
        _nameLable.font = [UIFont systemFontOfSize:15];
        _nameLable.textColor = MRMainColor;
        _nameLable.clipsToBounds = YES;
    }
    return _nameLable;
}

- (UILabel *)contentLable
{
    if (!_contentLable) {
        _contentLable = [[UILabel alloc] init];
        _contentLable.font = [UIFont systemFontOfSize:13];
        _contentLable.textColor = MRTextColor;
        _contentLable.clipsToBounds = YES;
        _contentLable.numberOfLines = 0;
    }
    return _contentLable;
}

@end
