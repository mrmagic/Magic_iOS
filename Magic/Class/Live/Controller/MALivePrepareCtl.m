//
//  MALivePrepareCtl.m
//  Magic
//
//  Created by 王 on 16/9/30.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALivePrepareCtl.h"
#import "MALiveShowCtl.h"

@interface MALivePrepareCtl ()<UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UIImageView *backImageView;

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIView * navigationView;

@property (nonatomic, strong) UIButton *cameraButton;
@property (nonatomic, strong) UITextField *titleTextFiled;
@property (nonatomic, strong) UIButton *nextButton;


@end

@implementation MALivePrepareCtl

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [self keyBoardDiss];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self addNotification];
}
- (void)addNotification
{
    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeRoomSuccess:)
                                                 name:kCloseRoomSuccessNotification
                                               object:nil];
    
}

- (void)keyboardWillShow:(NSNotification *)note
{
    //获取键盘的高度
    NSDictionary *userInfo = [note userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    int height = keyboardRect.size.height;
    if (kscreenHeight - CGRectGetMaxY(self.nextButton.frame) - 10 < height) {
        CGFloat offset = height - (kscreenHeight - CGRectGetMaxY(self.nextButton.frame) - 10);
        [UIView animateWithDuration:0.2 animations:^{
            self.backView.frame = CGRectMake(0, - offset, kscreenWidth, kscreenHeight);
        }];
    }
}

- (void)keyboardWillHide:(NSNotification *)note
{
    [UIView animateWithDuration:0.2 animations:^{
        self.backView.frame = CGRectMake(0, 0, kscreenWidth, kscreenHeight);
    }];
}

- (void)closeRoomSuccess:(NSNotification *)note
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)keyBoardDiss
{
    [self.titleTextFiled resignFirstResponder];
}

#pragma mark - UI
- (void)setupUI
{
    [self.view addSubview:self.backImageView];
    [self.view addSubview:self.backView];
    [self.backView addSubview:self.navigationView];
    [self.backView addSubview:self.cameraButton];
    [self.backView addSubview:self.titleTextFiled];
    [self.backView addSubview:self.nextButton];
}


/**
 关闭按钮
 */
- (void)rightBarButtonItemClick
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


/**
 开播按钮
 */
- (void)nextButtonClick
{
    MALiveShowCtl *vc = [[MALiveShowCtl alloc] init];
    vc.showType = MALiveShowTypeLiving;
    [self pushToViewControllerWithController:vc];
}


/**
 选择封面
 */
- (void)cameraButtonClick
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"相机"
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * _Nonnull action) {
                                                       //拍照
                                                       UIImagePickerController *imagePickerController = [UIImagePickerController new];
                                                       imagePickerController.allowsEditing = NO;
                                                       imagePickerController.delegate = self;
                                                       imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                       imagePickerController.showsCameraControls = YES;
                                                       imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
                                                       [self presentViewController:imagePickerController animated:YES completion:nil];
                                                       
                                                   }];
    
    
    UIAlertAction *photos = [UIAlertAction actionWithTitle:@"相册"
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * _Nonnull action) {
                               
                                                       //相册
                                                       UIImagePickerController *imagePickerController = [UIImagePickerController new];
                                                       imagePickerController.allowsEditing = NO;
                                                       imagePickerController.delegate = self;
                                                       imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                       [self presentViewController:imagePickerController animated:YES completion:nil];
                                                   }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消"
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * _Nonnull action) {
                                                       
                                                   }];
    [alert addAction:camera];
    [alert addAction:photos];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    if ([type isEqualToString:@"public.image"])
    {
        //先把图片转成NSData
        UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        image = [self scaleFromImage:image];
        
        NSData *fengmiandata;
        if (UIImagePNGRepresentation(image) == nil)
        {
            fengmiandata = UIImageJPEGRepresentation(image, 1.0);
        }
        else
        {
            fengmiandata = UIImagePNGRepresentation(image);
        }
        
        //获取时间 命名图片
        NSTimeInterval now = [NSDate date].timeIntervalSince1970;
        
        //图片保存的路径
        NSString * DocumentsPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        //把刚刚图片转换的data对象拷贝至沙盒中 并保存为image.png
        [fileManager createDirectoryAtPath:DocumentsPath withIntermediateDirectories:YES attributes:nil error:nil];
        [fileManager createFileAtPath:[DocumentsPath stringByAppendingString:[NSString stringWithFormat:@"/%f.png", now]] contents:fengmiandata attributes:nil];
        //得到选择后沙盒中图片的完整路径
        NSString *filePath = [[NSString alloc]initWithFormat:@"%@%@",DocumentsPath, [NSString stringWithFormat:@"/%f.png", now]];
        
        UIImage *imagess = [UIImage imageWithContentsOfFile:filePath];
        
        [self.cameraButton setImage:imagess forState:UIControlStateNormal];
        [self.cameraButton setTitle:@" " forState:UIControlStateNormal];
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
}

//图片压缩
- (UIImage *)scaleFromImage:(UIImage*)image
{
    CGSize imageSize = image.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    CGFloat scaledWidth = kscreenWidth * 2;
    CGFloat scaledHeight = scaledWidth * height / width;
    
    if (width <= scaledWidth && height <= scaledHeight)
    {
        return image;
    }
    if (width == 0 || height == 0){
        return image;
    }
    CGSize targetSize = CGSizeMake(scaledWidth,scaledHeight);
    UIGraphicsBeginImageContext(targetSize);
    [image drawInRect:CGRectMake(0,0,scaledWidth,scaledHeight)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - UITextFiledDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
        if(range.length + range.location > textField.text.length){
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 15;//最大字数长度
}

- (void)textFiledChange:(UITextField *)textFiled
{
    if (self.titleTextFiled.text.length > 0) {
        self.nextButton.backgroundColor = MRMainColor;
        self.nextButton.userInteractionEnabled = YES;
    } else {
        self.nextButton.backgroundColor = MRTextColor;
        self.nextButton.userInteractionEnabled = NO;
    }
}

#pragma mak - 懒加载
- (UIImageView *)backImageView
{
    if (!_backImageView) {
        _backImageView = [[UIImageView alloc] init];
        _backImageView.frame = CGRectMake(0, 0, kscreenWidth, kscreenHeight);
        _backImageView.clipsToBounds = YES;
        _backImageView.contentMode = UIViewContentModeTop;
//        _backImageView.image = [UIImage imageNamed:@"image_login_back"];
        _backImageView.backgroundColor = [UIColor grayColor];
    }
    return _backImageView;
}

- (UIView *)backView
{
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.frame = CGRectMake(0, 0, kscreenWidth, kscreenHeight);
        _backView.clipsToBounds = YES;
        _backView.backgroundColor = [UIColor colorWithHex:0xefefef alpha:0.1];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyBoardDiss)];
        [_backView addGestureRecognizer:tap];
    }
    return _backView;
}

- (UIView *)navigationView
{
    if (!_navigationView) {
        _navigationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kscreenWidth, 64)];
        _navigationView.backgroundColor = [UIColor clearColor];
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        rightButton.frame = CGRectMake(kscreenWidth - 45, 24, 40, 40);
        [rightButton setImage:[UIImage imageNamed:@"icon_login_close"] forState:UIControlStateNormal];
        [rightButton addTarget:self action:@selector(rightBarButtonItemClick) forControlEvents:UIControlEventTouchUpInside];
        [_navigationView addSubview:rightButton];
        rightButton.contentMode = UIViewContentModeScaleAspectFill;
        rightButton.tag = 1001;
    }
    return _navigationView;
}

- (UIButton *)cameraButton
{
    if (!_cameraButton) {
        _cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cameraButton.frame = CGRectMake(30, CGRectGetMaxY(self.navigationView.frame) + 40, kscreenWidth - 60, (kscreenWidth - 60) * 9 / 16);
        _cameraButton.contentMode = UIViewContentModeScaleAspectFill;
        _cameraButton.backgroundColor = [UIColor clearColor];
        _cameraButton.clipsToBounds = YES;
        _cameraButton.layer.borderWidth = 0.5;
        _cameraButton.layer.borderColor = [UIColor whiteColor].CGColor;
        _cameraButton.layer.cornerRadius = 4;
        [_cameraButton addTarget:self action:@selector(cameraButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cameraButton;
}

- (UITextField *)titleTextFiled
{
    if (!_titleTextFiled) {
        _titleTextFiled = [[UITextField alloc] init];
        _titleTextFiled.frame = CGRectMake(35, CGRectGetMaxY(self.cameraButton.frame) + 15, kscreenWidth - 70, 30);
        _titleTextFiled.borderStyle = UITextBorderStyleRoundedRect;
        _titleTextFiled.keyboardType = UIKeyboardTypeASCIICapable;
        _titleTextFiled.placeholder = @"给你的直播写个标题吧";
        _titleTextFiled.font = [UIFont systemFontOfSize:13];
        _titleTextFiled.delegate = self;
        //添加方法
        [_titleTextFiled addTarget:self action:@selector(textFiledChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _titleTextFiled;
}

- (UIButton *)nextButton
{
    if (!_nextButton) {
        _nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _nextButton.frame = CGRectMake(35, CGRectGetMaxY(self.titleTextFiled.frame) + 17.5, kscreenWidth - 70, 30);
        _nextButton.backgroundColor = MRTextColor;
        _nextButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_nextButton setTitle:@"开播" forState:UIControlStateNormal];
        [_nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _nextButton.userInteractionEnabled = NO;
        
        [_nextButton addTarget:self action:@selector(nextButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _nextButton;
}


@end
