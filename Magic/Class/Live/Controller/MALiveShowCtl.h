//
//  直播端
//  Magic
//
//  Created by 王 on 16/9/29.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MAViewController.h"

typedef NS_ENUM(NSInteger, MALiveShowType) {
    MALiveShowTypeLiving = 0,     // 直播端
    MALiveShowTypeWatch,          // 观看端
};

@interface MALiveShowCtl : MAViewController

@property (nonatomic, copy)   NSString *anchorID; //主播id
@property (nonatomic, assign) MALiveShowType showType; 

@end
