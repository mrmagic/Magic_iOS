
//
//  回放页面
//  iphoneLive
//
//  Created
//  Copyright © 2016年 cat. All rights reserved.
//

#import "MMMeBoughtCtl.h"
#import "MAMePayCtl.h"

//view
#import "MALiveUserCollectionCell.h"
#import "PopupView.h"

//model
#import "MALiveUserModel.h"

//其他
#import <MediaPlayer/MediaPlayer.h>


#define backcolor [UIColor colorWithRed:52/255.0 green:54/255.0 blue:58/255.0 alpha:1.0];
#define leftW 40  //直播间左上角头像
#define movirPlayerZhiBoLivi  [UIFont systemFontOfSize:10]
#define hanshuiYingpiao @"魔力值:"

@interface MMMeBoughtCtl () <UICollectionViewDataSource,UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) MPMoviePlayerController *player;

@property (nonatomic, strong) UIView *backView;

//主播信息
@property (nonatomic, strong) UIView *leftView;
@property (nonatomic, strong) UIImageView *IconImageView;
@property (nonatomic, strong) UILabel *liveLabel;
@property (nonatomic, strong) UIButton *magicAttention;
@property (nonatomic, strong) UILabel *onlineLabel;

//用户列表
@property (nonatomic, strong) UICollectionView *collectionview;
@property (nonatomic, strong) UICollectionViewFlowLayout *flowlayout;
@property (nonatomic, strong) NSMutableArray *listModelArray;//用户列表数组模型


//播放时间
@property (nonatomic, strong) UIView *timeView;
@property (nonatomic, strong) UILabel *timeNowLabel;
@property (nonatomic, strong) UISlider *timeSlider;
@property (nonatomic, strong) UILabel *timeTotleLabel;
@property (nonatomic, strong) NSTimer *timer;

//聊天
@property (nonatomic, strong) UITableView *tableView;

//按钮
@property (nonatomic, strong) UIButton *startBTN;
@property (nonatomic, strong) UIButton *cancleButton;

//弹窗
@property (nonatomic, strong) PopupView *popView;

@property (nonatomic, strong) UIButton *backBTN;


@property (nonatomic, copy) NSString *tanChuangID;
@property (nonatomic, assign) BOOL isConsumption;
@property (nonatomic, assign) BOOL isRecharge; //是否充值成功
@property (nonatomic, assign) BOOL ispopView;
@property (nonatomic, assign) BOOL isbackView;
@property (nonatomic, assign) BOOL playOK;

@property (nonatomic, copy) NSString *playUrl;


@end

@implementation MMMeBoughtCtl

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)dealloc
{
    [self removeTimer];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.ispopView = NO;
        self.isConsumption = YES;
        self.playUrl = @"ttp://v.cctv.com/flash/mp4video6/TMS/2011/01/05/cf752b1c12ce452b3040cab2f90bc265_h264818000nero_aac32-1.mp4";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];

}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    //禁止优化返回
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    [self setupUI];
    //更新用户列表
    [self getUserList];
    //更新观看人数
    [self updateCount];
}

#pragma mark - 试图构建
/**
 *  构建试图
 */
-(void)setupUI
{
    [self.view addSubview:self.player.view];
    [self.self.player.view addSubview:self.backView];
    [self.backView addSubview:self.leftView];
    [self.leftView addSubview:self.IconImageView];
    [self.leftView addSubview:self.liveLabel];
    [self.leftView addSubview:self.onlineLabel];
    [self.leftView addSubview:self.magicAttention];
    [self.backView addSubview:self.collectionview];
    
    [self.backView addSubview:self.timeView];
    [self.backView addSubview:self.startBTN];
    [self.backView addSubview:self.cancleButton];
}
#pragma mark - 网络请求
/**
 *  请求用户列表
 */
-(void)getUserList
{
    NSString *url = [purl stringByAppendingFormat:@"?service=Video.getReUserList"];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger ret) {
                                 if(ret == 200)
                                 {
                                     NSArray *data = [JSON valueForKey:@"data"];
                                     NSNumber *code = [data valueForKey:@"code"];
                                     
                                     if([code integerValue] == 0){
                                         NSArray *info = [data valueForKey:@"info"];
                                         if (self.listModelArray.count > 0) {
                                             self.listModelArray = nil;
                                         }
                                         for (NSDictionary *dic in info) {
                                             MALiveUserModel *model = [[MALiveUserModel alloc] initWithJson:dic];
                                             [self.listModelArray addObject:model];

                                         }
                                         [self.collectionview reloadData];
                                     }
                                 }
                             } failure:^(NSError *error) {
                                 
                             }];
}

/**
 更新观看人数
 */
-(void)updateCount
{
    NSString *url = [purl stringByAppendingFormat:@"?service=Video.updateLiveNums&uid=%@",self.userID];
    
    [[MRHttpClient sharedClient] get:url
                          parameters:nil success:^(NSDictionary *JSON, NSInteger ret) {
                              
                          } failure:^(NSError *error) {
                              
                          }];
}


/**
 判断关注

 @param ID 判断的id
 */
-(void)judgeWetherAttention:(NSString *)ID
{
    NSString *url = [purl stringByAppendingFormat:@"?service=User.isAttention&uid=%@&touid=%@&token=%@",[MRUser sharedClient].ID,ID,[MRUser sharedClient].token];
    //更新关注
    [self updateAttentionWithUrl:url];
}

//点击关注
-(void)payAttentionToTheHostOrAudience
{
    if ([MRUser sharedClient].ID == 0) {
        [self showError:@"此用户不是魔术师"];
    } else {
        NSString *url = [purl stringByAppendingFormat:@"?service=User.setAttention&uid=%@&showid=%@&token=%@",[MRUser sharedClient].ID,self.tanChuangID,[MRUser sharedClient].token];
        //更新关注
        [self updateAttentionWithUrl:url];
    }
}

#pragma mark -- UI更新
- (void)updateAttentionWithUrl:(NSString *)url
{
    [[MRHttpClient sharedClient] get:url
                          parameters:nil success:^(NSDictionary *JSON, NSInteger code) {
                              if(code == 0)
                              {
                                  NSNumber *info = [JSON valueForKey:@"info"];
                                  if ([info integerValue] == 0) {
                                      [self.popView.DoAttentionBTN setTitle:@"关注" forState:UIControlStateNormal];
                                      [self.popView.attentionBTN setTitle:@"未关注" forState:UIControlStateNormal];
                                      [self.magicAttention setTitle:@"关注" forState:UIControlStateNormal];
                                      
                                  }else{
                                      [self.popView.DoAttentionBTN setTitle:@"取消关注" forState:UIControlStateNormal];
                                      [self.popView.attentionBTN setTitle:@"已关注" forState:UIControlStateNormal];
                                      [self.magicAttention setTitle:@"取消关注" forState:UIControlStateNormal];
                                      
                                  }
                                  
                              }
                          } failure:^(NSError *error) {
                              
                          }];
}

#pragma mark - 播放器设置
- (void)addTimer
{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.01
                                                  target:self
                                                selector:@selector(exchangeSlider)
                                                userInfo:nil
                                                 repeats:YES];
    [self.timer fire];
}

- (void)removeTimer
{
    [self.timer invalidate];
    self.timer = nil;
}

- (void)beginPlay:(NSNotification *)note
{
    [self hideHUD];

    [self addTimer];
    NSInteger tot = self.player.duration;
    self.timeSlider.minimumValue = 0;
    self.timeSlider.maximumValue = tot;
    NSInteger hour = tot / 3600;
    NSInteger min = tot / 60 - hour * 60;
    NSInteger sec = tot - hour * 3600 - min * 60;
    self.timeTotleLabel.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", hour, min, sec];
}

- (void)exchangeSlider
{
    if (!self.isConsumption && self.player.currentPlaybackTime >= 15) {
        [self removeTimer];
        return;
    } else {
        NSInteger now = self.player.currentPlaybackTime;
        //避免出现抖动
        if (now >= 0) {
            NSInteger hour = now / 3600;
            NSInteger min = now / 60 - hour * 60;
            NSInteger sec = now - hour * 3600 - min * 60;
            self.timeNowLabel.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", hour, min, sec];
            self.timeSlider.value = self.player.currentPlaybackTime;
        }
    }
}

- (void)timeSliderChange
{
    [self removeTimer];
    [self.player pause];
}

- (void)timeSliderChangeEnd
{
    self.player.currentPlaybackTime = self.timeSlider.value;
    [self.player play];
    [self addTimer];
}

- (void)stateChange:(NSNotification *)note
{
    if (self.player.playbackState == MPMoviePlaybackStateStopped) {
        [self removeTimer];
    }
}

#pragma mark - 按钮点击方法

/**
 播放按钮点击
 */
-(void)startButtonClick
{
    if (self.playOK ==YES) {
        [self.startBTN setBackgroundImage:[UIImage imageNamed:@"icon_live_pause"] forState:UIControlStateNormal];
        [self.player play];
    }
    else{
        [self.startBTN setBackgroundImage:[UIImage imageNamed:@"live_btn_play"] forState:UIControlStateNormal];
        [self.player pause];
    }
    self.playOK = !self.playOK;
}

/**
 关闭按钮点击
 */
-(void)returnCancleClick
{
    [self removeTimer];
    [self.player stop];
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 主播信息点击
 
 @param tap 点击手势
 */
-(void)anchorMessage:(UITapGestureRecognizer *)tap
{
//    UIView *view = tap.view;
//    if (view == self.backView) {
//        [UIView animateWithDuration:0.3 animations:^{
//            self.popView.frame = CGRectMake(kscreenWidth*0.15, kscreenHeight*5, kscreenWidth*0.7, kscreenHeight*0.5);
//        }];
//    } else {
//        self.ispopView = !self.ispopView;
//        if (self.ispopView) {
//            //转义模型
//            MALiveUserModel *model = [[MALiveUserModel alloc] init];
//            model.userID = self.liveModel.ID;
//            model.avatar = self.liveModel.avatar;
//            model.user_nicename = self.liveModel.user_nicename;
//            model.fansnum = self.liveModel.fansnum;
//            model.signature = self.liveModel.signature;
//
//            self.popView.listmodel = model;
//            self.tanChuangID = self.liveModel.uid;
//            [self judgeWetherAttention:self.tanChuangID];
//            
//            [UIView animateWithDuration:0.3 animations:^{
//                self.popView.frame = CGRectMake(kscreenWidth * 0.15,kscreenHeight*0.2,kscreenWidth*0.7, kscreenHeight * 0.5);
//            }];
//        } else {
//            [UIView animateWithDuration:0.3 animations:^{
//                self.popView.frame = CGRectMake(kscreenWidth*0.15, kscreenHeight*5, kscreenWidth*0.7, kscreenHeight*0.5);
//            }];
//        }
//    }
}

#pragma mark - UICollectionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.listModelArray.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MALiveUserCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MALiveUserCollectionCell" forIndexPath:indexPath];
    MALiveUserModel *model = self.listModelArray[indexPath.item];
    [cell cellWithModel:model];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MALiveUserModel *model = [self.listModelArray objectAtIndex:indexPath.item];
    self.popView.listmodel = model;
    //判断关注
    self.tanChuangID = nil;
    self.tanChuangID = [NSString string];
    self.tanChuangID = model.userID;
    [self judgeWetherAttention:self.tanChuangID];
    
    if ([model.isMagic integerValue]) {
        [UIView animateWithDuration:0.2 animations:^{
            self.popView.frame = CGRectMake(kscreenWidth * 0.15,kscreenHeight*0.2,kscreenWidth*0.7, kscreenHeight * 0.5);
        }];
    }
}

//每个cell的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(40,40);
}
//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0,5,0,5);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

#pragma mark - 懒加载

- (MPMoviePlayerController *)player
{
    if (!_player) {
        _player = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:self.playUrl]];
        _player.controlStyle = MPMovieControlStyleNone;
        _player.scalingMode = MPMovieScalingModeFill;
        [_player prepareToPlay];
        _player.shouldAutoplay = YES;
        [_player.view setFrame:self.view.bounds];  // player的尺寸
        [self.view addSubview:_player.view];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beginPlay:) name: MPMovieDurationAvailableNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stateChange:) name: MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
    }
    return _player;
}

- (UIView *)backView
{
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.frame = CGRectMake(0, 0, kscreenWidth, kscreenHeight);
        _backView.backgroundColor = [UIColor clearColor];
    }
    return _backView;
}
- (UIView *)leftView
{
    if (!_leftView) {
        _leftView = [[UIView alloc]init];
        _leftView.frame = CGRectMake(15, 15, 150, 40);
        _leftView.userInteractionEnabled = YES;
        _leftView.layer.masksToBounds = YES;
        _leftView.backgroundColor = [UIColor colorWithHex:0x434343 alpha:0.4];
        _leftView.layer.cornerRadius = self.leftView.frame.size.height / 2;
    }
    return _leftView;
}

- (UIImageView *)IconImageView
{
    if (!_IconImageView) {
        _IconImageView = [[UIImageView alloc] init];
        _IconImageView.frame = CGRectMake(5,5, 30, 30);
        _IconImageView.layer.masksToBounds = YES;
        _IconImageView.layer.cornerRadius = 15;
        _IconImageView.image = [UIImage imageNamed:@"image_placeHolder"];
    }
    return _IconImageView;
}
- (UILabel *)liveLabel
{
    if (!_liveLabel) {
        _liveLabel =  [[UILabel alloc]init];
        _liveLabel.text = @"直播Live";
        _liveLabel.textAlignment = NSTextAlignmentCenter;
        _liveLabel.font = [UIFont systemFontOfSize:11];
        _liveLabel.frame = CGRectMake(40,0,[self.liveLabel widthForLabelWithHeight:11] + 10,20);
        self.liveLabel.textColor = [UIColor whiteColor];
    }
    return _liveLabel;
}

- (UILabel *)onlineLabel
{
    if (!_onlineLabel) {
        _onlineLabel = [[UILabel alloc] init];
        _onlineLabel.frame = CGRectMake(40,17,[self.liveLabel widthForLabelWithHeight:20] + 10,20);
        _onlineLabel.textAlignment = NSTextAlignmentCenter;
        _onlineLabel.textColor = [UIColor whiteColor];
        _onlineLabel.font = [UIFont systemFontOfSize:12];
        NSString *str = @"100000";
        if ([str integerValue] > 10000) {
            str = [NSString stringWithFormat:@"%.1f万", [str floatValue] / 10000];
        }
        self.onlineLabel.text = [NSString stringWithFormat:@"%@", str];
    }
    return _onlineLabel;
}

- (UIButton *)magicAttention
{
    if (!_magicAttention) {
        _magicAttention = [UIButton buttonWithType:UIButtonTypeCustom];
        _magicAttention.backgroundColor = [UIColor whiteColor];
        _magicAttention.titleLabel.font = [UIFont systemFontOfSize:10];
        [_magicAttention setTitleColor:MRMainColor forState:UIControlStateNormal];
        _magicAttention.frame  = CGRectMake(CGRectGetMaxX(self.liveLabel.frame) + 5,10,45,20);
        _magicAttention.clipsToBounds = YES;
        _magicAttention.layer.cornerRadius = 20 / 2;
    }
    return _magicAttention;
}

- (UICollectionViewFlowLayout *)flowlayout
{
    if (!_flowlayout) {
        _flowlayout = [[UICollectionViewFlowLayout alloc]init];
        _flowlayout.itemSize = CGSizeMake(30,30);
        _flowlayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    return _flowlayout;
}

- (UICollectionView *)collectionview
{
    if (!_collectionview) {
        _collectionview = [[UICollectionView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.leftView.frame) + 5, 15, kscreenWidth - CGRectGetMaxX(self.leftView.frame) - 5 - 15, 40)
                                            collectionViewLayout:self.flowlayout];
        _collectionview.bounces = NO;
        _collectionview.showsHorizontalScrollIndicator = NO;
        _collectionview.delegate = self;
        _collectionview.dataSource = self;
        _collectionview.backgroundColor = [UIColor clearColor];
        [_collectionview registerClass:[MALiveUserCollectionCell class] forCellWithReuseIdentifier:@"MALiveUserCollectionCell"];
    }
    return _collectionview;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(15,kscreenHeight - 200 - 60, kscreenWidth - 30, 200)
                                                 style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor clearColor];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}

- (UIView *)timeView
{
    if (!_timeView) {
        _timeView = [[UIView alloc] init];
        _timeView.frame = CGRectMake(60, kscreenHeight - 55, kscreenWidth - 120, 55);
        _timeView.backgroundColor = [UIColor clearColor];
        [self.backView addSubview:_timeView];
        
        //进度条
        self.timeSlider = [[UISlider alloc] init];
        self.timeSlider.frame = CGRectMake(0, 0, kscreenWidth - 120, 40);
        [self.timeSlider setMinimumTrackImage:[UIImage imageNamed:@"image_evaluate_videoLine"] forState:UIControlStateNormal];
        [self.timeSlider setMaximumTrackImage:[UIImage imageNamed:@"image_evaluate_videoLine"] forState:UIControlStateNormal];
        [self.timeSlider setThumbImage:[UIImage imageNamed:@"icon_evaluate_videoTime"] forState:UIControlStateNormal];
        [self.timeView addSubview:self.timeSlider];
        [self.timeSlider addTarget:self action:@selector(timeSliderChange) forControlEvents:UIControlEventTouchDown];
        [self.timeSlider addTarget:self action:@selector(timeSliderChangeEnd) forControlEvents:UIControlEventTouchUpInside];
        [self.timeView addSubview:self.timeSlider];
        
        //播放时间
        self.timeNowLabel = [[UILabel alloc] init];
        self.timeNowLabel.frame = CGRectMake(0, 30, 80, 12);
        self.timeNowLabel.font = [UIFont systemFontOfSize:12];
        self.timeNowLabel.textColor = [UIColor whiteColor];
        self.timeNowLabel.text = @"00:00:00";
        self.timeNowLabel.textAlignment = NSTextAlignmentLeft;
        [self.timeView addSubview:self.timeNowLabel];
        
        //总时间
        self.timeTotleLabel = [[UILabel alloc] init];
        self.timeTotleLabel.frame = CGRectMake(kscreenWidth - 120 - 80, 30, 80, 12);
        self.timeTotleLabel.font = [UIFont systemFontOfSize:12];
        self.timeTotleLabel.textColor = [UIColor whiteColor];
        self.timeTotleLabel.text = @"00:00:00";
        self.timeTotleLabel.textAlignment = NSTextAlignmentRight;
        [self.timeView addSubview:self.timeTotleLabel];
    }
    return _timeView;
}

- (UIButton *)startBTN
{
    if (!_startBTN) {
        _startBTN = [UIButton buttonWithType:UIButtonTypeCustom];
        _startBTN.enabled = YES;
        _startBTN.tintColor = [UIColor whiteColor];
        [_startBTN setBackgroundImage:[UIImage imageNamed:@"icon_live_pause"] forState:UIControlStateNormal];
        [_startBTN addTarget:self action:@selector(startButtonClick) forControlEvents:UIControlEventTouchUpInside];
        _startBTN.frame = CGRectMake(15,kscreenHeight - 40 - 10, 40, 40);
    }
    return _startBTN;
}


- (UIButton *)cancleButton
{
    if (!_cancleButton) {
        _cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancleButton.tintColor = [UIColor whiteColor];
        [_cancleButton setBackgroundImage:[UIImage imageNamed:@"icon_live_close"] forState:UIControlStateNormal];
        _cancleButton.backgroundColor = [UIColor clearColor];
        [_cancleButton addTarget:self action:@selector(returnCancleClick) forControlEvents:UIControlEventTouchUpInside];
        _cancleButton.frame = CGRectMake(kscreenWidth - 40 -15,kscreenHeight - 40 - 10,40,40);
    }
    return _cancleButton;
}

- (NSMutableArray *)listModelArray
{
    if (!_listModelArray) {
        _listModelArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _listModelArray;
}


@end
