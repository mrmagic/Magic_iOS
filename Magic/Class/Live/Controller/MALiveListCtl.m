//
//  直播列表页
//  Magic
//
//  Created by 王 on 16/9/19.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALiveListCtl.h"
#import "MASelectView.h"
#import "MALiveCell.h"
#import "MMMeBoughtCtl.h"
#import "MALiveHeaderView.h"
#import "MALiveMagicModel.h"
#import "MALiveShowCtl.h"
#import "MALoginCtl.h"
#import "MALivePrepareCtl.h"
#import "MAMeGuestCtl.h"

#import "MALiveAnnounceModel.h"
#import "MALiveLivingModel.h"
#import "MALivePlayBackModel.h"

typedef NS_ENUM(NSInteger, MALiveDataSourceType) {
    MALiveDataSourceTypeLiving = 0,     // 直播
    MALiveDataSourceTypePlayback,       // 回放
};

#define tableViewHeight kscreenHeight - 64 - 49

@interface MALiveListCtl () <UIScrollViewDelegate, MASelectViewDelegate, UITableViewDelegate, UITableViewDataSource, MALiveHeaderViewDelegate, MALiveCellDelegate>

@property (nonatomic, strong) MASelectView * selectView;
@property (nonatomic, strong) UIScrollView *scrollView;

//头部
@property (nonatomic, strong) MALiveHeaderView *livingHeaderView;
@property (nonatomic, strong) MALiveHeaderView *playBackHeaderView;

//tableView
@property (nonatomic, strong) UITableView *livingTableView;
@property (nonatomic, strong) UITableView *playBackTableView;

//列表数组
@property (nonatomic, strong) NSMutableArray *announceArray;  //预告
@property (nonatomic, strong) NSMutableArray *livingArray;    //直播
@property (nonatomic, strong) NSMutableArray *playbackArray;  //回放


//人气最佳
@property (nonatomic, strong) NSMutableArray *livingUserArray;
@property (nonatomic, strong) NSMutableArray *playBackUserArray;

//页码
@property (nonatomic, assign) NSInteger livingPage;
@property (nonatomic, assign) NSInteger playBackPage;

@property (nonatomic, strong) UIButton *playLivingButton;

//数据类型
@property (nonatomic, assign) MALiveDataSourceType dataSourceType;


@end

@implementation MALiveListCtl

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.livingPage = 0;
        self.playBackPage = 0;
        self.dataSourceType = MALiveDataSourceTypeLiving;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    
    [self.livingTableView.mj_header beginRefreshing];
}

#pragma mark  -构建UI
- (void)setupUI
{
    self.navigationItem.titleView = self.selectView;
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.livingTableView];
    [self.scrollView addSubview:self.playBackTableView];
    [self.view addSubview:self.playLivingButton];
}

/**
 区头部图构建
 */
- (UIView *)viewForHeaderFoeSectionWithImage:(UIImage *)image title:(NSString *)title
{
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(0, 0, kscreenWidth, 45);
    view.backgroundColor = MRBackGroundCloor;
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake(27.5, 12.5, 20, 20);
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    imageView.image = image;
    
    [view addSubview:imageView];
    
    UILabel *label = [[UILabel alloc] init];
    label.text = title;
    label.font = [UIFont systemFontOfSize:15];
    label.textColor = MRTextColor;
    label.frame = CGRectMake(CGRectGetMaxX(imageView.frame), 0, [label widthForLabelWithHeight:45], 45);
    [view addSubview:label];
    
    return view;
}
#pragma mark -数据加载
/**
 刷新
 */
- (void)relodaData
{
    if (self.dataSourceType == MALiveDataSourceTypeLiving) {
        self.livingPage = 0;
        [self dataSourceWithLivingWithRecommedMost];
        [self dataSourceWithAnnounce];
        [self dataSourceWithLiving];
    } else {
        [self dataSourceWithLivingWithPlayBack];
        [self dataSourceWithPlayBack];
    }
}

/**
 加载更多
 */
- (void)loadMore
{
    
}

/**
  直播 人气最佳
 */
-(void)dataSourceWithLivingWithRecommedMost
{
    NSString *url = [NSURL stringURLWithSever:@"User.mostPopular"
                                   parameters:nil];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 if (code == 0) {
                                     NSArray *array = [JSON objectForKey:@"masters"];
                                     for (NSDictionary *dic in array) {
                                         MALiveMagicModel *model = [[MALiveMagicModel alloc] initWithJson:dic];
                                         [self.livingUserArray addObject:model];
                                     }
                                     [self.livingHeaderView updateWithHeaderView:self.livingUserArray];
                                     self.livingTableView.tableHeaderView = self.livingHeaderView;
                                     [self.livingTableView reloadData];
                                 }
                             } failure:^(NSError *error) {
                                 //不提示错误
                             }];
}



/**
 获取预告列表
 */
-(void)dataSourceWithAnnounce
{
    NSString *url = [NSURL stringURLWithSever:@"Foreshow.GetForeshows"
                                   parameters:nil];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 if (code == 0) {
                                     NSArray *foreshows = [JSON valueForKey:@"foreshows"];
                                     if (self.announceArray.count > 0) {
                                         self.announceArray = nil;
                                     }
                                     for (NSDictionary *dic in foreshows) {
                                         MALiveAnnounceModel *model = [[MALiveAnnounceModel alloc] initWithJson:dic];
                                         [self.announceArray addObject:model];
                                     }
                                     [self.livingTableView reloadData];
                                 }
                             } failure:^(NSError *error) {
                             //不进行二次展示
                             }];
}


/**
 获取直播列表
 */
-(void)dataSourceWithLiving
{
    NSDictionary *parameters = @{@"action": @"0"};
    NSString *url = [NSURL stringURLWithSever:@"Liverecord.getLiveList"
                                   parameters:parameters];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 if (code == 0) {
                                     NSArray *lives = [JSON valueForKey:@"lives"];
                                     if (self.livingPage == 0) {
                                         self.livingArray = nil;
                                     }
                                     for (NSDictionary *dic in lives) {
                                         MALiveLivingModel *model = [[MALiveLivingModel alloc] initWithJson:dic];
                                         [self.livingArray addObject:model];
                                     }
                                     [self.livingTableView reloadData];
                                 }
                                 [self.livingTableView.mj_header endRefreshing];
                                 self.livingPage++;
                             } failure:^(NSError *error) {
                                 [self.livingTableView.mj_header endRefreshing];
                                 [self showError:MRNetWorkError];
                             }];
}

/**
 回放 人气最佳
 */
-(void)dataSourceWithLivingWithPlayBack
{
    NSString *url = [NSURL stringURLWithSever:@"User.mostPopular"
                                   parameters:nil];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 [self.playBackTableView.mj_header endRefreshing];
                                 if (code == 0) {
                                     NSArray *array = [JSON objectForKey:@"masters"];
                                     for (NSDictionary *dic in array) {
                                         MALiveMagicModel *model = [[MALiveMagicModel alloc] initWithJson:dic];
                                         [self.playBackUserArray addObject:model];
                                     }
                                     [self.playBackHeaderView updateWithHeaderView:self.playBackUserArray];
                                     self.playBackTableView.tableHeaderView = self.playBackHeaderView;
                                     
                                     [self.playBackTableView reloadData];
                                 }
                             } failure:^(NSError *error) {
                                 //不提示错误
                             }];
}

/**
 获取回放列表
 */
-(void)dataSourceWithPlayBack
{
    //忽略鉴权
    NSDictionary *parameters = @{@"action": @"0"};
    NSString *url = [NSURL stringURLWithSever:@"Liverecord.GetPlaybackList"
                                   parameters:parameters];
    
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 if (code == 0) {
                                     NSArray *info = [JSON valueForKey:@"playbacks"];
                                     if (self.playBackPage == 0) {
                                         self.playbackArray = nil;
                                     }
                                     for (NSDictionary *dic in info) {
                                         MALivePlayBackModel *model = [[MALivePlayBackModel alloc] initWithJson:dic];
                                         [self.playbackArray addObject:model];
                                     }
                                     [self.playBackTableView reloadData];
                                 }
                                 self.playBackPage++;
                                 [self.playBackTableView.mj_header endRefreshing];
                             } failure:^(NSError *error) {
                                 [self.playBackTableView.mj_header endRefreshing];
                                 [self showError:MRNetWorkError];
                             }];
}

- (void)MASelectViewIndexChangeWithIndex:(NSInteger)index
{
    [UIView animateWithDuration:0.2 animations:^{
        self.scrollView.contentOffset = CGPointMake(kscreenWidth * index, 0);
    }];
    self.dataSourceType = index;
    if (index == 1 && self.playbackArray.count == 0 && self.playBackPage == 0) {
        [self.playBackTableView.mj_header beginRefreshing];
        [self relodaData];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.scrollView) {
        NSInteger index = (scrollView.contentOffset.x  + kscreenWidth / 2) / kscreenWidth;
        [self.selectView updateToSelectIndex:index];
        self.dataSourceType = index;
        if (index == 1 && self.playbackArray.count == 0 && self.playBackPage == 0) {
            [self.playBackTableView.mj_header beginRefreshing];
            [self relodaData];
        }
    }
    [UIView animateWithDuration:0.5 animations:^{
        self.playLivingButton.frame = CGRectMake((kscreenWidth - 30) / 2, kscreenHeight - 64 - 49 - 30 - 10, 30, 30);
    }];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [UIView animateWithDuration:0.5 animations:^{
        self.playLivingButton.frame = CGRectMake((kscreenWidth - 30) / 2, kscreenHeight - 64 - 49, 30, 30);
    }];
}

#pragma mark - TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.livingTableView) {
        return 2;
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.livingTableView) {
        if (section == 0) {
            return self.livingArray.count;
        } else {
            return self.announceArray.count;
        }
    } else {
        return self.playbackArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.livingTableView) {
        MALiveCell *cell = [MALiveCell cellWithTableView:tableView];
        if (indexPath.section == 0) {
            MALiveLivingModel *model = [self.livingArray objectAtIndex:indexPath.row];
            [cell cellWithLiveModel:model];
        } else {
            MALiveAnnounceModel *model = [self.announceArray objectAtIndex:indexPath.row];
            [cell cellWithAnnounceModel:model];
        }
        return cell;
    } else {
        MALiveCell *cell = [MALiveCell cellWithTableView:tableView];
        MALivePlayBackModel *model = [self.playbackArray objectAtIndex:indexPath.row];
        [cell cellWithPlayBackModel:model];
        cell.delegate = self;
        return cell;
    }
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == self.livingTableView) {
        if (section == 0) {
            if (self.livingArray.count > 0) {
                UIView *view =  [self viewForHeaderFoeSectionWithImage:[UIImage imageNamed:@"icon_live_live"] title:@"正在直播"];
                return view;
            }
        } else {
            if (self.announceArray.count > 0) {
                UIView *view =  [self viewForHeaderFoeSectionWithImage:[UIImage imageNamed:@"icon_live_purchase"] title:@"精彩预告"];
                return view;
            }
        }
    } else {
        if (self.playbackArray.count > 0) {
            UIView *view =  [self viewForHeaderFoeSectionWithImage:[UIImage imageNamed:@"icon_live_repeat"] title:@"精彩回放"];
            return view;
        }
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == self.livingTableView) {
        if (section == 0) {
            if (self.livingArray.count > 0) {
                return 45;
            }
        } else {
            if (self.announceArray.count > 0) {
                return 45;
            }
        }
    } else {
        if (self.playbackArray.count > 0) {
            return 45;
        }
    }
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kscreenWidth * 9 / 16 + 130;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.livingTableView) {
        if (indexPath.section == 0) {
            MALiveLivingModel *model = [self.announceArray objectAtIndex:indexPath.row];
            
            MALiveShowCtl *vc = [[MALiveShowCtl alloc] init];
            vc.showType = MALiveShowTypeWatch;
            vc.anchorID = model.userID;
            [self pushToViewControllerWithController:vc];
        } else {
            MALiveAnnounceModel *model = [self.livingArray objectAtIndex:indexPath.row];
            [self pushToViewControllerWithUrl:@"asd" title:model.title];
        }
    } else {
        MALiveLivingModel *model = [self.playbackArray objectAtIndex:indexPath.row];
        MMMeBoughtCtl *vc = [[MMMeBoughtCtl alloc] init];
//        vc.liveModel = model;
        [self pushToViewControllerWithController:vc];
    }
}


/**
 开播按钮点击
 */
- (void)playLivingButtonClick
{
    MALivePrepareCtl *vc = [[MALivePrepareCtl alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - MALiveHeaderViewDelegate

/**
 人气最佳点击
 */
- (void)MALiveHeaderViewViewClickWithModel:(MALiveMagicModel *)model
{
    
    MAMeGuestCtl *vc = [[MAMeGuestCtl alloc] init];
    vc.userId = model.ID;
    [self pushToViewControllerWithController:vc];
}

- (void)MATopicCellFollowButtonButtonClickWithModel:(MALivePlayBackModel *)model
                                            success:(void (^)(NSInteger code))success
                                            failure:(void (^)(NSError *error))failure
{
    if ([MRUser sharedClient].ID == 0) {
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:[[MALoginCtl alloc] init]];
        [self presentViewController:nav
                           animated:YES
                         completion:nil];
    } else {
        //忽略鉴权
        NSDictionary *parameters = @{@"cid": model.videoId,
                                     @"ctype": @"1",
                                     @"action": [NSNumber numberWithBool:!model.collected],
                                     };
        NSString *url = [NSURL stringURLWithSever:@"Liverecord.GetPlaybackList"
                                       parameters:parameters];
        [[MRHttpClient sharedClient] get:url
                              parameters:nil
                                 success:^(NSDictionary *JSON, NSInteger code) {
                                     success(!model.collected);
                                 } failure:^(NSError *error) {
                                     failure(nil);
                                 }];
    }
}

#pragma mark - 懒加载
- (MASelectView *)selectView
{
    if (!_selectView) {
        _selectView = [[MASelectView alloc] init];
        _selectView.frame = CGRectMake(0, 0, 150, 40);
        _selectView.titleArray = @[@"直播", @"回放"];
        _selectView.space = 0;
        _selectView.delegate = self;
        [_selectView update];
    }
    return _selectView;
}

- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.frame = CGRectMake(0, 0, kscreenWidth, kscreenHeight);
        _scrollView.pagingEnabled = YES;
        _scrollView.contentSize =CGSizeMake(kscreenWidth * 2, kscreenHeight);
        _scrollView.delegate = self;
    }
    return _scrollView;
}


- (UITableView *)livingTableView
{
    if (!_livingTableView) {
        _livingTableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, kscreenWidth, tableViewHeight) style:UITableViewStylePlain];
        _livingTableView.delegate = self;
        _livingTableView.dataSource = self;
        _livingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _livingTableView.backgroundColor = MRBackGroundCloor;
        
        //下拉刷新
        _livingTableView.mj_header = ({
            MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(relodaData)];
            header.lastUpdatedTimeLabel.hidden = YES;
            header.stateLabel.hidden = YES;
            header;
        });
    }
    return _livingTableView;
}

- (UITableView *)playBackTableView
{
    if (!_playBackTableView) {
        _playBackTableView = [[UITableView alloc]initWithFrame:CGRectMake(kscreenWidth,0, kscreenWidth, tableViewHeight) style:UITableViewStylePlain];
        _playBackTableView.delegate = self;
        _playBackTableView.dataSource = self;
        _playBackTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _playBackTableView.backgroundColor = MRBackGroundCloor;
        
        //下拉刷新
        _playBackTableView.mj_header = ({
            MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(relodaData)];
            header.lastUpdatedTimeLabel.hidden = YES;
            header.stateLabel.hidden = YES;
            header;
        });
    }
    return _playBackTableView;
}

- (MALiveHeaderView *)playBackHeaderView
{
    if (!_playBackHeaderView) {
        _playBackHeaderView = [[MALiveHeaderView alloc] init];
        _playBackHeaderView.frame = CGRectMake(0, 0, kscreenWidth, 137.5);
        _playBackHeaderView.backgroundColor = MRBackGroundCloor;
        _playBackHeaderView.delegate = self;
    }
    return  _playBackHeaderView;
}

- (MALiveHeaderView *)livingHeaderView
{
    if (!_livingHeaderView) {
        _livingHeaderView = [[MALiveHeaderView alloc] init];
        _livingHeaderView.frame = CGRectMake(0, 0, kscreenWidth, 137.5);
        _livingHeaderView.backgroundColor = MRBackGroundCloor;
        _livingHeaderView.delegate = self;

    }
    return  _livingHeaderView;
}

- (UIButton *)playLivingButton
{
    if (!_playLivingButton) {
        _playLivingButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _playLivingButton.frame = CGRectMake((kscreenWidth - 30) / 2, kscreenHeight - 30 - 10 - 64 - 49, 30, 30);
        _playLivingButton.backgroundColor = MRMainColor;
        [_playLivingButton addTarget:self action:@selector(playLivingButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _playLivingButton;
}

- (NSMutableArray *)announceArray
{
    if (!_announceArray) {
        _announceArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _announceArray;
}

- (NSMutableArray *)livingArray
{
    if (!_livingArray) {
        _livingArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _livingArray;
}
- (NSMutableArray *)playbackArray
{
    if (!_playbackArray) {
        _playbackArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _playbackArray;
}

- (NSMutableArray *)livingUserArray
{
    if (!_livingUserArray) {
        _livingUserArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _livingUserArray;
}

- (NSMutableArray *)playBackUserArray
{
    if (!_playBackUserArray) {
        _playBackUserArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _playBackUserArray;
}

@end
