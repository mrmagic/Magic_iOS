//
//  MALiveShowCtl.m
//  Magic
//
//  Created by 王 on 16/9/29.
//  Copyright © 2016年 王. All rights reserved.
//

#import "MALiveShowCtl.h"
#import <GPUImage/GPUImage.h>
#import <libksygpulive/libksygpulive.h>
#import "MALiveUserCell.h"
#import "MALiveUserModel.h"
#import "MALiveChatCell.h"
#import "MALiveSocketHealper.h"


@interface MALiveShowCtl ()<UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

//推流
@property (nonatomic, strong) KSYGPUStreamerKit * streamer;
@property (nonatomic, strong) NSTimer *timer;         //僵尸粉定时器
@property (nonatomic, strong) NSTimer *heartbeatTime; //心跳包
@property (nonatomic, assign) NSInteger timerCount;   //请求僵尸粉次数
@property (nonatomic, assign) NSInteger userCount;    //在线人数

//拉流播放器
@property (nonatomic, strong) KSYMoviePlayerController *player;

//socket
@property (nonatomic, strong) SocketIOClient *socket;


//背景
@property (nonatomic, strong) UIView *backView;

//主播信息
@property (nonatomic, strong) UIView *leftView;
@property (nonatomic, strong) UIImageView *IconImageView;
@property (nonatomic, strong) UILabel *liveLabel;
@property (nonatomic, strong) UIButton *magicAttention;
@property (nonatomic, strong) UILabel *onlineLabel;

//用户列表
@property (nonatomic, strong) UICollectionViewFlowLayout *flowlayout;
@property (nonatomic, strong) UICollectionView *collectionview;
@property (nonatomic, strong) NSMutableArray *listModelArray;

//聊天
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *chatArray;

//按钮
@property (nonatomic, strong) UIView *controlView;
@property (nonatomic, strong) UIButton *cameraButton;
@property (nonatomic, strong) UIButton *chatButton;
@property (nonatomic, strong) UIButton *cancleButton;

@property (nonatomic, strong) UIView *toolBar;
@property (nonatomic, strong) UITextField *keyField;
@property (nonatomic, strong) UIButton *pushButton;

@property (nonatomic, copy) NSString *url;
@property (nonatomic, strong) NSString *timeStamp;



@end

@implementation MALiveShowCtl

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.timeStamp = @"12121212";
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    //禁止优化返回
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initWithStream];
    [self sendInfor];
    [self addNotification];
}

#pragma mark - 网络请求
- (void)sendInfor
{
    NSDictionary *parameters = @{@"showid":[MRUser sharedClient].ID,};
    NSString *url = [NSURL stringURLWithSever:@"User.setNodejsInfo" parameters:parameters];
    [[MRHttpClient sharedClient] get:url
                          parameters:nil
                             success:^(NSDictionary *JSON, NSInteger code) {
                                 if(code == 0)
                                 {
                                     [self addSocket];
                                 }else{
                                     
                                 }
                                 
                             } failure:^(NSError *error) {
                                 
                             }];
}


#pragma mark - UITextFiledDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(range.length + range.location > textField.text.length){
        return NO;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 50;//最大字数长度
}
- (BOOL)textFieldShouldReturn:(UITextField *)aTextfield
{
    [self pushButtonClick];
    return YES;
}
#pragma mark - 通知
- (void)addNotification
{
    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)keyboardWillShow:(NSNotification *)note
{
    //获取键盘的高度
    NSDictionary *userInfo = [note userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    int height = keyboardRect.size.height;
    [UIView animateWithDuration:0.2 animations:^{
        self.backView.frame = CGRectMake(0, - height, kscreenWidth, kscreenHeight - 60);
    }];
    [UIView animateWithDuration:0.2 animations:^{
        self.toolBar.frame = CGRectMake(0, kscreenHeight - height - 44, kscreenWidth, 44);
    }];
    
}

- (void)keyboardWillHide:(NSNotification *)note
{
    [UIView animateWithDuration:0.2 animations:^{
        self.backView.frame = CGRectMake(0, 0, kscreenWidth, kscreenHeight - 60);
    }];
    [UIView animateWithDuration:0.2 animations:^{
        self.toolBar.frame = CGRectMake(0, kscreenHeight, kscreenWidth, 44);
    }];
    self.keyField.text = @"";
}

- (void)keyBoardDiss
{
    [self.keyField resignFirstResponder];
}

#pragma mark - 构造试图
- (void)initWithStream
{
    if (self.showType == MALiveShowTypeLiving) {
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if (status == AVAuthorizationStatusDenied || status == AVAuthorizationStatusRestricted) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                           message:@"相机授权未开启"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            alert.message = @"请在系统设置中开启相机授权";
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"暂不"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               [self.navigationController popViewControllerAnimated:YES];
                                                           }];
            
            
            UIAlertAction *ensure = [UIAlertAction actionWithTitle:@"去设置"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                               if ([[UIApplication sharedApplication] canOpenURL:url]) {
                                                                   [[UIApplication sharedApplication] openURL:url];
                                                               }
                                                           }];
            [alert addAction:cancel];
            [alert addAction:ensure];
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            [self startPreview];
            [self startStream];
            //请求僵尸粉
            self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(zombie) userInfo:nil repeats:YES];
            [self setupUI];
        }
    } else {
        [self.view addSubview:self.player.view];
        [self setupUI];
    }
}

/**
 *  构建试图
 */
-(void)setupUI
{
    [self.view addSubview:self.backView];
    [self.backView addSubview:self.leftView];
    [self.leftView addSubview:self.IconImageView];
    [self.leftView addSubview:self.liveLabel];
    [self.leftView addSubview:self.onlineLabel];
    if (self.showType == MALiveShowTypeLiving) {
        self.leftView.frame = CGRectMake(15, 15, CGRectGetMaxX(self.liveLabel.frame) + 5, 40);
    } else {
        [self.leftView addSubview:self.magicAttention];
    }
    [self.backView addSubview:self.collectionview];
    [self.backView addSubview:self.tableView];
    [self.view addSubview:self.controlView];
    if (self.showType == MALiveShowTypeLiving) {
        [self.controlView addSubview:self.cameraButton];
        [self.controlView addSubview:self.chatButton];
        [self.controlView addSubview:self.cancleButton];
    } else {
        [self.controlView addSubview:self.chatButton];
        self.chatButton.frame = CGRectMake(15,10,40,40);
        [self.controlView addSubview:self.cancleButton];
    }
    [self.view addSubview:self.toolBar];
}

#pragma mark - 点击方法
/**
 切换摄像头
 */
- (void)cameraButtonClick
{
    [self.streamer switchCamera];
}


/**
 聊天按钮点击
 */
- (void)chatButtonClick
{
     [self.keyField becomeFirstResponder];
}


/**
 发送按钮点击
 */
- (void)pushButtonClick
{
    if (self.keyField.text.length > 0) {
        [MALiveSocketHealper SocketHealperSendChatWithSocket:self.socket
                                                      roomID:[MRUser sharedClient].ID
                                                     content:self.keyField.text];
        self.keyField.text = @"";
    }
}

/**
 关闭直播
 */
- (void)returnCancleClick
{
    if (self.showType == MALiveShowTypeLiving) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:@"是否结束直播?"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"否"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                           
                                                           
                                                       }];
        
        __weak typeof(self) weakSelf = self;
        UIAlertAction *ensure = [UIAlertAction actionWithTitle:@"是"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                           [weakSelf closeRoom];
                                                       }];
        [alert addAction:cancel];
        [alert addAction:ensure];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        [self.player stop];
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)closeRoom
{
    [self stopStream];
    [self stopPreview];
    [self.timer invalidate];
    self.timer = nil;
    [self.heartbeatTime invalidate];
    self.heartbeatTime = nil;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:kCloseRoomSuccessNotification
                                                            object:nil];
    });
}

#pragma mark - 推流拉流设置
/**
 开始采集
 */
- (void)startPreview
{
    [self.streamer startPreview:self.view];
}
/**
 关闭预览
 */
- (void)stopPreview
{
    [self.streamer stopPreview];
}

/**
 开始推流
 */
- (void)startStream
{
    self.url  = [pushRtmpUrl stringByAppendingString:self.timeStamp];
    [self.streamer.streamerBase startStream:[NSURL URLWithString:self.url]];
}

/**
 停止推流
 */
- (void)stopStream
{
    [self.streamer.streamerBase stopStream];
}

- (void)streamerState:(NSNotification *)note
{
    if (self.streamer.streamerBase.streamErrorCode == KSYStreamErrorCode_NONE) {
        NSLog(@"正常推流");
    } else if (self.streamer.streamerBase.streamErrorCode == KSYStreamErrorCode_CONNECT_BREAK) {
        NSLog(@"网络中断");
    } else if (self.streamer.streamerBase.streamErrorCode == KSYStreamErrorCode_ENCODE_FRAMES_FAILED){
        NSLog(@"当前帧获取失败");
    } else {
        NSLog(@"推流失败");
    }
}

- (void)streamerEvent:(NSNotification *)note
{
    if (self.streamer.streamerBase.netStateCode == KSYNetStateCode_NONE) {
        NSLog(@"正常推流");
    } else if (self.streamer.streamerBase.netStateCode == KSYNetStateCode_SEND_PACKET_SLOW) {
        NSLog(@"网速较慢");
    }
}

#pragma mark- 自定义方法
/**
 滚动到最新信息
 */
-(void)tableViewLastMessage
{
    NSUInteger sectionCount = [self.tableView numberOfSections];
    if (sectionCount) {
        NSUInteger rowCount = [self.tableView numberOfRowsInSection:0];
        if (rowCount) {
            NSUInteger ii[2] = {0, rowCount - 1};
            NSIndexPath* indexPath = [NSIndexPath indexPathWithIndexes:ii length:2];
            [self.tableView scrollToRowAtIndexPath:indexPath
                                  atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
    }
}


/**
 更新在线人数
 */
- (void)onlineLabelUpdata
{
    NSString *str;
    if (self.userCount > 10000) {
        str = [NSString stringWithFormat:@"%.1f万", ((float)self.userCount) / 10000];
    } else {
        str = [NSString stringWithFormat:@"%ld", self.userCount];
    }
    self.onlineLabel.text = [NSString stringWithFormat:@"%@", str];
}

//获取僵尸粉
-(void)zombie
{
    [MALiveSocketHealper SocketHealperRequestZombieWithSocket:self.socket];
}

//心跳包
-(void)sendHeartbeat
{
    [MALiveSocketHealper SocketHealperSendHeartbeatWithSocket:self.socket];
}

#pragma mark- socket
- (void)addSocket
{
    self.socket = [[SocketIOClient alloc] initWithSocketURL:[[NSURL alloc] initWithString:chat]
                                                     config:@{@"log": @NO,@"forcePolling":@YES,@"reconnectWait":@1}];
    //连接socket
    [self.socket connect];
    
    [self.socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSArray *cur = @[@{@"username":[MRUser sharedClient].user_nicename,
                           @"uid":[MRUser sharedClient].ID,
                           @"token":[MRUser sharedClient].token,
                           @"roomnum":[MRUser sharedClient].ID
                           }];
        [self.socket emit:@"conn" with:cur];
    }];
    
    //连接错误
    [self.socket on:@"error" callback:^(NSArray * data, SocketAckEmitter * ack) {
        [self.socket reconnect];
    }];
    
    //断开连接
    [self.socket on:@"disconnect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        [self.socket disconnect];
        [self.socket off:@""];
    }];
    
    //
    [self.socket on:@"conn" callback:^(NSArray* data, SocketAckEmitter* ack) {
       self.heartbeatTime = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(sendHeartbeat) userInfo:nil repeats:YES];
    }];
    
    [self.socket on:@"broadcastingListen" callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSArray* JsonData = [data[0] JSONValue];
        NSDictionary *msg = [[JsonData valueForKey:@"msg"] objectAtIndex:0];
        NSString *method = [msg valueForKey:@"_method_"];
        if ([method isEqualToString:@"SystemNot"]) { //系统消息
            NSLog(@"系统消息");
            MALiveChatModel *model = [[MALiveChatModel alloc] initWithJson:msg];
            [self.chatArray addObject:model];
            [self.tableView reloadData];
            [self tableViewLastMessage];
            
        } else if ([method isEqual:@"requestFans"]){
            NSLog(@"%@", msg);
            NSString *msgtype = [msg valueForKey:@"msgtype"];
            if([msgtype isEqual:@"0"]){
                NSString *action = [msg valueForKey:@"action"];
                if ([action isEqual:@"3"]) {
                    //僵尸粉
                    NSArray *ct = [msg valueForKey:@"ct"];
                    NSArray *data = [ct valueForKey:@"data"];
                    if ([[data valueForKey:@"code"] integerValue] == 0) {
                        NSArray *info = [data valueForKey:@"info"];
                        NSArray *list = [info valueForKey:@"list"];
                        if (self.timerCount > 10) {
                            [self.timer invalidate];
                            self.timer  = nil;
                        }
                        for (NSDictionary *dic in list) {
                            MALiveUserModel *model = [[MALiveUserModel alloc] initWithJson:dic];
                            [self.listModelArray addObject:model];
                        }
                        self.userCount = self.listModelArray.count;
                        [self onlineLabelUpdata];
                        [self.collectionview reloadData];
                    }
                }
            }
        }else if ([method isEqual:@"SendMsg"]) { //发送消息
            NSInteger msgtype = [[msg valueForKey:@"msgtype"] integerValue];
            switch (msgtype) {
                case 0: //用户进出
                {
                    NSInteger action = [[msg valueForKey:@"action"] integerValue];
                    switch (action) {
                        case 0: //用户进入
                        {
                            MALiveUserModel *model = [[MALiveUserModel alloc] initWithJson:[msg valueForKey:@"ct"]];
                            [self.listModelArray addObject:model];
                             [self onlineLabelUpdata];
                            [self.collectionview reloadData];
                        }
                            break;
                        case 1: //用户离开
                        {
                            NSMutableArray *array = [NSMutableArray arrayWithCapacity:1];
                            NSString *ID = [[msg valueForKey:@"ct"] valueForKey:@"uid"];
                            for (int i = 0; i < self.listModelArray.count; i++) {
                                MALiveUserModel *model = [self.listModelArray objectAtIndex:i];
                                if ([model.userID isEqualToString:ID]) {
                                    [array addObject:model];
                                }
                            }
                            for (MALiveUserModel *model in array) {
                                [self.listModelArray removeObject:model];
                            }
                             [self onlineLabelUpdata];
                            [self.collectionview reloadData];
                        }
                            break;
                            
                        default:
                            break;
                    }
                }
                    break;
                case 1://
                {
                    
                }
                    break;
                case 2://聊天
                {
                    MALiveChatModel *model = [[MALiveChatModel alloc] initWithJson:msg];
                    model.content = [msg valueForKey:@"ct"];
                    model.userName = [msg valueForKey:@"uname"];;
                    
                    [self.chatArray addObject:model];
                    [self.tableView reloadData];
                    [self tableViewLastMessage];
                }
                    break;
                default:
                    break;
            }
        }  else if([method isEqualToString:@"disconnect"]){ //用户断开连接
            NSString *action = [msg valueForKey:@"action"];
            NSDictionary *SUBdIC =[msg valueForKey:@"ct"];
            NSString *ID = [SUBdIC valueForKey:@"id"];
            
            //用户离开
            if ([action isEqual:@"1"]) {
                NSMutableArray *array = [NSMutableArray arrayWithCapacity:1];
                for (int i = 0; i < self.listModelArray.count; i++) {
                    MALiveUserModel *model = [self.listModelArray objectAtIndex:i];
                    if ([model.userID isEqualToString:ID]) {
                        [array addObject:model];
                    }
                }
                for (MALiveUserModel *model in array) {
                    [self.listModelArray removeObject:model];
                }
                [self.collectionview reloadData];
                 [self onlineLabelUpdata];
            }
        }
    }];

}

#pragma mark - UICollectionViewDelagate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.listModelArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MALiveUserCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MALiveUserCell" forIndexPath:indexPath];
    MALiveUserModel *model = [self.listModelArray objectAtIndex:indexPath.item];
    [cell cellWithModel:model];
    return cell;
}

#pragma mark - TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.chatArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MALiveChatCell *cell = [MALiveChatCell cellWithTableView:tableView];
    MALiveChatModel *model = [self.chatArray objectAtIndex:indexPath.row];
    [cell cellWithModel:model];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MALiveChatModel *model = [self.chatArray objectAtIndex:indexPath.row];
    return model.viewHeight;
}

#pragma mark - 懒加载

/**
 推流
 */
- (KSYGPUStreamerKit *)streamer
{
    if (!_streamer) {
        _streamer = [[KSYGPUStreamerKit alloc] initWithDefaultCfg];
        _streamer.videoFPS = 18;
        _streamer.videoDimensionUserDefine = CGSizeMake(kscreenWidth, kscreenHeight);
        _streamer.videoOrientation = AVCaptureVideoOrientationPortrait;
        _streamer.bDefaultToSpeaker = YES;
        _streamer.streamerBase.videoCodec = KSYVideoCodec_X264;
        _streamer.streamerBase.audiokBPS  = 48;
        _streamer.streamerBase.videoMaxBitrate   = 800;
        _streamer.streamerBase.videoMinBitrate   = 300;
        _streamer.streamerBase.enAutoApplyEstimateBW = YES;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(streamerState:)
                                                     name:KSYStreamStateDidChangeNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(streamerEvent:)
                                                     name:KSYNetStateEventNotification
                                                   object:nil];
    }
    return _streamer;
}

/**
 拉流
 */
- (KSYMoviePlayerController *)player
{
    if (!_player) {
        self.url  = [playRtmpUrl stringByAppendingString:self.timeStamp];
        _player = [[KSYMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:self.url]];
        _player.controlStyle = MPMovieControlStyleNone;
        _player.view.frame = self.view.bounds;
        _player.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        _player.shouldAutoplay = YES;
        _player.scalingMode = MPMovieScalingModeAspectFit;
        [_player prepareToPlay];
    }
    return _player;
}

- (UIView *)backView
{
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.frame = CGRectMake(0, 0, kscreenWidth, kscreenHeight - 60);
        _backView.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyBoardDiss)];
        [_backView addGestureRecognizer:tap];
    }
    return _backView;
}
- (UIView *)leftView
{
    if (!_leftView) {
        _leftView = [[UIView alloc]init];
        _leftView.frame = CGRectMake(15, 15, 150, 40);
        _leftView.userInteractionEnabled = YES;
        _leftView.layer.masksToBounds = YES;
        _leftView.backgroundColor = [UIColor colorWithHex:0x434343 alpha:0.4];
        _leftView.layer.cornerRadius = self.leftView.frame.size.height / 2;
    }
    return _leftView;
}

- (UIImageView *)IconImageView
{
    if (!_IconImageView) {
        _IconImageView = [[UIImageView alloc] init];
        _IconImageView.frame = CGRectMake(5,5, 30, 30);
        _IconImageView.layer.masksToBounds = YES;
        _IconImageView.layer.cornerRadius = 15;
        _IconImageView.image = [UIImage imageNamed:@"image_placeHolder"];
    }
    return _IconImageView;
}
- (UILabel *)liveLabel
{
    if (!_liveLabel) {
        _liveLabel =  [[UILabel alloc]init];
        _liveLabel.text = @"直播Live";
        _liveLabel.textAlignment = NSTextAlignmentCenter;
        _liveLabel.font = [UIFont systemFontOfSize:11];
        _liveLabel.frame = CGRectMake(40,0,[self.liveLabel widthForLabelWithHeight:11] + 10,20);
        self.liveLabel.textColor = [UIColor whiteColor];
    }
    return _liveLabel;
}

- (UILabel *)onlineLabel
{
    if (!_onlineLabel) {
        _onlineLabel = [[UILabel alloc] init];
        _onlineLabel.frame = CGRectMake(40,17,[self.liveLabel widthForLabelWithHeight:20] + 10,20);
        _onlineLabel.textAlignment = NSTextAlignmentCenter;
        _onlineLabel.textColor = [UIColor whiteColor];
        _onlineLabel.font = [UIFont systemFontOfSize:12];
    }
    return _onlineLabel;
}

- (UIButton *)magicAttention
{
    if (!_magicAttention) {
        _magicAttention = [UIButton buttonWithType:UIButtonTypeCustom];
        _magicAttention.backgroundColor = [UIColor whiteColor];
        _magicAttention.titleLabel.font = [UIFont systemFontOfSize:10];
        [_magicAttention setTitleColor:MRMainColor forState:UIControlStateNormal];
        _magicAttention.frame  = CGRectMake(CGRectGetMaxX(self.liveLabel.frame) + 5,10,45,20);
        _magicAttention.clipsToBounds = YES;
        _magicAttention.layer.cornerRadius = 20 / 2;
        [_magicAttention setTitle:@"关注" forState:UIControlStateNormal];
    }
    return _magicAttention;
}

- (UICollectionViewFlowLayout *)flowlayout
{
    if (!_flowlayout) {
        _flowlayout = [[UICollectionViewFlowLayout alloc]init];
        _flowlayout.itemSize = CGSizeMake(30,30);
        _flowlayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    return _flowlayout;
}

- (UICollectionView *)collectionview
{
    if (!_collectionview) {
        _collectionview = [[UICollectionView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.leftView.frame) + 5, 15, kscreenWidth - CGRectGetMaxX(self.leftView.frame) - 5 - 15, 40)
                                            collectionViewLayout:self.flowlayout];
        _collectionview.bounces = NO;
        _collectionview.showsHorizontalScrollIndicator = NO;
        _collectionview.delegate = self;
        _collectionview.dataSource = self;
        _collectionview.backgroundColor = [UIColor clearColor];
        [_collectionview registerClass:[MALiveUserCell class] forCellWithReuseIdentifier:@"MALiveUserCell"];
    }
    return _collectionview;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(15,kscreenHeight - 200 - 60, kscreenWidth - 30, 200)
                                                 style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
    }
    return _tableView;
}

- (UIView *)controlView
{
    if (!_controlView) {
        _controlView = [[UIView alloc] init];
        _controlView.frame = CGRectMake(0, kscreenHeight - 60, kscreenWidth, 60);
        _controlView.backgroundColor = [UIColor clearColor];
    }
    return _controlView;
}

- (UIButton *)cameraButton
{
    if (!_cameraButton) {
        _cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cameraButton.tintColor = [UIColor whiteColor];
        [_cameraButton setBackgroundImage:[UIImage imageNamed:@"icon_live_camera"] forState:UIControlStateNormal];
        _cameraButton.backgroundColor = [UIColor clearColor];
        [_cameraButton addTarget:self action:@selector(cameraButtonClick) forControlEvents:UIControlEventTouchUpInside];
        _cameraButton.frame = CGRectMake(15,10,40,40);
    }
    return _cameraButton;
}

- (UIButton *)chatButton
{
    if (!_chatButton) {
        _chatButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _chatButton.tintColor = [UIColor whiteColor];
        [_chatButton setBackgroundImage:[UIImage imageNamed:@"icon_live_talk"] forState:UIControlStateNormal];
        _chatButton.backgroundColor = [UIColor clearColor];
        [_chatButton addTarget:self action:@selector(chatButtonClick) forControlEvents:UIControlEventTouchUpInside];
        _chatButton.frame = CGRectMake(15 + 40 +10,10,40,40);
    }
    return _chatButton;
}

- (UIView *)toolBar
{
    if (!_toolBar) {
        _toolBar = [[UIView alloc] init];
        _toolBar.frame = CGRectMake(0,kscreenHeight, kscreenWidth, 44);
        _toolBar.backgroundColor = [UIColor whiteColor];
        
        //输入框
        self.keyField = [[UITextField alloc]initWithFrame:CGRectMake(5, 0, kscreenWidth - 70, 44)];
        self.keyField.returnKeyType = UIReturnKeySend;
        self.keyField.borderStyle = UITextBorderStyleNone;
        self.keyField.delegate = self;
        self.keyField.placeholder = @"最多输入50个字符";
        [_toolBar addSubview:self.keyField];//输入框

        //分割线
        UIView *line = [[UIView alloc] init];
        line.frame = CGRectMake(kscreenWidth - 60, 7, 0.5, 30);
        line.backgroundColor = MRLineColor;
        [_toolBar addSubview:line];
        
        //发送按钮
        self.pushButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.pushButton setTitle:@"发送" forState:UIControlStateNormal];
        [self.pushButton setTitleColor:MRTextColor forState:UIControlStateNormal];
        self.pushButton.layer.masksToBounds = YES;
        self.pushButton.layer.cornerRadius = 5;
        self.pushButton.frame = CGRectMake(kscreenWidth - 60, 0, 60, 44);
        [self.pushButton addTarget:self
                            action:@selector(pushButtonClick)
                  forControlEvents:UIControlEventTouchUpInside];
        [_toolBar addSubview:self.pushButton];//发送
    }
    return _toolBar;
}

- (UIButton *)cancleButton
{
    if (!_cancleButton) {
        _cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancleButton.tintColor = [UIColor whiteColor];
        [_cancleButton setBackgroundImage:[UIImage imageNamed:@"icon_live_close"] forState:UIControlStateNormal];
        _cancleButton.backgroundColor = [UIColor clearColor];
        [_cancleButton addTarget:self action:@selector(returnCancleClick) forControlEvents:UIControlEventTouchUpInside];
        _cancleButton.frame = CGRectMake(kscreenWidth - 40 -15,10,40,40);
    }
    return _cancleButton;
}

- (NSMutableArray *)listModelArray
{
    if (!_listModelArray) {
        _listModelArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _listModelArray;
}
- (NSMutableArray *)chatArray
{
    if (!_chatArray) {
        _chatArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _chatArray;
}




@end
