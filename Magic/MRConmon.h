//
//  魔术配置文件
//  iphoneLive
//
//  Created by
//  Copyright © 2016年 cat. All rights reserved.
//

#ifndef MRConmon_h
#define MRConmon_h

#pragma mark -- 通知
#define  kApplicationDidEnterBackground  @"kApplicationDidEnterBackground"  //App 进入后台
#define  kApplicationWillEnterForeground @"kApplicationWillEnterForeground" //App 返回前台
#define  kTabBarControllerSelectedIndex  @"kTabBarControllerSelectedIndex"  //tabbar重复点击相同的item
#define  kWebViewReturn                  @"kWebViewReturn"                  //h5返回
#define  kLoginStatusChangeNotification  @"kLoginStatusChangeNotification"  //登录状态改变
#define  kRegisterSuccessNotification    @"kRegisterSuccessNotification"    //注册成功
#define  kAttentionSuccessNotification   @"kAttentionSuccessNotification"   //关注成功
#define  kBecomeMagicSuccessNotification @"kBecomeMagicSuccessNotification" //成为魔术师
#define  kCloseRoomSuccessNotification   @"kCloseRoomSuccessNotification"   //关闭直播
#define  kMeAttentionResult              @"kMeAttentionResult"              //关注结果 (1关注成功 2取消关注)
#define  kMePayPageResult                @"kMePayPageResult"                //支付结果 (1充值成功 2充值失败)


#pragma mark -- 域名 推流
// 产品模式; 0:正式; 1:开发; 2:测试
#define MR_PRODUCT_MODE 0

//开发
#if MR_PRODUCT_MODE == 1
#define purl @"https://api.live.superx.org/liveapi/"
#define chatServer @"chat.live.superx.org"
#define chatPort 19967

//TODO 测试
#elif MR_PRODUCT_MODE == 2
#define purl @"https://api.live.superx.org/liveapi/"
#define chatServer @"chat.live.superx.org"
#define chatPort 19967

//正式
#else
#define purl @"https://api.live.superx.org/liveapi/"
#define chatServer @"chat.live.superx.org"
#define chatPort 19967
#define chat @"http://chat.live.superx.org:19967"

#endif

//app内购验证
#define sanBox 1


//推流
#define playRtmpUrl @"rtmp://phone_play.yunbaozhibo.com/5showcam/"
#define pushRtmpUrl @"rtmp://phone_push.yunbaozhibo.com/5showcam/"

#pragma mark --  尺寸配置
#define  kscreenWidth  [UIScreen mainScreen].bounds.size.width //屏幕宽度
#define  kscreenHeight  [UIScreen mainScreen].bounds.size.height //屏幕长度




#pragma mark -- 颜色
#define MRMainColor [UIColor colorWithHex:0xf25050]
#define MRLineColor [UIColor colorWithHex:0xc5c5c5]
#define MRTextColor [UIColor colorWithHex:0x434343]
#define MRBackGroundCloor [UIColor colorWithHex:0xefefef]
#define MRTableViewColor  [UIColor colorWithRed:249/255.0 green:249/255.0 blue:249/255.0 alpha:1.0]

#pragma mark -- 字体




#pragma mark -- 文字配置

#define wifi @"您当前处于非wifi状态下,确定使用蜂窝数据吗?"

#define MRNetWorkError @"网络连接错误,请检查您的网络!"   //App 返回前台

#pragma mark --  其他配置
#define MRSystemVersion  [[UIDevice currentDevice] systemVersion] //手机系统版本
#define MRVersion        [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"] //软件版本


#endif
