//
//  AppDelegate.h
//  Magic
//
//  Created by 王 on 16/9/29.
//  Copyright © 2016年 王. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

